

var mysql = require('mysql');
var dbconfig = require('../database');

var connection = mysql.createConnection(dbconfig.connection);

var crear_esquema = "/crearesquema"

module.exports = function(app) {


    app.post(crear_esquema , function(req,res,done){
        var bd = req.body.bd;
   
        connection.query('CREATE DATABASE IF NOT EXISTS ' + bd ,  function(err){
            if(err) {
                console.log(err);
                return done(err);
            }

            connection.query('USE ' + bd, function (err) { //conexion
            if(err) {
                console.log(err);
                return done(err);
            }

            //creaciones de tablas
            connection.query(sql_seccion, function(err) {
                if(err) {
                    console.log(err);
                    return done(err);
                }
            })
            connection.query(sql_ubicacion, function(err) {
                if(err) {
                    console.log(err);
                    return done(err);
                }
            })
            connection.query(sql_productos, function(err) {
                if(err) {
                    console.log(err);
                    return done(err);
                }
            })
            connection.query(sql_promocion, function(err) {
                if(err) {
                    console.log(err);
                    return done(err);
                }
            })
            connection.query(sql_asignar_promocion, function(err) {
                if(err) {
                    console.log(err);
                    return done(err);
                }
            })
            connection.query(sql_promoasg_productos, function(err) {
                if(err) {
                    console.log(err);
                    return done(err);
                }
            })
            connection.query(sql_supermercado, function(err) {
                if(err){
                    console.log(err);
                    return done(err);
                }
            })
            connection.query(sql_servicios, function(err) {
                if(err) {
                    console.log(err);
                    return done(err);
                }
            })
            connection.query(sql_listas, function(err) {
                if(err) {
                    console.log(err);
                    return done(err);
                }
            })
            connection.query(sql_lista_producto, function(err) {
                if(err) {
                    console.log(err);
                    return done(err);
                }
            })

            //relaciones entre tablas
            connection.query(sql_relacion_ubicacion(bd), function(err) {
                if(err) {
                    console.log(err);
                    return done(err);
                }
            })

            connection.query(sql_relacion_productos(bd), function(err) {
                if(err) {
                    console.log(err);
                    return done(err);
                }
            })

            connection.query(sql_relacion_asgpromocion(bd), function(err) {
                if(err) {
                    console.log(err);
                    return done(err);
                }              
            })

            connection.query(sql_relacion_promoasg_productos(bd), function(err) {
                if(err) {
                    console.log(err);
                    return done(err);
                }              
            })
            connection.query(sql_relacion_lista_productos(bd), function(err) {
                if(err) {
                    console.log(err);
                    return done(err);
                }              
            })
            
        })//conexion con la bd


        })// creacion de la bd
        console.log('Success: scheme Created!')
        //
        res.send(200);
      
    })

}

var sql_seccion = 'CREATE TABLE IF NOT EXISTS seccion('
            + 'id INT(11) NOT NULL AUTO_INCREMENT,'
            + 'nombre VARCHAR(80),'
            + 'PRIMARY KEY(id)'
            + ')';

var sql_ubicacion = 'CREATE TABLE IF NOT EXISTS ubicacion('
            + 'cod_ubicacion INT(11) NOT NULL AUTO_INCREMENT,'
            + 'corredor INT(11),'
            + 'numeroEstante INT(11),'
            + 'numeroPiso INT(11),'
            + 'pasillo INT(11),'
            + 'fk_seccion INT(11),'
            + 'PRIMARY KEY(cod_ubicacion)'
            + ')'


var sql_productos = 'CREATE TABLE IF NOT EXISTS producto('
            + 'codigo INT NOT NULL AUTO_INCREMENT,'
            + 'cod_prod VARCHAR(50),'
            + 'cod_barras_prod VARCHAR(20),'
            + 'nom_prod VARCHAR(150),'
            + 'imagen VARCHAR(150),'
            + 'cant_prod INT(11),'
            + 'cant_vendido INT(11) NOT NULL DEFAULT 0,'
            + 'costo_prod DOUBLE,'
            + 'pvp_prod DOUBLE,'
            + 'iva_prod VARCHAR(5),'
            + 'cod_marca INT(11),'
            + 'tiempo_garantia VARCHAR(10),'
            + 'ubicacion_id INT(11),'
            + 'PRIMARY KEY(codigo)'
            +  ')'

var sql_promocion = 'CREATE TABLE IF NOT EXISTS promocion('
            + 'id_promocion INT NOT NULL AUTO_INCREMENT,'
            + 'descripcion VARCHAR(255),'
            + 'nombre VARCHAR(80),'
            + 'descuento VARCHAR(80),'
            + 'PRIMARY KEY(id_promocion)'
            +  ')'

var sql_asignar_promocion = 'CREATE TABLE IF NOT EXISTS asignarpromocion('
            + 'id_asg_promocion INT NOT NULL AUTO_INCREMENT,'
            + 'fecha DATE,'
            + 'fechaFin DATE,'
            + 'id_promocion INT(11),'
            + 'PRIMARY KEY(id_asg_promocion)'
            +  ')'

var sql_promoasg_productos = 'CREATE TABLE IF NOT EXISTS promoasg_productos('
            + 'id_asgpromo_product INT NOT NULL AUTO_INCREMENT,'
            + 'fk_asignar_promocion INT(11),'
            + 'fk_producto INT(11),'
            + 'PRIMARY KEY(id_asgpromo_product)'
            +  ')'

var sql_supermercado = 'CREATE TABLE IF NOT EXISTS supermercado('
            + 'id INT NOT NULL AUTO_INCREMENT,'
            + 'nombre VARCHAR(80),'
            + 'nombre_bd VARCHAR(100),'
            + 'direccion VARCHAR(200),'
            + 'id_paypal VARCHAR(200),'
            + 'id_analitica_paypal VARCHAR(200),'
            + 'telefonos VARCHAR(100),'
            + 'mision TEXT(100),'
            + 'vision TEXT(100),'
            + 'ruc VARCHAR(13),'
            + 'longitud VARCHAR(120),'
            + 'latitud VARCHAR(100),'
            + 'pagar_entrega DOUBLE,'
            + 'id_admin INT(11),'
            + 'PRIMARY KEY(id)'
            +  ')'

var sql_servicios = 'CREATE TABLE IF NOT EXISTS servicios('
            + 'id INT NOT NULL AUTO_INCREMENT,'
            + 'promocion BOOLEAN,'
            + 'ubicacion_producto BOOLEAN,'
            + 'listas BOOLEAN,'
            + 'analitica BOOLEAN,'
            + 'PRIMARY KEY(id)'
            +  ')'

var sql_listas = 'CREATE TABLE IF NOT EXISTS listas('
            + 'id INT NOT NULL AUTO_INCREMENT,'
            + 'id_user_cliente INT(11),'
            + 'date_time_pedido DATETIME,'
            + 'date_time_despacho DATETIME,'
            + 'estado_inicial BOOLEAN,'
            + 'estado_proceso BOOLEAN,'
            + 'estado_despachada BOOLEAN,'
            + 'entrega BOOLEAN,'
            + 'PRIMARY KEY(id)'
            +  ')'

var sql_lista_producto = 'CREATE TABLE IF NOT EXISTS lista_producto('
            + 'id INT NOT NULL AUTO_INCREMENT,'
            + 'fk_lista INT(11),'
            + 'fk_producto INT(11),'
            + 'cantidad INT(11),'
            + 'PRIMARY KEY(id)'
            +  ')'

function sql_relacion_ubicacion(bd){

    return 'ALTER TABLE '+ bd +'.ubicacion '
        + 'ADD CONSTRAINT clave_foranea_seccion '
        + 'FOREIGN KEY (fk_seccion) '
        + 'REFERENCES '+ bd +'.seccion(id) '
        + 'ON DELETE CASCADE ON UPDATE CASCADE';
}

function sql_relacion_productos (bd){
    return 'ALTER TABLE producto '
        + 'ADD CONSTRAINT fk_producto_ubicacion '
        + 'FOREIGN KEY (ubicacion_id) '
        + 'REFERENCES '+ bd +'.ubicacion(cod_ubicacion) '
        + 'ON DELETE SET NULL ON UPDATE CASCADE';
}

function sql_relacion_asgpromocion(bd){
    return 'ALTER TABLE asignarpromocion '
        + 'ADD CONSTRAINT fk_promocionid '
        + 'FOREIGN KEY (id_promocion) '
        + 'REFERENCES '+ bd +'.promocion(id_promocion) '
        + 'ON DELETE CASCADE ON UPDATE CASCADE';
}

function sql_relacion_promoasg_productos(bd){
    return 'ALTER TABLE promoasg_productos ' 
        + 'ADD CONSTRAINT fk_reference_asg_prod '
        + 'FOREIGN KEY (fk_asignar_promocion) '
        + 'REFERENCES '+ bd +'.asignarpromocion(id_asg_promocion) '
        + 'ON DELETE CASCADE ON UPDATE CASCADE';
}

function sql_relacion_lista_productos(bd){
    return 'ALTER TABLE lista_producto ' 
        + 'ADD CONSTRAINT fk_lista_asg_prod '
        + 'FOREIGN KEY (fk_lista) '
        + 'REFERENCES '+ bd +'.listas(id) '
        + 'ON DELETE CASCADE ON UPDATE CASCADE';
}
