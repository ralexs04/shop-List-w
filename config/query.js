//load for img
//var express  = require('express');
var path = require('path');
var fs = require('fs');
var uuid = require('node-uuid');

// load up the user model
var mysql = require('mysql');
var bcrypt = require('bcrypt-nodejs'); // para encriptar la contraseni a actualizar

var dbconfig = require('./database');
var connection = mysql.createConnection(dbconfig.connection);

// expose this function to our app using module.exports
connection.query('USE ' + dbconfig.database);

module.exports = function(app) {
var connect = connection.query('USE ' + dbconfig.database);
 // routes generales

//web service

app.get('/',function(req,res){
    res.send('Welcome');
});

/*===========================================================SECCION MOVIL======================================*/


var codigoBarra = '/productos/codigob/:codigo/:name_bd';


//get data to update
app.get( codigoBarra , function(req,res,done){
    var codigo_b = req.params.codigo;
    var name_bd = req.params.name_bd;
    
    connection.query('USE ' + name_bd);
    connection.query("SELECT  codigo, imagen, nom_prod, pvp_prod, ubicacion_id FROM  producto WHERE cod_barras_prod = ?", [codigo_b] ,function(err,rows){
             if (err)
                    return done(err);
                if (!rows.length)
                    return done(null, false, req.flash('loginMessage', 'Mysql error, check your query"'));
                 if(rows.length < 1)
                  return done(null, false, req.flash('loginMessage', 'Producto no encontrado"'));
                res.send(JSON.stringify(rows));
        });
});

var ubicacion = '/productos/ubicacion/:ubicacion/:name_bd';
//obtener ubicacion con id
app.get( ubicacion , function(req,res,done){
    var name_bd = req.params.name_bd;
    var ubicacion_id = req.params.ubicacion;
    connection.query('USE ' + name_bd);

    connection.query("SELECT * FROM  ubicacion u , seccion s WHERE u.cod_ubicacion = ? AND u.fk_seccion = s.id", [ubicacion_id] ,function(err,rows){

                 if (err)
                    return done(err);
                if (!rows.length)
                    return done(null, false, req.flash('loginMessage', 'Mysql error, check your query"'));
                 if(rows.length < 1)
                  return done(nll, false, req.flash('loginMessage', 'Ubicacion no encontrado"'));

                res.send(JSON.stringify(rows));
        connection.query('USE ' + dbconfig.database);
        });
});

var promocion = '/productos/promocion/:name_bd';

app.get( promocion , function(req,res,done){
    var currentDate = new Date();

    var name_bd = req.params.name_bd;
    
    connection.query('USE ' + name_bd);
    connection.query("SELECT  p.imagen , p.nom_prod, p.codigo, p.ubicacion_id, p.pvp_prod, promo.nombre, promo.descripcion, promo.descuento, asp.fecha, asp.fechaFin FROM asignarpromocion asp, promocion promo, promoasg_productos prop, producto p WHERE asp.fechaFin >= CURRENT_DATE and asp.fecha <= CURRENT_DATE and promo.id_promocion = asp.id_promocion and prop.fk_asignar_promocion = asp.id_asg_promocion and p.codigo = prop.fk_producto" , [] ,function(err,rows){
       if (err)
        return done(err);
      if (!rows.length)
        return done(null, false, req.flash('loginMessage', 'Mysql error, check your query"'));
      if(rows.length < 1)
        return done(nll, false, req.flash('loginMessage', 'Promocion no encontrada"'));
      res.send(JSON.stringify(rows));
      connection.query('USE ' + dbconfig.database);
    });
});

//cuenta cuantas promociones estan disposibles
var promocion_cantidad = '/productos/promocion/cantidad/:name_bd';
app.get( promocion_cantidad , function(req,res,done){
    var currentDate = new Date();
    var name_bd = req.params.name_bd;

    connection.query('USE ' + name_bd);
    connection.query("SELECT COUNT(p.codigo) as cantidad FROM asignarpromocion asp, promocion promo, promoasg_productos prop, producto p WHERE asp.fechaFin >= CURRENT_DATE and asp.fecha <= CURRENT_DATE and promo.id_promocion = asp.id_promocion and prop.fk_asignar_promocion = asp.id_asg_promocion and p.codigo = prop.fk_producto" , [] ,function(err,rows){
       if (err)
        return done(err);
      if (!rows.length)
        return done(null, false, req.flash('loginMessage', 'Mysql error, check your query"'));
      if(rows.length < 1)
        return done(nll, false, req.flash('loginMessage', 'Promocion no encontrada"'));
      res.send(JSON.stringify(rows));
      connection.query('USE ' + dbconfig.database);
    });
});

/*verificar si ese producto esta asignado a una promocion*/


var producto_asignado_promocion = '/productos/asignado_promo/:parametro/:name_bd';
app.get( producto_asignado_promocion , function(req,res,done){

var fk_producto = req.params.parametro
var name_bd = req.params.name_bd;

    connection.query('USE ' + name_bd );
    connection.query("SELECT * FROM  promoasg_productos  WHERE fk_producto = ?  " , [fk_producto] ,function(err,rows){
       if (err)
        return done(err);
      if (!rows.length)
        return done(null, false, req.flash('loginMessage', 'Mysql error, check your query"'));
      if(rows.length < 1)
        return done(nll, false, req.flash('loginMessage', 'Producto no encontrado"'));
      res.send(JSON.stringify(rows));

    connection.query('USE ' + dbconfig.database);

    });
});


/*verificar si la promocion se encuentra asignada*/

var promocion_asignada = '/promocion/asignado_promo/:parametro/:name_bd';

app.get( promocion_asignada , function(req,res,done){

    var id_promocion = req.params.parametro;
    var name_bd = req.params.name_bd;
    
    connection.query('USE ' + name_bd);

    connection.query("SELECT * FROM  asignarpromocion  WHERE id_promocion = ?  " , [id_promocion] ,function(err,rows){
       if (err)
        return done(err);
      if (!rows.length)
        return done(null, false, req.flash('message', 'Mysql error, check your query"'));
      if(rows.length < 1)
        return done(nll, false, req.flash('message', 'promocion no encontrado"'));
      res.send(JSON.stringify(rows));

      connection.query('USE ' + dbconfig.database);
    });
});

//verificar si la ubicacion tiene productos

var ubicacion_productos = '/ubicacion/tiene_productos/:parametro/:name_bd';
app.get( ubicacion_productos , function(req,res,done){
  var cod_ubicacion = req.params.parametro;
  var name_bd = req.params.name_bd;
  connection.query('USE ' + name_bd);
  connection.query("SELECT * FROM producto  WHERE ubicacion_id = ?  " , [cod_ubicacion] ,function(err,rows){
       if (err)
        return done(err);
      if (!rows.length)
        return done(null, false, req.flash('message', 'Mysql error, check your query"'));
      if(rows.length < 1)
        return done(nll, false, req.flash('message', 'ubicacion no encontrada'));
      res.send(JSON.stringify(rows));
      connection.query('USE ' + dbconfig.database);
    });
});

var todos_productos = '/productos/totales/:name_bd';
app.get( todos_productos , function(req,res,done){
  var name_bd = req.params.name_bd;
  connection.query('USE ' + name_bd);
  connection.query("SELECT * FROM producto" , [] ,function(err,rows){
       if (err)
        return done(err);
      if (!rows.length)
        return done(null, false, req.flash('message', 'Mysql error, check your query"'));
      if(rows.length < 1)
        return done(nll, false, req.flash('message', 'productos no encontrados'));
      res.send(JSON.stringify(rows));
      connection.query('USE ' + dbconfig.database);
    });
});


/*===========================================================seccion WEB ==========================================*/

/*************************PROMOCIONES**************************/
var url_promocion = '/web/promocion/:name_bd?';
//var url_promocion = '/web/promocion/:name_bd';

app.get( url_promocion , function(req,res,done){
    //var ubicacion_id = req.params.ubicacion;
    
    var name_bd = req.params.name_bd;
    connection.query('USE ' + name_bd);

    var currentDate = new Date();
    connection.query("SELECT * FROM  promocion ORDER BY id_promocion DESC", [] ,function(err,rows){
      if (err)
        return done(err);
      if (!rows.length)
        return done(null, false, req.flash('loginMessage', 'Mysql error, check your query"'));
      if(rows.length < 1)
        return res.send(JSON.stringify({"mensaje " : "No existen promociones"}));

      res.send(JSON.stringify(rows));
      connection.query('USE ' + dbconfig.database);
    });
});

////////GUARDAR UNA NUEVA PROMOCION///////
app.post(url_promocion , function(req,res,done){
    //get data
    var data = {
        nombre:req.body.nombre,
        descripcion:req.body.descripcion,
        descuento :req.body.descuento
     };

    //inserting into mysql
    var name_bd = req.body.name_bd;
    connection.query('USE ' + name_bd);
    connection.query("INSERT INTO promocion set ? ",data, function(err, rows){

           if(err){
                console.log(err);
                return done("Mysql error, check your query");
           }
          //devuelve el id de la fila insertada
          res.send(JSON.stringify(rows.insertId));

        });
    connection.query('USE ' + dbconfig.database);
});

/////////////PROMOCION ID////////////////////
var promocionId = '/admin/productos/promocion/:promocion_id/:name_bd';

//obtener una promocion en base a un ID
app.get(promocionId , function(req,res,done){
    var promocion_id = req.params.promocion_id;
    var name_bd = req.params.name_bd;
    connection.query('USE ' + name_bd);

    connection.query("SELECT * FROM  promocion WHERE id_promocion = ?", [promocion_id] ,function(err,rows){
            if(err){// si encuentra error en consulta
                console.log(err);
                return done("Mysql error, check your query");
            }
            //if user not found
            if(rows.length < 1)// si el resultado esta en cero no encontro valores
                return res.send("Promocion Not found");
            res.send(JSON.stringify(rows));//enviar los datos encontrados
            connection.query('USE ' + dbconfig.database);
        });
    });

//Editar Promocion Seleccionada
app.put(promocionId , function(req,res,done){
 var promocion_id = req.params.promocion_id; //recibe el parametro  que me llega por la url
 var name_bd = req.params.name_bd;

    //obtener los datos
    var data = {
        nombre:req.body.nombre,
        descripcion:req.body.descripcion,
        descuento :req.body.descuento
     };
    //insertar en mysql
    connection.query('USE ' + name_bd);
    connection.query("UPDATE promocion set ? WHERE id_promocion = ? ",[data , promocion_id], function(err, rows){
       if(err){ //error en consulta
            console.log(err);
            return done("Mysql error, check your query");
       }
      res.send(200);//enviar todo Ok
      connection.query('USE ' + dbconfig.database); 
      });
});

//ELIMINAR PROMOCION SELECIONADA (Elimina la promocion y la asignacion que le pertenece DELETE CASCADE)
app.delete(promocionId , function(req,res,done){
     var promocion_id = req.params.promocion_id;
     connection.query("DELETE FROM promocion  WHERE id_promocion = ? ",[promocion_id], function(err, rows){
             if(err){ //error en consulta
                console.log(err);
                return done("Mysql error, check your query");
             }
             res.send(200);//enviar todo ok
        });
});


/*************************PROMOCIONES ASG**************************/
var promocion_asg = '/admin/productos/promocionasg/:name_bd?';
//get data to update
app.get(promocion_asg , function(req,res,done){
    var name_bd = req.params.name_bd;
    var currentDate = new Date();//obtener la fecha actual

    connection.query('USE ' + name_bd);
    connection.query("SELECT  DISTINCT asgp.id_asg_promocion , asgp.fecha , asgp.fechaFin , pr.nombre , pr.id_promocion FROM  asignarpromocion asgp , promocion pr  , promoasg_productos pasgp WHERE  pasgp.fk_asignar_promocion = asgp.id_asg_promocion AND pr.id_promocion = asgp.id_promocion  ORDER BY id_asg_promocion  DESC", [] ,function(err,rows){

            if(err){
                console.log(err);
                return done("Mysql error, check your query");
            }
            //if promocion no encontrada
            if(rows.length < 1)
                return res.send(JSON.stringify({"mensaje":"Sin Promociones Asigandas"}));
                res.header('Access-Control-Allow-Origin', "*");
                res.set('Content-Type', 'application/json');
                res.send(JSON.stringify(rows));

    connection.query('USE ' + dbconfig.database);

        });

});

////////GUARDAR UNA NUEVA ASIGNACION DE PROMOCION///////
app.post(promocion_asg , function(req,res,done){
    var name_bd = req.params.name_bd;
    var data = {//obtener los datos
        fecha:req.body.fecha,
        id_promocion:req.body.id_promocion,
        fechaFin:req.body.fechaFin
     };

    //inserting into mysql
    connection.query('USE ' + name_bd);
    connection.query("INSERT INTO asignarpromocion set ? ",data, function(err, rows){
           if(err){
                console.log(err);
                return done("Mysql error, check your query");
           }
          //devuelve el id de la fila insertada
          res.send(JSON.stringify(rows.insertId));
          connection.query('USE ' + dbconfig.database);
        });
});

////////// BUSCAR PRODUCTOS /////////////
var productos = '/admin/productos/:cod_nombre/:name_bd';
//get data to update
app.get( productos , function(req,res,done){
    var cod_nombre = req.params.cod_nombre;
    var name_bd = req.params.name_bd;
    //select en bd mysql
    connection.query('USE ' + name_bd);
    connection.query("SELECT *  FROM  producto WHERE nom_prod LIKE ? OR cod_barras_prod LIKE ? ", ['%'+cod_nombre+'%' , '%'+cod_nombre+'%']  ,function(err,rows){

               if(err){// error en consulta
                console.log(err);
                return done("Mysql error, check your query");
            }

            if(rows.length < 1)
                return res.send("Productos Not found");

            res.header('Access-Control-Allow-Origin', "*");
            res.set('Content-Type', 'application/json');
            res.send(JSON.stringify(rows)); //enviar productos encontrados
    connection.query('USE ' + dbconfig.database);
        });
});

////////// GUARDAR PRODUCTO /////////////
var product = '/admin/productos/:name_bd';

app.post( product , function(req,res,done){
    
    var name_bd = req.params.name_bd;
    //select en bd mysql
    //grabar imagen
    if (req.body.imagen){
      var obj_img = getImgToSave(req.body.imagen);
      saveimg(obj_img);
      req.body.imagen = obj_img.nombre +"."+obj_img.extension;
    }
    connection.query('USE ' + name_bd);
    connection.query("INSERT INTO producto set ? ",req.body, function(err, rows){
      if(err){// error en consulta
        console.log(err);
        return done("Mysql error, check your query");
      }
        res.send(200);   
        connection.query('USE ' + dbconfig.database);
    });
});

function saveimg(obj_img){
  
  fs.writeFile(obj_img.url, obj_img.imagen, 'base64', function(err) {
      if(err) {
          console.log("error al guardar imagen" , err);
      }else{
        console.log("guardado correcto");
    }
        });
}

        
        
function getImgToSave(raw_img){
    var img_array = raw_img.split(';base64,');
    var data_imagen = img_array[1]; // data de la imagen
    var ext = img_array[0].split("/")[1].toLowerCase(); // extension de la imagen
    ext = ext === 'jpeg' ? "jpg" : ext;
    var nombre_imagen = uuid.v1();
    var url = cargarUrl() + path.sep + nombre_imagen + "." + ext;

    return {
        "imagen": data_imagen,
        "extension": ext,
        "url": url,
        "nombre": nombre_imagen
    };
}

function cargarUrl(){
  var url = path.join(__dirname, '../public/img/files');
  return url;
  //return process.env[(process.platform == 'win32') ? 'USERPROFILE' : 'HOME'];
}

app.put( product , function(req,res,done){
    
    if (req.body.imagen){
      var obj_img = getImgToSave(req.body.imagen);
      saveimg(obj_img);
      req.body.imagen = obj_img.nombre +"."+obj_img.extension;
    }

    var name_bd = req.params.name_bd;
    //select en bd mysql
    connection.query('USE ' + name_bd);
    connection.query("UPDATE producto set ? WHERE codigo = ? ",[req.body , req.body.codigo], function(err, rows){
    //connection.query("INSERT INTO producto set ? ",req.body, function(err, rows){
      if(err){// error en consulta
        console.log(err);
        return done("Mysql error, check your query");
      }

    //res.send(200);   
    return res.json({msg: "Ok", imagen : req.body.imagen})
    connection.query('USE ' + dbconfig.database);
        });
});

//relacion de muchos a muchos

var promocion_asg_relaciones = '/admin/productos/relacionespromocionasg';

/*app.post(promocion_asg_relaciones , function(req,res,done){

    var data = { //obtener  los datos de la bd
        fk_asignar_promocion:req.body.id_asignar_promocion,
        fk_producto:req.body.id_producto,
     };

    //inserting into mysql
    connection.query("INSERT INTO promoasg_productos set ? ",data, function(err, rows){
           if(err){ // error en consulta
                console.log(err);
                return done("Mysql error, check your query");
           }
          //devuelve el id de la fila insertada
          res.send(JSON.stringify(rows.insertId));
        });
});*/

//seleecionar los productos que le corresponden a una asignacion de promocion
var promocion_asg_relaciones_id = '/admin/productos/relacionespromocionasg/:id/:name_bd';

app.post(promocion_asg_relaciones_id , function(req,res,done){

    var name_bd = req.params.name_bd;

    var data = { //obtener  los datos de la bd
        fk_asignar_promocion:req.body.id_asignar_promocion,
        fk_producto:req.body.id_producto,
     };

    //inserting into mysql
    connection.query('USE ' + name_bd);
    connection.query("INSERT INTO promoasg_productos set ? ",data, function(err, rows){
           if(err){ // error en consulta
                console.log(err);
                return done("Mysql error, check your query");
           }
          //devuelve el id de la fila insertada
          res.send(JSON.stringify(rows.insertId));
          connection.query('USE ' + dbconfig.database);
        });
});

//obetner los productos en asignacion de promocion
app.get(promocion_asg_relaciones_id , function(req,res,done){
    var asg_promocion_id = req.params.id;
    var name_bd = req.params.name_bd;

    connection.query('USE ' + name_bd);
    connection.query("SELECT promo.nombre,  promo.id_promocion , pasgp.id_asgpromo_product , asgp.fecha, asgp.fechaFin,  p.codigo , p.imagen ,p.nom_prod , p.pvp_prod , p.cod_barras_prod , p.imagen  FROM  producto p , promoasg_productos pasgp , asignarpromocion asgp , promocion promo WHERE  asgp.id_asg_promocion = ? AND asgp.id_asg_promocion  = pasgp.fk_asignar_promocion AND p.codigo = pasgp.fk_producto AND asgp.id_promocion = promo.id_promocion", [asg_promocion_id]  ,function(err,rows){
         if(err){
                console.log(err);
                return done("Mysql error, check your query");
            }
            //if user not found
            if(rows.length < 1)
                return res.send("Re Not found");
            res.header('Access-Control-Allow-Origin', "*");
            res.set('Content-Type', 'application/json');
            res.send(JSON.stringify(rows));
            connection.query('USE ' + dbconfig.database);
        });
});

//actualizar la asignacion de promcion la fecha o la promocion

app.put(promocion_asg_relaciones_id , function(req,res,done){
    var id_asg_promocion = req.params.id;
    var name_bd = req.params.name_bd;

// obtener datos
    var data = {
        fecha:req.body.fecha,
        fechaFin:req.body.fechaFin,
        id_promocion:req.body.id_promocion
     };

    //inserting into mysql
    connection.query('USE ' + name_bd);
    connection.query("UPDATE asignarpromocion SET ? WHERE id_asg_promocion = ? ",[data , id_asg_promocion], function(err, rows){
           if(err){
                console.log(err);
                return done("Mysql error, check your query");
           }
          //res.send(200);
          res.send(200);
    connection.query('USE ' + dbconfig.database);
        });
});

app.delete( promocion_asg_relaciones_id  , function(req,res,done){
    var id_asgpromo_product = req.params.id;
    var name_bd = req.params.name_bd;

     connection.query('USE ' + name_bd);
     connection.query("DELETE FROM promoasg_productos  WHERE id_asgpromo_product = ? ",[id_asgpromo_product], function(err, rows){
             if(err){
                console.log(err);
                return done("Mysql error, check your query");
             }
             res.send(200);
        connection.query('USE ' + dbconfig.database);

        });
});

//ASG ID
var promocion_asg_id = '/admin/productos/promocionasgid/:id/:name_bd';

//delete Promocion
app.delete( promocion_asg_id , function(req,res,done){
    var id_asg_promocion = req.params.id;
    var name_bd = req.params.name_bd;
    connection.query('USE ' + name_bd);

     connection.query("DELETE FROM asignarpromocion  WHERE id_asg_promocion = ? ",[id_asg_promocion], function(err, rows){

             if(err){
                console.log(err);
                return done("Mysql error, check your query");
             }
             res.send(rows);
        connection.query('USE ' + dbconfig.database);
        });
});

//obetner una promocion seleccionada (parece que no esta siendo utilizanda)
app.get( promocion_asg_id, function(req,res,done){
    var asg_promocion_id = req.params.id; // obtener el id
    connection.query("SELECT asgp.id_asg_promocion , asgp.fecha , pr.nombre FROM  asignarpromocion asgp , promocion pr   WHERE asgp.id_asg_promocion = ?", [asg_promocion_id] ,function(err,rows){
         if(err){// en caso dse error
                console.log(err);
                return done("Mysql error, check your query");
            }

            //if user not found
            if(rows.length < 1)
                return res.send("Promocion Asiganda Not found");

                res.header('Access-Control-Allow-Origin', "*");
                res.set('Content-Type', 'application/json');
                res.send(JSON.stringify(rows));
        });
});

/////////UBICACION/////////////////
var ubicacion_web = '/web/productos/ubicacion/:name_bd';

//get data to update
app.get( ubicacion_web , function(req,res,done){
  var name_bd = req.params.name_bd;
  connection.query('USE ' + name_bd);

  connection.query("SELECT * FROM  ubicacion u  , seccion s WHERE u.fk_seccion = s.id  ORDER BY cod_ubicacion DESC", [] ,function(err,rows){
            if(err){
                console.log(err);
                return done("Mysql error, check your query");
            }
            //if promocion no encontrada
            if(rows.length < 1)
                return res.send(JSON.stringify({'mensaje' : "Ubicacion Not found"}));
                res.header('Access-Control-Allow-Origin', "*");
                res.set('Content-Type', 'application/json');
                res.send(JSON.stringify(rows));
        connection.query('USE ' + dbconfig.database);
        });

    });

/////////GUARDAR UNA NUEVA ubicacion///////
app.post(ubicacion_web , function(req,res,done){
    //get data
    var name_bd = req.params.name_bd;
    var data = {
        corredor:req.body.corredor,
        numeroEstante:req.body.numeroEstante,
        pasillo:req.body.pasillo,
        numeroPiso:req.body.numeroPiso,
        fk_seccion:req.body.fk_seccion
     };
    
    //inserting into mysql
    connection.query('USE ' + name_bd);
    connection.query("INSERT INTO ubicacion set ? ",data, function(err, rows){

           if(err){ // en caso de error
                console.log(err);
                return done("Mysql error, check your query");
           }
          //devuelve el id de la fila insertada
          res.send(JSON.stringify(rows.insertId));
    connection.query('USE ' + dbconfig.database);
        });

     });

/////////UBICACION + ID/////////////////
var ubicacion_web_id = '/web/productos/ubicacion/:id/:name_bd';

app.get(ubicacion_web_id , function(req,res,done){
    var ubicacion_id = req.params.id;
    var name_bd = req.params.name_bd;
    connection.query('USE ' + name_bd);
    connection.query("SELECT * FROM  ubicacion u , seccion s WHERE u.cod_ubicacion = ? AND u.fk_seccion = s.id ", [ubicacion_id] ,function(err,rows){

            if(err){
                console.log(err);
                return done("Mysql error, check your query");
            }

            //if ubicacion not found
            if(rows.length < 1)
                return res.send("ubicacion Not found");

                res.header('Access-Control-Allow-Origin', "*");
                res.set('Content-Type', 'application/json');
                res.send(JSON.stringify(rows));
                connection.query('USE ' + dbconfig.database);
        });
    });
//eliminar ubicacion
app.delete(ubicacion_web_id , function(req,res,done){
    var name_bd = req.params.name_bd;
    var codigo_ubicacion = req.params.id;
     connection.query('USE ' + name_bd);
     connection.query("DELETE FROM ubicacion  WHERE cod_ubicacion = ? ",[codigo_ubicacion], function(err, rows){
             if(err){
                console.log(err);
                return done("Mysql error, check your query");
             }
             res.send(200);
             connection.query('USE ' + dbconfig.database);
        });
     });



///// actualizar ubicacion
app.put(ubicacion_web_id , function(req,res,done){
    var id_ubicacion = req.params.id;
    var name_bd = req.params.name_bd;
    
    //get data
    var data = {
        corredor:req.body.corredor,
        numeroEstante:req.body.numeroEstante,
        numeroPiso:req.body.numeroPiso,
        pasillo:req.body.pasillo,
        fk_seccion:req.body.fk_seccion
     };

    //inserting into mysql
    connection.query('USE ' + name_bd);
    connection.query("UPDATE ubicacion set ? WHERE cod_ubicacion = ? ",[data,id_ubicacion], function(err, rows){
           if(err){
                console.log(err);
                return done("Mysql error, check your query");
           }
          res.send(200);
          connection.query('USE ' + dbconfig.database);
        });
     });

/*************************UBICACIONES ASG**************************/
var ubicacion_asg = '/web/productos/ubicacionasg/:name_bd';

//asigar una ubicacion a un producto
app.put( ubicacion_asg , function(req,res,done){
    //var id_productos_ubicacion = req.params.id;
    //get data
    var name_bd = req.params.name_bd;
    var codigo=req.body.codigo;
    var ubicacion_id=req.body.ubicacion_id;
    //inserting into mysql
    connection.query('USE ' + name_bd);
    connection.query("UPDATE producto set ubicacion_id = ? WHERE codigo = ? ",[ubicacion_id,codigo], function(err, rows){
           if(err){
                console.log(err);
                return done("Mysql error, check your query");
           }
          res.send(200);
          connection.query('USE ' + dbconfig.database);
        });
     });


var ubicacion_asg_id = '/web/productos/ubicacionasg/:id/:name_bd';

//obtener todos los productos que tienen la ubicacion asiganda

app.get(ubicacion_asg_id , function(req,res,done){
    var id_ubicacion = req.params.id;
    var name_bd = req.params.name_bd;

    connection.query('USE ' + name_bd);
    connection.query("SELECT   imagen ,codigo , nom_prod , cod_barras_prod , ubicacion_id FROM  producto WHERE  ubicacion_id = ? ", [id_ubicacion] ,function(err,rows){

            if(err){
                console.log(err);
                return done("Mysql error, check your query");
            }
            //if promocion no encontrada
            if(rows.length < 1)
                return res.send(rows.length);
                res.header('Access-Control-Allow-Origin', "*");
                res.set('Content-Type', 'application/json');
                res.send(JSON.stringify(rows));
      connection.query('USE ' + dbconfig.database);
        });

    });

//en caso de borrar un producto de una ubicacion

app.put(ubicacion_asg_id , function(req,res,done){
 var codigo = req.params.id; //coge el parametro  que me llega por la url
 var name_bd = req.params.name_bd;
    //obtener los datos
    var data = {
       ubicacion_id:req.body.ubicacion_id
     };
    //insertar en mysql
    connection.query('USE ' + name_bd);
    connection.query("UPDATE producto SET ubicacion_id = NULL  WHERE codigo = ? ",[codigo], function(err, rows){
           if(err){
                console.log(err);
                return done("Mysql error, check your query");
           }
          res.send(200);
          connection.query('USE ' + dbconfig.database);
        });
});

////////// BUSCAR PRODUCTOS  PARA ASIGNARLES UNA UBICACION/////////////

var prod_asg_ubi = '/web/productos/productosasg/:cod_nombre/:name_bd';

//get data to update
app.get(prod_asg_ubi , function(req,res,done){
    //var ubicacion_id = req.params.ubicacion;
    var cod_nombre = req.params.cod_nombre;
    var name_bd = req.params.name_bd;
    connection.query('USE ' + name_bd);
    connection.query("SELECT imagen , codigo , nom_prod , pvp_prod , cod_barras_prod  , imagen FROM  producto WHERE  ubicacion_id IS NULL AND nom_prod LIKE ?  OR cod_barras_prod LIKE ? ", ['%'+cod_nombre+'%' , '%'+cod_nombre+'%']  ,function(err,rows){
               if(err){
                console.log(err);
                return done("Mysql error, check your query");
            }
            //if user not found
            if(rows.length < 1)
                return done("Producto Not found");

            //res.render('edit',{title:"Edit user",data:rows});

                res.header('Access-Control-Allow-Origin', "*");
                res.set('Content-Type', 'application/json');
                res.send(JSON.stringify(rows));
          connection.query('USE ' + dbconfig.database );
        });

    });



//cargar productos que no  tienen una ubicacion

var prod_asg_sin_ubi = '/web/productos/productosasg/:name_bd';

//get data
app.get(prod_asg_sin_ubi , function(req,res,done){
    //var ubicacion_id = req.params.ubicacion;
    
    var cod_nombre = req.params.cod_nombre;
    var name_bd = req.params.name_bd;
    connection.query('USE ' + name_bd);
    connection.query("SELECT codigo , nom_prod , pvp_prod , cod_barras_prod FROM  producto WHERE  ubicacion_id IS NULL ", []  ,function(err,rows){

               if(err){
                console.log(err);
                return done("Mysql error, check your query");
            }

            //if user not found
            if(rows.length < 1)
                return res.send("Producto Not found");

            //res.render('edit',{title:"Edit user",data:rows});

                res.header('Access-Control-Allow-Origin', "*");
                res.set('Content-Type', 'application/json');
                res.send(JSON.stringify(rows));
          connection.query('USE ' + dbconfig.database );
        });

    });

//obtener todos los productos que no tinen ubicacion

var prod_asg_sin_ubi_cantidad = '/productos/productosasg/cantidad/:name_bd';
app.get(prod_asg_sin_ubi_cantidad , function(req,res,done){
    //var ubicacion_id = req.params.ubicacion;
    var name_bd = req.params.name_bd;

    connection.query('USE ' + name_bd);
    connection.query("SELECT COUNT(codigo) as cantidad FROM  producto WHERE  ubicacion_id IS NULL ", []  ,function(err,rows){

               if(err){
                console.log(err);
                return done("Mysql error, check your query");
            }

            //if user not found
            if(rows.length < 1)
                return res.send("Producto Not found");

            //res.render('edit',{title:"Edit user",data:rows});

                res.header('Access-Control-Allow-Origin', "*");
                res.set('Content-Type', 'application/json');
                res.send(JSON.stringify(rows));
                connection.query('USE ' + dbconfig.database);
        });

    });

/*===========================================================FIN SECCION WEB ==========================================*/

//busca productos por el nombre ingresado

//now for Single route (GET,DELETE,PUT)
var productosbuscados = '/productos/getProductos/:texto/:name_bd';

//get data to update
app.get(productosbuscados , function(req,res,done){

    var texto = req.params.texto;
    var name_bd = req.params.name_bd;

    connection.query('USE ' + name_bd);
    connection.query("SELECT codigo, imagen, nom_prod, pvp_prod, ubicacion_id FROM producto WHERE nom_prod LIKE ? AND cant_prod > 5", '%'+[texto]+'%'  ,function(err,rows){

            if(err){
                console.log(err);
                return done("Mysql error, check your query");
            }
            //if user not found
            if(rows.length < 1)
                return res.send(JSON.stringify(400));

            //res.render('edit',{title:"Edit user",data:rows});

                res.header('Access-Control-Allow-Origin', "*");
                res.set('Content-Type', 'application/json');
                res.send(JSON.stringify(rows));

      connection.query('USE ' + dbconfig.database);
        });

    });


//perfil de usuario

var url_perfil = '/perfil/usuario/:username';
app.get(url_perfil , function(req,res,done){
    var username = req.params.username;
    connection.query("SELECT id , username , email , nombres , apellidos FROM  users WHERE username = ?", [username] ,function(err,rows){

            if(err){
                console.log(err);
                return done("Mysql error, check your query");
            }
            //if user not found
            if(rows.length < 1)
                return res.send("User Not found");

            //res.render('edit',{title:"Edit user",data:rows});

                res.header('Access-Control-Allow-Origin', "*");
                res.set('Content-Type', 'application/json');
                res.send(JSON.stringify(rows));

        });

    });

//cuando es put le envio el id del usuario cargado
app.put(url_perfil , function(req,res,done){
    var user_id = req.params.username;

    //validation
    //get data
    var fecha_actual= new Date();

    var data = {
        username:req.body.username,
        email:req.body.email,
        nombres:req.body.nombres,
        apellidos:req.body.apellidos,
        fecha_modificacion:fecha_actual
     };

    //inserting into mysql
    connection.query("UPDATE users set ? WHERE id = ? ",[data,user_id], function(err, rows){
           if(err){
                console.log(err);
                return done("Mysql error, check your query");
           }
          res.send(200);
        });
     });

//fin perfil usuario


//cuando se cambian los datos por primera vez de recuperar login actualizar id 
var url = updateperfil = '/updaterecuperacion'
app.post(updateperfil , function(req,res,done){
    var id = req.body.id;
    var recuperacion_id = req.body.recuperacion_id;
   
    //inserting into mysql
    connection.query("UPDATE users set recuperacion_id = ? WHERE id = ? ",[recuperacion_id , id], function(err, rows){
           if(err){
                console.log(err);
                return done("Mysql error, check your query");
           }
          res.send(200);
        });
     });


//update data
app.put(productosbuscados , function(req,res,done){
    var user_id = req.params.id;

    //validation
    req.assert('nombre','Poner nombre').notEmpty();
    req.assert('apellidos','poner apellidos').notEmpty();
    req.assert('edad','Poner edad en numeros').notEmpty();


    //get data
    var data = {
        nombre:req.body.nombre,
        apellidos:req.body.apellidos,
        edad:req.body.edad
     };

    //inserting into mysql
    connection.query("UPDATE empleados set ? WHERE id = ? ",[data,user_id], function(err, rows){

           if(err){
                console.log(err);
                return done("Mysql error, check your query");
           }

          res.send(200);

        });

     });



//delete data
app.delete(productosbuscados , function(req,res,done){

    var user_id = req.params.id;

     connection.query("DELETE FROM empleados  WHERE id = ? ",[user_id], function(err, rows){

             if(err){
                console.log(err);
                return done("Mysql error, check your query");
             }

             res.send(200);

        });
        //console.log(query.sql);
});


 // =========================================================================
    // UPDATE PASSWORD ============================================================
    // =========================================================================
    // actualiza la contraseña


app.put('/password/update' , function(req,res,done){

  //  var username = req.body.nombre;
    var username = req.body.username;
    var password = req.body.password;
    
    var contrasenia = bcrypt.hashSync(password, null, null)//encriptar la contrasenia
        var data = {
        username:req.body.username,
        password: contrasenia
     };

    //insertar en mysql
    connection.query("UPDATE users set ? WHERE username = ?",[ data , username], function(err, rows) {
           if(err){
                console.log(err);
                return done("Mysql error, check your query");
           }
          res.send(200);
        });
});


//recuperacion de contraseña

var url_recuperacion_clave = '/inicio/recuperacion';
app.post(url_recuperacion_clave , function(req,res,done){

var username =req.body.nombre_usuario;
var nombre_abuelo = req.body.nombre_abuelo;
var artista_favorito = req.body.artista_favorito;
var comida_favorita = req.body.comida_favorita;
var color_favorito = req.body.color_favorito;
var pasatiempo = req.body.pasatiempo;

//connection.query("SELECT p.nom_prod, p.pvp_prod, promo.nombre, asp.fecha, asp.fechaFin FROM asignarpromocion asp, promocion promo, promoasg_productos prop, producto p WHERE asp.fechaFin >= CURRENT_DATE and asp.fecha <= CURRENT_DATE and promo.id_promocion = asp.id_promocion and prop.fk_asignar_promocion = asp.id_asg_promocion and p.codigo = prop.fk_producto" , [] ,function(err,rows){
    //inserting into mysql
    connection.query("SELECT us.username , rc.nombre_abuelo , rc.artista_favorito, rc.comida_favorita , rc.color_favorito , rc.pasatiempo FROM users us , recuperar_clave rc WHERE us.username = ? AND rc.id = us.recuperacion_id",[username], function(err, rows) {
     // connection.query("SELECT * FROM recuperar_clave ",[], function(err, rows) {
           if(err){
                console.log(err);
                return done("Mysql error, check your query");
           }



            if (rows.length<1) {
                return res.send(JSON.stringify({"mensaje_inf" : "Usuario no encontrado"}));
                    /*return done('Usuario no encontrado');
                    /* res.status(400).send('Something broke!');*/
                } else {
                  //obtener los valores y comprar
                 if(rows[0].nombre_abuelo != nombre_abuelo)
                    return res.send(JSON.stringify({"mensaje_inf" : "Nombre de abuelo incorrecto"}));


                   if(rows[0].artista_favorito != artista_favorito)
                    return res.send(JSON.stringify({"mensaje_inf" : "Artista favorito incorrecto"}));

                   if(rows[0].comida_favorita != comida_favorita)
                    return res.send(JSON.stringify({"mensaje_inf" : "Comida favorita incorrecta"}));

                   if(rows[0].color_favorito != color_favorito)
                    return res.send(JSON.stringify({"mensaje_inf" : "Color favorito incorrecto"}));

                   if(rows[0].pasatiempo != pasatiempo)
                    return res.send(JSON.stringify({"mensaje_inf" : "Pasatiempo incorrecto"}));




               res.send(JSON.stringify(rows));



              }


          //devuelve el id de la fila insertada
          //res.send(JSON.stringify(rows.insertId));
        });
});

//supermercado


var url_supermercado_index = '/index/supermercado/'
app.get(url_supermercado_index , function(req,res,done){
  //en este caso en parametro se enviara el nombre de usuario
    var username = req.params.parametro;
    connection.query("SELECT nombre , direccion  FROM supermercado  ", [] ,function(err,rows){

            if(err){
                console.log(err);
                return done("Mysql error, check your query");
            }
            //if user not found
            if(rows.length < 1)
                return res.send("Supermercado Not found");

                res.header('Access-Control-Allow-Origin', "*");
                res.set('Content-Type', 'application/json');
                res.send(JSON.stringify(rows));

        });

    });



var url_supermercado = '/web/supermercado/:parametro/:parametro1'
app.get(url_supermercado , function(req,res,done){
  //en este caso en parametro se enviara el nombre de usuario
    var username = req.params.parametro;
    var name_bd = req.params.parametro1;
    
    connection.query('USE ' + name_bd);
    
    //connection.query("SELECT s.id , s.nombre , s.direccion , s.telefonos, s.mision, s.vision , s.ruc  , s.longitud , s.latitud FROM  users u ,  supermercado s WHERE u.username = ? AND u.id = s.id_admin", [username] ,function(err,rows){
    connection.query("SELECT * FROM supermercado", [username] ,function(err,rows){

            if(err){
                console.log(err);
                return done("Mysql error, check your query");
            }
            //if user not found
            if(rows.length < 1)
                return res.send("Supermercado Not found");

                res.header('Access-Control-Allow-Origin', "*");
                res.set('Content-Type', 'application/json');
                res.send(JSON.stringify(rows));

        });

    connection.query('USE ' + dbconfig.database);

    });

//Editar informacion supermercado
app.put(url_supermercado , function(req,res,done){

    var name_bd = req.params.parametro1;
    connection.query('USE ' + name_bd);
  //en este caso en parametro se enviara el id del supermercado a editar
 var id = req.params.parametro; //recibe el parametro  que me llega por la url

    //obtener los datos
    var data = {
        nombre:req.body.nombre,
        direccion:req.body.direccion,
        telefonos:req.body.telefonos,
        mision:req.body.mision,
        vision:req.body.vision,
        ruc:req.body.ruc,
        longitud:req.body.longitud,
        latitud:req.body.latitud
     };
    //insertar en mysql
    connection.query("UPDATE supermercado set ? WHERE id = ? ",[data , id], function(err, rows){
           if(err){ //error en consulta
                console.log(err);
                return done("Mysql error, check your query");
           }
          connection.query('USE ' + dbconfig.database);
          res.send(200);//enviar todo Ok
        });
});

var url_paypal = '/web/paypal/:name_bd/:id_super';

//Editar Paypal supermercado
app.put(url_paypal , function(req,res,done){

    var name_bd = req.params.name_bd;
    connection.query('USE ' + name_bd);
    var id = req.params.id_super; //recibe el parametro  que me llega por la url

    //insertar en mysql
    connection.query("UPDATE supermercado set ? WHERE id = ? ",[req.body , id], function(err, rows){
           if(err){ //error en consulta
                console.log(err);
                return done("Mysql error, check your query");
           }
          connection.query('USE ' + dbconfig.database);
          res.send(200);//enviar todo Ok
        });
});

var entregarLista = '/web/entregarlista/:name_bd/:id_super';

//Editar la cantidad de entregar lista
app.put(entregarLista , function(req,res,done){

    var name_bd = req.params.name_bd;
    connection.query('USE ' + name_bd);
    var id = req.params.id_super; //recibe el parametro  que me llega por la url

    //insertar en mysql
    connection.query("UPDATE supermercado set ? WHERE id = ? ",[req.body , id], function(err, rows){
           if(err){ //error en consulta
                console.log(err);
                return done("Mysql error, check your query");
           }
          connection.query('USE ' + dbconfig.database);
          res.send(200);//enviar todo Ok
        });
});

//datos de recuperar clave en perfil (edicion)

var url_datos_recuperar = '/web/datosRecuperar/:parametro'

app.get(url_datos_recuperar , function(req,res,done){
  //en este caso en parametro se enviara el nombre de usuario
    var name_bd = req.params.name_bd;
    var username = req.params.parametro;
    connection.query("SELECT rc.id , rc.nombre_abuelo, rc.artista_favorito , rc.comida_favorita, rc.color_favorito, rc.pasatiempo FROM  recuperar_clave rc ,  users u WHERE u.username = ? AND u.recuperacion_id = rc.id", [username] ,function(err,rows){

            if(err){
                console.log(err);
                return done("Mysql error, check your query");
            }
            //if user not found
            if(rows.length < 1)
                return res.send(400);

                res.header('Access-Control-Allow-Origin', "*");
                res.set('Content-Type', 'application/json');
                res.send(JSON.stringify(rows));
        });

    });

//Editar informacion campos recuperacion de clave
app.put(url_datos_recuperar , function(req,res,done){
  //en este caso en parametro se enviara el id a editar
 var id = req.params.parametro; //recibe el parametro  que me llega por la url

    //obtener los datos
    var data = {
        nombre_abuelo:req.body.nombre_abuelo,
        artista_favorito:req.body.artista_favorito,
        comida_favorita:req.body.comida_favorita,
        color_favorito:req.body.color_favorito,
        pasatiempo:req.body.pasatiempo

     };
    //insertar en mysql
    connection.query("UPDATE recuperar_clave set ? WHERE id = ? ",[data , id], function(err, rows){
           if(err){ //error en consulta
                console.log(err);
                return done("Mysql error, check your query");
           }
          res.send(200);//enviar todo Ok
        });
});

var datos_recuperar = '/web/datosRecuperar/';

app.put(datos_recuperar , function(req,res,done){
      //obtener los datos
    var data = {
        nombre_abuelo:req.body.nombre_abuelo,
        artista_favorito:req.body.artista_favorito,
        comida_favorita:req.body.comida_favorita,
        color_favorito:req.body.color_favorito,
        pasatiempo:req.body.pasatiempo

     };
    //insertar en mysql
    connection.query("INSERT INTO recuperar_clave set ? ",[data], function(err, rows){
           if(err){ //error en consulta
                console.log(err);
                return done("Mysql error, check your query");
           }
          //res.send(rows);//enviar todo Ok
          res.send(JSON.stringify(rows.insertId));
        });
});


/*=============================seccion de ubicacion =====================*/



//obtener todas las secciones

var url_seccion = '/web/secciones/:name_bd'
app.get(url_seccion , function(req,res,done){
  //consulta para obtener todas las secciones
    var name_bd = req.params.name_bd;
    connection.query('USE ' + name_bd);
    connection.query("SELECT * FROM seccion ", [] ,function(err,rows){

            if(err){
                console.log(err);
                return done("Mysql error, check your query");
            }
            //if user not found
            if(rows.length < 1)
                return res.send(JSON.stringify({"mensaje" : "No tiene secciones"}));

                res.header('Access-Control-Allow-Origin', "*");
                res.set('Content-Type', 'application/json');
                res.send(JSON.stringify(rows));
        connection.query('USE ' + dbconfig.database);
        });

    });


////////GUARDAR UNA NUEVA SECCION///////
app.post(url_seccion , function(req,res,done){
    var name_bd = req.params.name_bd;
    
    //get data
    var data = {
        nombre:req.body.nombre
     };
    //inserting into mysql
    connection.query('USE ' + name_bd);
    connection.query("INSERT INTO seccion set ? ", data , function(err, rows){

           if(err){
                console.log(err);
                return done("Mysql error, check your query");
           }
          //devuelve el id de la fila insertada
          res.send(JSON.stringify(rows.insertId));
    connection.query('USE ' + dbconfig.database);
        });

});


/////////////SECCION ID////////////////////
var seccionId = '/web/secciones/:seccion_id/:name_bd';

//obtener una seccion en base a un ID
app.get(seccionId , function(req,res,done){
    var seccion_id = req.params.seccion_id;
    var name_bd = req.params.name_bd;
    connection.query('USE ' + name_bd);
    connection.query("SELECT * FROM  seccion WHERE id = ?", [seccion_id] ,function(err,rows){
            if(err){// si encuentra error en consulta
                console.log(err);
                return done("Mysql error, check your query");
            }
            //if user not found
            if(rows.length < 1)// si el resultado esta en cero no encontro valores
                return res.send("Seccion Not found");
            res.send(JSON.stringify(rows));//enviar los datos encontrados
      connection.query('USE ' + dbconfig.database);
        });
    });

//Editar Seccion Seleccionada
app.put(seccionId , function(req,res,done){
 var seccion_id = req.params.seccion_id; //recibe el parametro  que me llega por la url
 var name_bd = req.params.name_bd;
 
    //obtener los datos
    var data = {
         nombre:req.body.nombre
     };
    //insertar en mysql
    connection.query('USE ' + name_bd);
    connection.query("UPDATE seccion set ? WHERE id = ? ",[data , seccion_id], function(err, rows){
           if(err){ //error en consulta
                console.log(err);
                return done("Mysql error, check your query");
           }
          res.send(200);//enviar todo Ok
        });
    connection.query('USE ' + dbconfig.database);
});

//ELIMINAR SECCION SELECIONADA (Elimina la seccion y la asignacion que le pertenece DELETE CASCADE)
app.delete(seccionId , function(req,res,done){
     var seccion_id = req.params.seccion_id;
     var name_bd = req.params.name_bd;
     connection.query('USE ' + name_bd);
     connection.query("DELETE FROM seccion  WHERE id = ? ",[seccion_id], function(err, rows){
             if(err){ //error en consulta
                console.log(err);
                return done("Mysql error, check your query");
             }
             res.send(200);//enviar todo ok
            connection.query('USE ' + dbconfig.database);
        });
});

/*verificar si la seccion se encuentra asignada*/


var seccion_asginada = '/seccion/asignada_ubicacion/:parametro/:name_bd';
app.get( seccion_asginada , function(req,res,done){
var id_seccion = req.params.parametro;
var name_bd = req.params.name_bd;

  connection.query('USE ' + name_bd);
    connection.query("SELECT cod_ubicacion FROM  ubicacion  WHERE fk_seccion = ?  " , [id_seccion] ,function(err,rows){
       if (err)
        return done(err);
      if (!rows.length)
        return done(null, false, req.flash('message', 'Mysql error, check your query"'));
      if(rows.length < 1)
        return done(nll, false, req.flash('message', 'seccion no encontrada'));
      res.send(JSON.stringify(rows));
      connection.query('USE ' + dbconfig.database);
    });
});

//fin del web serivice



//seccion movil
//supermercado

var url_supermercado_movil = '/web/supermercadomovil/:name_bd';
app.get(url_supermercado_movil , function(req,res,done){

    var name_bd = req.params.name_bd;
    connection.query('USE ' + name_bd);
    connection.query("SELECT * FROM supermercado  ", [] ,function(err,rows){

      if(err){
          console.log(err);
          return done("Mysql error, check your query");
      }

      res.header('Access-Control-Allow-Origin', "*");
      res.set('Content-Type', 'application/json');
      res.send(JSON.stringify(rows));
    connection.query('USE ' + dbconfig.database);

        });

    });




};//fin metodo exports
