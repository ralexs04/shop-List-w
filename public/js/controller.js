//var compareToRol = 'superusuario'; //[administrador, superusuario]
//var rolAdministrador = 'administrador'; //[administrador, superusuario]
var promociones = [];
var secciones = [];
var asg_promo_select = []; // cuando se edita la seccion de asignacion de promociones

angular.module('starter.controllers',
                ['LocalStorageModule',
                'cgNotify',
                'angularSmartconfirm',
                '720kb.datepicker',
                'acute.select'])

        .run(function (acuteSelectService) {
            //cargar plantilla para el select
            acuteSelectService.updateSetting("templatePath", "librerias/acute.select/template");
        })
        .controller('ConfiguracionCtrl', function ($http,
                                                   $scope,
                                                   localStorageService,
                                                   $location,
                                                   notify) {
            //al iniciar se carga el formulario para el ingreso de la clave para continuar
            $scope.config = true;

            $scope.ocultarFormularioConfiguracion = function () {
                $scope.configUser = false;
                $scope.config = true;
                //borrar la contraseña del campo input
                $scope.confg.contrasenia = "";

                //ocultar datos paypal
                $scope.editar = false;
                $scope.noEditar = true;
            };
            $scope.eventChangePass = function () {
                $scope.mensaje_config = "";
            };
            //gestion campos en datos de recuperacion clave
            $scope.editar = true; //por defecto dejamos los campos desabilitados;
            $scope.habilitarCampos = function () {
                $scope.editar = false;
            };
            $scope.desabilitarCampos = function () {
                $scope.editar = true;
            };
            $scope.mostrarBotones = true;
            $scope.ShowBotones = function () {
                $scope.mostrarBotones = $scope.mostrarBotones === true ? false : true;
                $scope.mostrarBotonesOpcion = $scope.mostrarBotonesOpcion === true ? false : true;
            };

            $scope.cargarDatosRecuperacionClave = function () {
                var url = '/web/datosRecuperar/' + localStorageService.get('localStorageDemo');
                $http.get(url).success(function (data, status, headers, config) {
                    console.log("datos obtendinos" + JSON.stringify(data));
                    $scope.recuperar = data[0];
                }).error(function (data, status, headers, config) {
                    console.log("Ops: could not get any data");
                });
            };

            $scope.editarPagar = false;
            $scope.noEditarPagar = true;

            $scope.edicionPagar = function(){
                $scope.editarPagar = $scope.editarPagar === true ? false : true;
                $scope.noEditarPagar = $scope.noEditarPagar === false ? true : false;
            }

            $scope.guardarCantidadEntrega = function (_cantidad) {
                var bd_supmercado = localStorageService.get('localStorageUser').bd_supmercado;
                var data = {pagar_entrega : _cantidad}
                var url = "/web/entregarlista/"+ bd_supmercado +"/" + $scope.supermercado.id;
                $http.put(url, data).success(function (data, status, headers, config) {
                    alertaSuccess(notify, 'Guardado Satisfactoriamente ..!!');
                    $scope.supermercado.pagar_entrega = _cantidad;
                    $scope.edicionPagar();
                }).error(function (data, status, headers, config) {
                    alertaError(notify , 'Error al guardar Intente mas tarde :( ');
                    $scope.edicionPagar();
                });
            }

            //recuperar contrasenia
            $scope.enviarCorreo = function (mensaje) {
                var data = {'mensaje': mensaje};
                $http.post("/sendemail", data)
                        .success(function (data, status, headers, config) {
                            console.log(JSON.stringify(data));
                        }).error(function (data, status, headers, config) {
                    console.log("Error en : " + data);
                });
            };

            //guardar datos
            $scope.guardar_data_recuperacion = function (recuperar) {

                var url, data;
                if (!$scope.recuperar){
                    data = recuperar;
                    url = '/web/datosRecuperar/'; 
                }else{
                    url = '/web/datosRecuperar/' + $scope.recuperar.id;
                    data = $scope.recuperar;
                }
                $http.put(url, data)
                    .success(function (data, status, headers, config) {
                        //actualizar la vista en caso de guardado exitoso
                        var mensaje = "Los datos del sistema Shop-List para la recuperacion de la contraseña an sido actualizados..!! \n\
                        \n nota: No olvidar éstos datos, porque a traves de ellos le permitiran recuperar la contraseña en caso de ser olvidada..!!!."

                           //verificar si es la primera vez actualizar el id del administradpr
                           if (data !== 'OK') {
                            //enviar a actualizar el id del administrar
                            var data = {id  : localStorageService.get('localStorageUser').id, 
                                        recuperacion_id : data };
                            guardar_id_recuperar_user(data);
                           }
                        /*enviar correo al administrador*/
                        //$scope.enviarCorreo(mensaje);
                        alertaSuccess(notify , 'Guardado Satisfactoriamente ...!!!');
                    }).error(function (data, status, headers, config) {
                        alertaError(notify , 'Error al guardar Intente mas tarde :( ');
                        console.log("Ops: " + data);
                });
            };

            function guardar_id_recuperar_user(data){
                 $http.post("/updaterecuperacion", data)
                    .success(function (data, status, headers, config) {
                        console.log("se guardo correctamente" + JSON.stringify(data));
                    }).error(function (data, status, headers, config) {
                        console.log("Ops: " + data);
                });
            }

            /*login para actualizar los datos*/
            $scope.loginConfig = function () {
                var datos = {"username": localStorageService.get('localStorageDemo'),
                            "password": $scope.confg.contrasenia};
                var url = '/contrasenia';
                $http.post(url, datos)
                    .success(function (data, status, headers, config) {
                        //hacer visible el campo de configuracion
                        $scope.configUser = true;
                        $scope.config = false;

                    }).error(function (data, status, headers, config) {
                        /*mensaje de restrincion*/
                        $scope.mensaje_config = "No tiene permisos para ingresar ..!!!";
                        console.log("Error en -->> : " + data);
                });
            };

            $scope.editar = false;
            $scope.noEditar = true;
            $scope.editarAnalitica = false;
            $scope.noEditarAnalitica = true;

            $scope.edicionPaypal = function(){
                $scope.editar = $scope.editar === true ? false : true;
                $scope.noEditar = $scope.noEditar === false ? true : false;
            }
            $scope.edicionPaypalAnalitica = function(){
                $scope.editarAnalitica = $scope.editarAnalitica === true ? false : true;
                $scope.noEditarAnalitica = $scope.noEditarAnalitica === false ? true : false;
            }

            $scope.RegistrarIdPaypal = function() {
                var bd_supmercado = localStorageService.get('localStorageUser').bd_supmercado;
                var url = "/web/paypal/"+ bd_supmercado +"/" + $scope.supermercado.id;
                var data = {id_paypal : $scope.confg.id_paypal};
                
                $http.put(url, data)
                    .success(function (data, status, headers, config) {
                        alertaSuccess(notify, 'Guardado Satisfactoriamente ..!!');
                        $scope.edicionPaypal();
                        console.log("el registro a sido " , data);

                    }).error(function (data, status, headers, config) {
                        alertaError(notify, 'Error Al guardar intente mas tarde porfavor..!!');
                        $scope.edicionPaypal();
                        console.log("error" , data);
                });
            };
            $scope.RegistrarIdPaypalAnalitica = function() {
                var bd_supmercado = localStorageService.get('localStorageUser').bd_supmercado;
                var url = "/web/paypal/"+ bd_supmercado +"/" + $scope.supermercado.id;
                var data = {id_analitica_paypal : $scope.confg.id_analitica_paypal};
                
                $http.put(url, data)
                    .success(function (data, status, headers, config) {
                        alertaSuccess(notify, 'Guardado Satisfactoriamente ..!!');
                        $scope.edicionPaypalAnalitica();
                        console.log("el registro a sido " , data);

                    }).error(function (data, status, headers, config) {
                        alertaError(notify, 'Error Al guardar intente mas tarde porfavor..!!');
                        $scope.edicionPaypalAnalitica();
                        console.log("error" , data);
                });
            };

              //obtener los datos del supermercado
            $scope.get_datos_supermercado = function() {
                var bd_supmercado = localStorageService.get('localStorageUser').bd_supmercado;
                var user = localStorageService.get('localStorageDemo');
                var url = '/web/supermercado/' + user + "/" + bd_supmercado;
                console.log("enviar a ver datos del supermercado " , url);
                $http.get(url).success(function (data, status, headers, config) {
                    /*almacena los datos del supermercado*/
                    $scope.supermercado = data[0];
                    $scope.confg = {id_paypal : '' , id_analitica_paypal : '' };
                    if ($scope.supermercado.id_paypal) {
                        $scope.confg.id_paypal = $scope.supermercado.id_paypal;
                    }
                    if ($scope.supermercado.id_analitica_paypal){
                        $scope.confg.id_analitica_paypal = $scope.supermercado.id_analitica_paypal;
                    }
                                      

                }).error(function (data, status, headers, config) {
                    console.log("Ops: could not get any data");
                });
            };
        })
        /*controlador del fragmento de bienvenido*/
        .controller('BienvenidoCtrl', function ($http, $scope, localStorageService, notify) {

            $scope.cargaImagen = cargaImagen;
            // Validacion controlar ingreso de telefono correcto
            $scope.phoneNumberPattern = (function () {
                var regexp = /^\(?(\d{3})\)?[ .-]?(\d{3})[ .-]?(\d{4})$/;
                return {
                    test: function (value) {
                        if ($scope.requireTel === false) {
                            return true;
                        }
                        return regexp.test(value);
                    }
                };
            })();

            function cargaImagen(){
                try {
                    if (window.FileReader) {
                        var oFReader = new FileReader();
                        oFReader.readAsDataURL(document.getElementById("imgInput").files[0]);
                        oFReader.onload = function (oFREvent) {
                            $scope.$apply(function(){
                                $scope.imagen = oFREvent.target.result;
                                document.getElementById("imgPreview").src= $scope.imagen;
                            });
                        };
                    } else {
                        alert("Informacion",'Su navegador no soporta importación de archivos, por favor actualicelo');
                    }
                } catch (e) {
                    console.log(e);
                }
            }

            //obtener los datos del supermercado
            $scope.get_datos_supermercado = function () {
                var bd_supmercado = localStorageService.get('localStorageUser').bd_supmercado
                var user = localStorageService.get('localStorageDemo');
                var url = '/web/supermercado/' + user + "/" + bd_supmercado;

                $http.get(url).success(function (data, status, headers, config) {
                    /*almacena los datos del supermercado*/
                    $scope.supermercado = data[0];
                }).error(function (data, status, headers, config) {
                    console.log("Ops: could not get any data");
                });
            };
            //obtener los datos del supermercado para la pantalla de inicio index
            $scope.get_datos_supermercado_index = function () {
                var url = '/index/supermercado/';
                $http.get(url).success(function (data, status, headers, config) {
                    $scope.supermercadoIndex = data[0];
                }).error(function (data, status, headers, config) {
                    console.log("Ops: could not get any data");
                });
            };

            $scope.editar_nombre = function (nombre) {
                document.getElementById('nombre').value = nombre;
            };
            $scope.editar_mision = function (supermercado) {
                document.getElementById('mision').value = supermercado.mision;
            };
            $scope.editar_vision = function (supermercado) {
                document.getElementById('vision').value = supermercado.vision;
            };
            $scope.editar_info = function (supermercado) {
                document.getElementById('direccion').value = supermercado.direccion;
                document.getElementById('telefonos').value = supermercado.telefonos;
                document.getElementById('ruc').value = supermercado.ruc;
                document.getElementById('longitud').value = supermercado.longitud;
                document.getElementById('latitud').value = supermercado.latitud;
            };

            //actualizar los datos del supermecado
            $scope.update_datos_supermercado = function (supermercado, vision, mision) {
                console.log("los datos sel supermercado son " + JSON.stringify(supermercado));
                var nombre = document.getElementById('nombre').value;
                var direccion = document.getElementById('direccion').value;
                var telefonos = document.getElementById('telefonos').value;
                var ruc = document.getElementById('ruc').value;
                var latitud = document.getElementById('latitud').value;
                var longitud = document.getElementById('longitud').value;
                //controlar los valores
                if (!nombre)
                    nombre = supermercado.nombre;
                if (!vision)
                    vision = supermercado.vision;
                if (!mision)
                    mision = supermercado.mision;
                if (!direccion)
                    direccion = supermercado.direccion;
                if (!telefonos)
                    telefonos = supermercado.telefonos;
                if (!ruc)
                    ruc = supermercado.ruc;
                if (!latitud)
                    latitud = supermercado.latitud;
                if (!longitud)
                    longitud = supermercado.longitud;

                //establecer los datos a enviar en JSON
                var name_bd = localStorageService.get('localStorageUser').bd_supmercado;

                var result = {  "id": supermercado.id, 
                                "nombre": nombre, 
                                "mision": mision, 
                                "vision": vision, 
                                "direccion": direccion, 
                                "telefonos": telefonos, 
                                "ruc": ruc, 
                                "latitud": latitud, 
                                "longitud": longitud
                                };

                var url = '/web/supermercado/' + supermercado.id +  "/" + name_bd;
                $http.put(url, result)//enviar acutalizar al  web service
                    .success(function (data, status, headers, config) {
                        //actualizar la vista en caso de guardado exitoso
                        $scope.supermercado = result;
                        alertaSuccess(notify, 'Guardado Satisfactoriamente ..!!');
                    }).error(function (data, status, headers, config) {
                        alertaError(notify, 'Error al guardar intente mas tarde..!!');
                        console.log("Ops: " + data);
                });
            }

            //ocultar mision
            $scope.visible_mision_inicio = true;
            $scope.ver_mision = function () {
                $scope.visible_mision = $scope.visible_mision === true ? false : true;
                $scope.visible_mision_inicio = $scope.visible_mision_inicio === false ? true : false;
            };
            //vision
            $scope.visible_vision_inicio = true;
            $scope.ver_vision = function () {
                $scope.visible_vision = $scope.visible_vision === true ? false : true;
                $scope.visible_vision_inicio = $scope.visible_vision_inicio === false ? true : false;
            };

            //ocultar edicion informacion
            $scope.visible_edit_inf = true;
            $scope.ver_inf = function () {
                $scope.clase_fondo = 'jumbotron';
                $scope.visible_inf = $scope.visible_inf === true ? false : true;
                $scope.visible_edit_inf = $scope.visible_edit_inf === false ? true : false;
            };

            //ocultar edicion titulo
            $scope.visible_titulo_inf = true;
            $scope.ver_titulo = function () {
                $scope.visible_titulo = $scope.visible_titulo === true ? false : true;
                $scope.visible_titulo_inf = $scope.visible_titulo_inf === false ? true : false;
            };

            //cambiar de color en la edicion informacion
            $scope.clase_fondo = '';
            $scope.reset_fondo = function () {
                $scope.clase_fondo = '';
                $scope.visibleMapa = false;
            };


            $scope.loadMapa = function(){
                $scope.visibleMapa = true;
                var geocoder;

                var map = new google.maps.Map(document.getElementById('map'), {
                  center: {lat: -33.8688, lng: 151.2195},
                  zoom: 17
                });
                var input = /** @type {!HTMLInputElement} */(
                document.getElementById('pac-input'));

                var types = document.getElementById('type-selector');
                map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
                map.controls[google.maps.ControlPosition.TOP_LEFT].push(types);

                var autocomplete = new google.maps.places.Autocomplete(input);
                autocomplete.bindTo('bounds', map);

                var infowindow = new google.maps.InfoWindow();
                //var infoWindowPrincipal = new google.maps.InfoWindow({map: map});
                var newpostmarker;

                //cargar variable para geocoder
                geocoder = new google.maps.Geocoder();


                // Try HTML5 geolocation.
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function(position) {
                        var latitude = position.coords.latitude;
                        var longitude = position.coords.longitude;
                        var locCurrent = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                        cargarLatLon(locCurrent, geocoder, true);

                      /*var pos = {
                        lat: latitude,
                        lng: longitude
                      };*/

                      //infoWindowPrincipal.setPosition(pos);
                      //map.setCenter(pos);
                    }, function() {
                    //handleLocationError(true, infoWindowPrincipal, map.getCenter());
                    console.log("error 1");
                    });
                } else {
                    // Browser doesn't support Geolocation
                    //handleLocationError(false, infoWindowPrincipal, map.getCenter());
                    console.log("Browser doesn't support Geolocation");
                }

                var marker = new google.maps.Marker({
                    map: map,
                    anchorPoint: new google.maps.Point(0, -29)
                });

                autocomplete.addListener('place_changed', function() {

                    infowindow.close();
                    marker.setVisible(false);
                    var place = autocomplete.getPlace();
                    if (!place.geometry) {
                        window.alert("Autocompletar regresó a lugar principal, no contiene la geometría");
                        return;
                    }

                  // If the place has a geometry, then present it on a map.
                if (place.geometry.viewport) {
                    map.fitBounds(place.geometry.viewport);
                } else {
                    map.setCenter(place.geometry.location);
                    map.setZoom(17);  // Why 17? Because it looks good.
                }
                marker.setIcon(/** @type {google.maps.Icon} */({
                    url: place.icon,
                    size: new google.maps.Size(71, 71),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 34),
                    scaledSize: new google.maps.Size(35, 35)
                }));

                marker.setPosition(place.geometry.location);
                marker.setVisible(true);
                var latitude = place.geometry.location.lat();
                var longitude = place.geometry.location.lng();
                cargarLatLon(place.geometry.location, geocoder);

                var address = '';
                if (place.address_components) {
                address = [
                    (place.address_components[0] && place.address_components[0].short_name || ''),
                    (place.address_components[1] && place.address_components[1].short_name || ''),
                    (place.address_components[2] && place.address_components[2].short_name || '')
                    ].join(' ');
                }

                infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
                infowindow.open(map, marker);
                });

                // Sets a listener on a radio button to change the filter type on Places
                // Autocomplete.
                function setupClickListener(id, types) {
                  /*var radioButton = document.getElementById(id);
                  radioButton.addEventListener('click', function() {
                    //autocomplete.setTypes(types);
                    
                  });*/
                    autocomplete.setTypes([]);
                }

                setupClickListener('changetype-all', []);
                setupClickListener('changetype-address', ['address']);
                setupClickListener('changetype-establishment', ['establishment']);
                setupClickListener('changetype-geocode', ['geocode']);


                function placeNewPostMarker(location) {
                    if (newpostmarker) {
                        newpostmarker.setPosition(location);
                        } else {
                        newpostmarker = new google.maps.Marker({
                            position: location,
                            map:map,
                        });
                    }
                    newpostmarker.setVisible(false);
                }

                //cargar lat and long click

                google.maps.event.addListener(map, "click", function (e) {
                    //lat and lng is available in e object
                    var latLng = e.latLng;
                    var latitude = e.latLng.lat();
                    var longitude = e.latLng.lng();
                    cargarLatLon(latLng, geocoder, true);
                });

                function cargarLatLon(latLng , geocoder, estado) {

                    var latitude = latLng.lat();
                    var longitude = latLng.lng();
                    document.getElementById('longitud').value = longitude;
                    document.getElementById('latitud').value = latitude;

                    geocoder.geocode( {'latLng': latLng},
                    function(results, status) {
                        if(status == google.maps.GeocoderStatus.OK) {
                            if(results[0]) {
                                if (estado){
                                    map.setCenter(latLng);
                                    placeNewPostMarker(latLng);
                                    infowindow.setContent(results[0].formatted_address);
                                    infowindow.open(map,newpostmarker);
                                }
                            }
                            else {
                        //document.getElementById("address").value = "No results";
                                console.log("No results");
                                if (estado){
                                    //infoWindowPrincipal.setContent(results[0].formatted_address);
                                    placeNewPostMarker(latLng);
                                    infowindow.setContent('Localizacion encontrada.');
                                    infowindow.open(map,newpostmarker);
                                }
                            }
                        }
                        else {
                            //document.getElementById("address").value = status;
                            console.log(status);
                        }
                    });
                }
            }

            function handleLocationError(browserHasGeolocation, infoWindow, pos) {
                infoWindow.setPosition(pos);
                infoWindow.setContent(browserHasGeolocation ?
                                    'Error: The Geolocation service failed.' :
                                    'Error: Your browser doesn\'t support geolocation.');
            }
                
        })

        //para guardar la session en el navegador
        .controller('InicioCtrl', function ($scope, 
                                            localStorageService, 
                                            $http, 
                                            $rootScope, 
                                            notify,
                                            $timeout,
                                            $loading,
                                            $location) {
            //terminos y condiciones

            $scope.activarterminos = false;

            $scope.mostratTerminos = function () {
                $scope.activarterminos = $scope.activarterminos === true ? false : true;

            }
            //$scope.localStorageDemo = localStorageService.get('localStorageDemo');
            $scope.servicios = [{titulo : "Gestion Promociones", namespace : "promocion"},
                                {titulo : "Ubicacion de Productos", namespace : "ubicacion_producto"},
                                {titulo : "Analítica", namespace : "analitica"},
                                {titulo : "Pedidos de listas" , namespace : "listas"}]

            //validar nombre de usuario
            $scope.existeUsuario = false;
            $scope.checkboxModel = {
                value1 : true
            };

            $scope.validaUsername = function(username){
                if (username) {
                    //verificar si existe en la bd
                    $http.get("/get_user/" + username)
                        .success(function (data, status, headers, config) {
                            $scope.existeUsuario = true;
                        }).error(function (data, status, headers, config) {
                            console.log("Ops: " + data);
                            $scope.existeUsuario = false;
                    });
                }
            }
            //guardar una nueva empresa
            $scope.redirect = false;
            $scope.guardar_empresa = function (empresa , redireccionar) {
                //obtener datos del adminitrador
                redirect = redireccionar;
                var administrador = {
                    'admin_usuario'     : empresa.adminNombreUsuario,
                    'admin_apellidos'   : empresa.adminApellidos,
                    'admin_correo'      : empresa.adminCorreo,
                    'admin_nombres'     : empresa.adminNombre,
                    'admin_contrasenia' : empresa.adminPassword,
                    'admin_rol'         : rolAdministrador,
                    'bd_supmercado'     : empresa.nombrebd
                }

                //obtener datos supermercado
                 var supermercado = {
                    nombre      : empresa.nombre,
                    direccion   : empresa.ubicacion,
                    bd          : empresa.nombrebd,
                    id_paypal   : '',
                    id_analitica_paypal : ''
                 }

                if (empresa.id_paypal) {
                    supermercado.id_paypal = empresa.id_paypal;
                }  
                if (empresa.id_analitica_paypal) {
                    supermercado.id_analitica_paypal = empresa.id_analitica_paypal;
                }

                var esquema = {
                    'bd' : empresa.nombrebd
                }

                $loading.start('empresa');

                 //1) guardar el usuario administrador
                 //2) crear el esquema
                 //3) guardar el sumpermercado con el id del administrador

                 guardar_administrador(administrador, esquema, supermercado);
                 //guardarSupermercado(supermercado)
                 //esperar un tiempo para guardar el supmermercado
                 //window.setTimeout( saveSupermercado(supermercado), 7000);
            }

            var idAdministador = null;
            function saveSupermercado (supermercado){
                $http.post("/supermercados", supermercado)
                    .success(function (data, status, headers, config) {
                    console.log("supermercado guardado correctamente");
                    //guardar los servicios
                    guardar_servicio(supermercado.bd);
                                
                    }).error(function (data, status, headers, config) {
                        console.log("error al guardar");
                        console.log("Ops: " + data);
                });
            }

            $scope.mostrarInfo = function(){
                swal("Información Paypal!", "Para obtener el ID de paypal debera Registrarse en Paypal" 
                    + " en la siguiente direccion: https://www.paypal.com y al instante obtendra una cuenta para que sus clientes" 
                    + " puedan realizar el pago en linea de sus pedidos de listas. En caso de tener alguna" 
                    + " duda con esta seccion, porfavor comuniquese con el administrador del sistema."
                    );
            }            
            $scope.mostrarInfoAnalitica = function(){
                swal("Información Paypal!", "Para obtener el ID de paypal debera Realizar el pago del modulo dando click en en el boton derecho \"comprar ahora\"" 
                    + " En caso de tener alguna" 
                    + " duda con esta seccion, porfavor comuniquese con el administrador del sistema."
                    );
            }

            function crearesquema (esquema, supermercado){
                $http.post("/crearesquema", esquema)
                        .success(function (data, status, headers, config) {
                            
                            console.log("esquema creado correctamente" ,  JSON.stringify(data));
                            // guardar_administrador(administrador);
                            console.log("guardar el supermercado");
                            //$scope.testenviar(supermercado);
                            $timeout(function () {
                                //$scope.saveSupermercado(supermercado);
                                saveSupermercado(supermercado);
                            }, 6000);
                           
                        }).error(function (data, status, headers, config) {
                    console.log("Error en : " + data);
                });
            }

        function guardar_administrador(administrador , esquema , supermercado){
            var url = "/usuario/admin";
            $http.put(url, administrador)
                .success(function (data, status, headers, config) {
                    // redireccionar a la lista de promociones
                    console.log("administrador guardado correctamente" + JSON.stringify(data));
                    //empezar a crear el esquema
                    supermercado.id_admin = data.insertId;
                    console.log("a guardar super" , supermercado);
                    crearesquema(esquema, supermercado);
                }).error(function (data, status, headers, config) {
                    console.log("Ops: " + data);
            });
        }

        function guardar_servicio(bd){ //server
            servicio_guardar.bd = bd; //cargar bd
            var url = "/servicios"
            $http.post(url, servicio_guardar)
                .success(function (data, status, headers, config) {
                    // redireccionar a la lista de promociones
                    console.log("servicio guardado correctamente" , data);
                    console.log("liberar el generando de la pagina ");
                    $loading.finish('empresa');
                    $scope.showRegistrarse();
                    /*if ($scope.checkboxModel.value1){
                        //si existe insertar datos en la base de datos
                        cargarProductosBd(bd);
                    }*/
                    if (redirect) {
                        $location.path('/ng-supermercados');
                    }
                    alertaSuccess(notify , 'Su sistema a sido creado Satisfactoriamente');
                        
                }).error(function (data, status, headers, config) {
                
                    console.log("Ops: " + data);
            });
        }

        function cargarProductosBd(name_bd) {
            var url = '/cargar_productos/' + name_bd;
            $http.get(url)
                .then(function(res) {
                    console.log("los productos correctos insertados "  , res);
                }, function(err){
                    console.log("error al inster datos ", err);
            });
        }

        var servicio_guardar = {
            promocion           : 0,
            ubicacion_producto  : 0,
            listas              : 0,
            analitica           : 0,
            bd                  : ''
        };

        var bandera = true;
        var bandera_ubicacion = true;
        var bandera_analitica = true;
        var bandera_listas = true ;
        $scope.showIdPaypal = false;
        $scope.showAnalitica = false;

        $scope.guardarServicio = function (servicio) { //pagina
            servicio.selected =! servicio.selected;
            if (servicio.namespace === 'promocion'){
                if(bandera){
                    servicio_guardar.promocion = 1
                    bandera=false;
                }
                else {
                    servicio_guardar.promocion = 0
                    bandera = true;
                }

            } else if (servicio.namespace === 'ubicacion_producto') {
                if (bandera_ubicacion){
                    servicio_guardar.ubicacion_producto = 1;
                    bandera_ubicacion = false;
                }
                else {
                    servicio_guardar.ubicacion_producto = 0;
                    bandera_ubicacion = true;
                }

            } else if (servicio.namespace === 'analitica') {
                if (bandera_analitica) {
                    servicio_guardar.analitica = 1;
                    bandera_analitica = false;
                    $scope.showAnalitica = true;
                }
                else {
                    servicio_guardar.analitica = 0;
                    $scope.showAnalitica = false;
                    bandera_analitica = true;
                }

            } else if (servicio.namespace === 'listas') {
                if (bandera_listas){
                    servicio_guardar.listas = 1;
                    bandera_listas = false;
                    $scope.showIdPaypal = true;
                }
                else {
                    servicio_guardar.listas = 0;
                    bandera_listas = true;
                    $scope.showIdPaypal = false;
                }

            }
            console.log("los servicios a guardar son " , servicio_guardar)
        }

        $scope.punto = ".";
        $scope.borrarDatosAlCargar = function () {
            $scope.recuperar = [];
            $scope.mensajeError = "";
        };

        $scope.changewrite = function () {
            $scope.punto = ".";
            $scope.mensgError = "";
        };
            //login
        var Session = null;
        $scope.login = function (user) {
            //verificar que ingrese valores en los camṕos
            if (user.username) {
                if (user.password) {
                    //mostrar en la vista icono loading
                    $scope.mostrargif_loading = true;

                    //enviar a guardar las asignaciones
                    var datos = {"username": user.username, "password": user.password, "remember": user.remember};

                    var url = '/login';
                    $http.post(url, datos)
                        .success(function (data, status, headers, config) {
                            if (data.message) {
                                console.log("error al login" + data.message);
                                var result = data.message;
                                $scope.mensgError = "Usuario o Contraseña Incorrecta!!!";
                                $scope.punto = "";
                                //ocultar loading
                                $scope.mostrargif_loading = false;
                            } else {
                                console.log("logeado correctamente" + JSON.stringify(data));
                                var loginData = [data.user];
                                //eliminar el password
                                delete loginData[0].password;
                                Session = loginData;
                                console.log("el login data es desde las session --> " + JSON.stringify(loginData));
                                $scope.localStorageUser = loginData[0];//para controlar las rutas
                                $scope.localStorageDemo = loginData[0].username;//localStorageService.get('localStorageDemo');
                                var bd_supmercado = loginData[0].bd_supmercado;//localStorageService.get('localStorageDemo');

                                $scope.$watch('localStorageDemo', function (value) {
                                    localStorageService.set('localStorageDemo', value);
                                    $scope.localStorageDemoValue = localStorageService.get('localStorageDemo');
                                });
                                $scope.$watch('localStorageUser', function (value) {
                                    localStorageService.set('localStorageUser', value);
                                    $scope.localStorageUserValue = localStorageService.get('localStorageUser');
                                });

                                $scope.storageType = 'Local storage';

                                if (localStorageService.getStorageType().indexOf('session') >= 0) {
                                    $scope.storageType = 'Session storage';
                                }

                                if (!localStorageService.isSupported) {
                                    $scope.storageType = 'Cookie';
                                }

                                $scope.$watch(function () {
                                    return localStorageService.get('localStorageDemo');
                                }, function (value) {
                                    $scope.localStorageDemo = value;
                                });


                                if(loginData[0].rol === rolAdministrador){
                                    console.log("son iguales cargar vistas");
                                    cargarservicios(bd_supmercado);
                                } 
                                window.location.href = host +"/plantilla.html"
                            }

                        }).error(function (data, status, headers, config) {

                        //ocultar el loading
                        $scope.mostrargif_loading = false;
                        alertaError(notify , 'Usuario o Contraseña Incorrecta ..!!');
                        console.log("Error en : " + data);
                    });
                } else {
                    $scope.mensgError = "Ingrese contraseña";
                    $scope.punto = "";
                }
            } else {
                $scope.mensgError = "Ingrese un usuario";
                $scope.punto = "";
            }
        };

        function cargarservicios ( bd_supmercado ) {
            $http.get("/serviciosdisponibles/" + bd_supmercado)
                .success(function (data, status, headers, config) {
                    console.log("los servicios con lo que cuenta son" , data);
                    if (data.data) {
                        //establecer las variables
                        $scope.localStorageViews = data.data[0];
                        /*promocion: 0
                        ubicacion_producto: 0*/
                    }
                }).error(function (data, status, headers, config) {
                    console.log("Error en : " + data);
            });
                    //local storange para las vistas
            $scope.$watch('localStorageViews', function (value) {
                localStorageService.set('localStorageViews', value);
                $scope.localStorageViewValue = localStorageService.get('localStorageViews');
            });
        }

        $scope.localStorageDemo = localStorageService.get('localStorageDemo');

        $scope.$watch('localStorageDemo', function (value) {
            localStorageService.set('localStorageDemo', value);
            $scope.localStorageDemoValue = localStorageService.get('localStorageDemo');
        });

        $scope.storageType = 'Local storage';

        if (localStorageService.getStorageType().indexOf('session') >= 0) {
            $scope.storageType = 'Session storage';
        }

        if (!localStorageService.isSupported) {
            $scope.storageType = 'Cookie';
        }

        $scope.$watch(function () {
            return localStorageService.get('localStorageDemo');
        }, function (value) {
            $scope.localStorageDemo = value;
        });

        $scope.clearAll = function(){
            console.log("salir");
            localStorageService.clearAll;
        }

            //envia correo
        $scope.enviarCorreo = function (mensaje) {
            var data = {'mensaje': mensaje};
            $http.post("/sendemail", data)
                .success(function (data, status, headers, config) {
                    console.log(JSON.stringify(data));
                }).error(function (data, status, headers, config) {
                    console.log("Error en : " + data);
            });
        }
            //enviar duda o sugerencia
        $scope.enviarCorreoSugerencia = function (menj) {
            $loading.start('email');
            var mensaje = "Nombre: " + menj.nombre + " " +
                          menj.mensaje + "  telefono: " + 
                          menj.telefono + " correo : " + menj.email;
            var data = {'mensaje': mensaje};
            $http.post("/sendemail", data)
                .success(function (data, status, headers, config) {
                    console.log(JSON.stringify(data));
                    $loading.finish('email');
                    alertaSuccess(notify , 'Mensaje enviado satisfactoriamente');
                    limiparDataCorreo();
                }).error(function (data, status, headers, config) {
                    $loading.finish('email');
                    limiparDataCorreo();
                    alertaError(notify , 'Error el enviar mensaje porfavor intente mas tarde');
                    console.log("Error en : " + data);
            });
        }

        function limiparDataCorreo(){
            document.getElementById('message').value    = '';
            document.getElementById('email').value      = '';
            document.getElementById('phone').value      = '';
            document.getElementById('name').value       = '';
        }

        //presentar el menu principal
        $scope.formVisibility = true;

        $scope.ShowForm = function () {
            $scope.form_recuperacion = false;
            $scope.form_mensaje_correcto = false;
            $scope.form_mensaje_error = false;
            $scope.formVisibility = $scope.formVisibility === true ? false : true;
            $scope.formVisibilityRecuperacion = $scope.formVisibility === false ? true : false;

        };


        var usuario = "";
        $scope.recuperar_contrasenia = function (recuperar) {
            usuario = recuperar.nombre_usuario; // para enviar al momento de ingresar la nueva contrasenia

            var datos_enviar = {"nombre_usuario"    : recuperar.nombre_usuario, 
                                "nombre_abuelo"     : recuperar.nombre_abuelo, 
                                "artista_favorito"  : recuperar.artista_favorito, 
                                "comida_favorita"   : recuperar.comida_favorita, 
                                "color_favorito"    : recuperar.color_favorito, 
                                "pasatiempo"        : recuperar.pasatiempo};

            var url = "/inicio/recuperacion";
            //enviar los datos al web service para la recuperacion de la contraseña
            $http.post(url, datos_enviar)
                .success(function (data, status, headers, config) {
                    $scope.mensajeError = "";

                    /*obtener el mensaje de error en caso de que los datos enviados no coincidan*/
                    if (data.mensaje_inf) {
                        $scope.mensajeError = data.mensaje_inf;
                    } else {
                        //habilitar para que pueda cambiar la clave
                        //ocultar formulario preguntas
                        $scope.formVisibilityRecuperacion = false;
                        //presentar el formulario claves
                        $scope.form_recuperacion = true;
                        //notificar al usuario que los datos an sido actualizados
                    }

                }).error(function (data, status, headers, config) {
                    $scope.mensajeError = data;
                    console.log("Error en  : " + data);
            });
        };

        $scope.reset_mensaje = function () {
            $scope.msgError = "";
        };


        $scope.resetear_clave = function (contrasenia) {
            $scope.msgError = '';
            var data = {"username": usuario, "password": contrasenia.contrasenia_nueva};
            if (contrasenia.contrasenia_nueva === contrasenia.contrasenia_nuevarp) {
                var url = "/password/update";
                $http.put(url, data)
                    .success(function (data, status, headers, config) {
                        console.log("se guardo correctamente" + JSON.stringify(data));
                        //success
                        $scope.form_recuperacion = false;
                        $scope.form_mensaje_correcto = true;

                        /*resetear valores de la cajas*/
                        document.getElementById('contrasenia_nueva').value = "";
                        document.getElementById('recover_p').value = "";

                        //notificar al correo
                        var mensaje = "La contraseña se a restablecido exitosamente ..!!! Ingrese con el nombre de usuario y contraseña actualizada actualmente al Sistema Shop-List.";
                        $scope.enviarCorreo(mensaje);


                    }).error(function (data, status, headers, config) {
                        //error
                        $scope.form_mensaje_error = true;
                        $scope.form_recuperacion = false;
                        console.log("Ops: " + data);
                });
            } else {
                /*presentar mensaje*/
                $scope.msgError = "Contraseña no coincide..!!!";
            };
        };
            //efectos login 
        $scope.fromLogin = false;
        $scope.showheader = true;
        $scope.showLogin = function(){
            $scope.fromLogin = $scope.fromLogin === true ? false : true;
            $scope.showheader = $scope.showheader === true ? false : true;
        }

        $scope.formRegistrarse = false;
        $scope.showRegistrarse = function(){
            console.log("registrarse");
            $scope.formRegistrarse = $scope.formRegistrarse === true ? false : true;
            $scope.formVisibility = $scope.formVisibility === true ? false : true;
        }

    })

    .controller('AppCtrl', function ($rootScope, appLoading) {
        $rootScope.topScope = $rootScope;
        $rootScope.$on('$routeChangeStart', function () {
            appLoading.loading();
        });
    })
    .controller('PerfilCtrl', function ($scope, localStorageService, $http, notify) {
        $scope.clase_fondo_perfil = "";
        console.log("el usuaio es ..!!!!" + localStorageService.get('localStorageDemo'));
        $scope.cargar_perfil = function () {
            var url = '/perfil/usuario/' + localStorageService.get('localStorageDemo');
            $http.get(url).success(function (data, status, headers, config) {
                var resultado = data;
                $scope.usuario_cargado = data[0]; //obtiene los datos perfil

            }).error(function (data, status, headers, config) {
                console.log("Ops: could not get any data");
            });

            $scope.cargar_nombres = function () {
                //cambiar de fondo la seccion de editar
                $scope.clase_fondo_perfil = 'jumbotron';
                console.log(JSON.stringify($scope.usuario_cargado));
                document.getElementById('nombre').value = $scope.usuario_cargado.nombres;
                document.getElementById('apellido').value = $scope.usuario_cargado.apellidos;
                document.getElementById('correo').value = $scope.usuario_cargado.email;
                document.getElementById('username').value = $scope.usuario_cargado.username;
            };
            $scope.actualizar_usuario = function () {
                $scope.clase_fondo_perfil = '';
                var data = $scope.usuario_cargado;

                if ($scope.usuario_edicion_nombre) { // si realizo cambios en el nombre
                    data.nombres = $scope.usuario_edicion_nombre;
                    console.log("los datos a guardar del jspn es cuando se edita el nmbr" + JSON.stringify(data));
                }
                if ($scope.usuario_edicion_apellido) { // si realizo cambios en el apellido
                    data.apellidos = $scope.usuario_edicion_apellido;
                    console.log("apellido" + JSON.stringify(data));
                }
                if ($scope.usuario_edicion_email) { // si realizo cambios en el correo
                    data.email = $scope.usuario_edicion_email;
                    console.log("correo" + JSON.stringify(data));
                }
                if ($scope.usuario_edicion_username) { // si realizo cambios en el correo
                    data.username = $scope.usuario_edicion_username;
                    console.log("username" + JSON.stringify(data));
                }

                //enviar al web service
                var url = '/perfil/usuario/' + data.id;
                $http.put(url, data)
                    .success(function (data, status, headers, config) {
                        console.log("se guardo correctamente");
                        if ($scope.usuario_edicion_username) { // si realizo cambios en el nombre usuario
                            console.log("username" + JSON.stringify($scope.usuario_edicion_username));
                            localStorageService.set('localStorageDemo', $scope.usuario_edicion_username);
                        }

                        //notificar al correo del usuario los cambios realizados
                        var enviar_correo = {"mensaje": "El perfil de usuario del sistema Shop-List han sido actualizados correctamente!!!"};
                        $http.post("/sendemail", enviar_correo)
                            .success(function (data, status, headers, config) {
                                console.log(JSON.stringify(data));
                            }).error(function (data, status, headers, config) {
                                console.log("Error en : " + data);
                        });
                        //presentar mensaje
                        alertaSuccess(notify , 'Actualizado Satisfactoriamente');
                        
                    }).error(function (data, status, headers, config) {
                        alertaError(notify, 'Error al actualizar');
                        console.log("Ops: " + data);
                });
            };
        };

        $scope.actualizar_contrasenia = function () {
            if ($scope.contrasenia.contrasenia_nueva === $scope.contrasenia.contrasenia_nuevarp) {
                var url = "/password/update";
                //var url = "/contraseniaupdate";
                var data = {"username": $scope.usuario_cargado.username,
                            "password": $scope.contrasenia.contrasenia_nueva,
                            "remember": $scope.remember};

                $http.put(url, data)
                    .success(function (data, status, headers, config) {
                        console.log("se guardo correctamente" + JSON.stringify(data));
                        //notificar al correo del usuario los cambios realizados
                        var enviar_correo = {"mensaje": "La contraseña  del sistema Shop-List ha sido actualizada correctamente !!!"};
                        $http.post("/sendemail", enviar_correo)
                            .success(function (data, status, headers, config) {
                                console.log(JSON.stringify(data));
                            }).error(function (data, status, headers, config) {
                                console.log("Error en : " + data);
                        });
                        alertaSuccess(notify, 'Actualizado Satisfactoriamente');
                        //borrar el usuario actual de la seccion y salir del sistema
                        window.location.href = host;

                    }).error(function (data, status, headers, config) {
                        alertaError(notify, 'Error al Actualizar..!!');
                        console.log("Ops: " + data);
                });
            } else {
                console.log("contraseñas no coinciden");
            }
        };

        $scope.verificar_contrasenia_antigua = function () {
            console.log("los datos a logeo seran" + JSON.stringify($scope.usuario_cargado.username) + "contrasena ingresada" + $scope.contrasenia.contrasenia_anterior);
            //enviar a guardar las asignaciones

            var datos = {"username": $scope.usuario_cargado.username, 
                        "password": $scope.contrasenia.contrasenia_anterior, 
                        "remember": $scope.remember};

            var url = '/contrasenia';
            $http.post(url, datos)
                .success(function (data, status, headers, config) {
                    console.log("logeado correctamente" + JSON.stringify(data));
                    $scope.actualizar_contrasenia();
                }).error(function (data, status, headers, config) {
                    alertaError(notify , 'Error al Actualizar..!!');
                    console.log("Error en -->> : " + data);
            });
        };

        $scope.hideForm = function () {
            $scope.formVisibility = false;
            $scope.prVisibility = true;

        };
        $scope.prVisibility = true;
        $scope.ShowForm = function () {
            $scope.formVisibility = $scope.formVisibility === true ? false : true;
            $scope.prVisibility = $scope.prVisibility === false ? true : false;
            //quitar el color de fondo del perfil
            $scope.clase_fondo_perfil = '';
        };

        //password
        $scope.passwordVisibility = true;
        $scope.hideFormPassword = function () {
            $scope.passVisibility = false;
            $scope.passwordVisibility = true;
        };
        $scope.showFormPassword = function () {
            $scope.passVisibility = $scope.passVisibility === true ? false : true;
            $scope.passwordVisibility = $scope.passwordVisibility === false ? true : false;
        };
        //verificar password
        $scope.checkPassword = function () {
            $scope.contraseniaForm.contrasenia_nuevarp.$error.dontMatch = $scope.contrasenia.contrasenia_nueva !== $scope.contrasenia.contrasenia_nuevarp;
        };
    })
        .controller('PlantillaCtrl', function ($scope, notify, localStorageService, $http) {
            //control de vistas
            $scope.cargarvistasmodulos = function () {
                var name_bd = localStorageService.get('localStorageUser').bd_supmercado;
                $http.get("/serviciosdisponibles/" + name_bd)
                    .success(function (data, status, headers, config) {
                        if(data.data){
                            //establecer las variables
                            var value = data.data[0];
                            localStorageService.set('localStorageViews', value);
                            $scope.viewPromocion    = localStorageService.get('localStorageViews').promocion;
                            $scope.viewUbicacionP   = localStorageService.get('localStorageViews').ubicacion_producto;
                            $scope.viewListas       = localStorageService.get('localStorageViews').listas;
                            $scope.viewAnalitica    = localStorageService.get('localStorageViews').analitica;
                        }
                    }).error(function (data, status, headers, config) {
                        console.log("Error en : " + data);
                });
            }

            //control del menu
            $scope.openMenu = function(){
                console.log("funcion abrir llamda");
                $('.ui.labeled.icon.sidebar')
                .sidebar('toggle');
            }


            if (localStorageService.get('localStorageUser').rol === compareToRol) {
                $scope.isSuperUsuario = true;
            } 
            else {
                console.log("es administrador")
                $scope.isAdministrador = true;
             }
            //para las notificaciones configuracion
            $scope.msg = 'Hello! This is a sample message!';
            $scope.template = '';
            $scope.positions = ['center', 'left', 'right'];
            $scope.position = $scope.positions[0];
            $scope.duration = 10000;

            //demo de notificaciones
            $scope.demo = function () {
                notify({
                    message: $scope.msg,
                    classes: $scope.classes, // alert-danger or alert-success
                    templateUrl: $scope.template,
                    position: $scope.position,
                    duration: $scope.duration
                });
            };

            //cerrar todas las notificaciones abiertas
            $scope.closeAll = function () {
                notify.closeAll();
            };

            $scope.demoMessageTemplate = function () {
                var messageTemplate = '<span>This is an example using a dynamically rendered Angular template for the message text. ' +
                        'I can have <a href="" ng-click="clickedLink()">hyperlinks</a> with ng-click or any valid Angular enhanced html.</span>';

                notify({
                    messageTemplate: messageTemplate,
                    classes: $scope.classes,
                    scope: $scope,
                    templateUrl: $scope.template,
                    position: $scope.position,
                });
            };
            $scope.clickedLink = function () {
                notify('You clicked a link!');
            };
        })

        .controller('LoginCtrl', function ($scope, $http, $location, Session, $rootScope, appLoading) {
            $scope.currentUser = null;
            $scope.login = function () {
                console.log("login equivocado");
                //enviar a guardar las asignaciones
                var datos = {"username": $scope.username, "password": $scope.password, "remember": $scope.remember};
                var url = '/login';
                //envia datos al servidor
                $http.post(url, datos)
                        .success(function (data, status, headers, config) {
                            //logeado correcto
                            var loginData = [data.user];
                            //eliminar el password
                            delete loginData[0].password;
                            //redireccionar a la pantalla de inicio en caso de que la contraseña se actualizo con exito
                            window.location.href = host;
                        }).error(function (data, status, headers, config) {
                    console.log("Error en : " + data);
                });
            };
        })
        .controller('ProductosPorAsgCtrl', function ($scope, $http, localStorageService) {

            var name_bd = localStorageService.get('localStorageUser').bd_supmercado;

            $scope.cargar_productos_sin_ubicacion = function () {
                console.log("enviar una peticion al server");
                var url = "/web/productos/productosasg/" + name_bd;
                /*enviar peticion al web-service para ver los productos que faltan de asigarle ubicacion*/
                $http.get(url).success(function (respuesta) {
                    //cargar todas los  productos sin ubicacion
                    $scope.productosinubicacion = respuesta;

                }).error(function (data, status, headers, config) {
                    $scope.token = 'error: ' + data + ' ' + status + ' ' + headers + ' ' + JSON.stringify(config);
                    console.log('-->' + $scope.token);
                });
            };


        })

        .controller('PromocionActivaCtrl', function ($scope, $http , localStorageService) {
            $scope.numero_promociones_disponibles = 0;
            var name_bd = localStorageService.get('localStorageUser').bd_supmercado;

            $scope.cargar_promociones_activas = function () {

                var url = "/productos/promocion/" + name_bd;
                $http.get(url).success(function (respuesta) {
                    //cargar todas las promociones disponibles
                    $scope.numero_promociones_disponibles = respuesta.length;
                    $scope.promocionesActivas = respuesta;
                    //cargar el numero de promociones
                    $scope.numero_promociones = respuesta.length;
                    //console.log(JSON.stringify($scope.promocionesActivas));
                }).error(function (data, status, headers, config) {
                    $scope.token = 'error: ' + data + ' ' + status + ' ' + headers + ' ' + JSON.stringify(config);
                    console.log('-->' + $scope.token);
                });
            };
            $scope.cargar_cantidad_promo_activas = function () {

            if (localStorageService.get('localStorageUser').rol !== compareToRol ){

                var url = "/productos/promocion/cantidad/" + name_bd;
                $http.get(url).success(function (respuesta) {
                    //cargar todas las promociones disponibles
                    $scope.numero_promociones_disponibles = respuesta[0].cantidad;
                }).error(function (data, status, headers, config) {
                    $scope.token = 'error: ' + data + ' ' + status + ' ' + headers + ' ' + JSON.stringify(config);
                    console.log('-->' + $scope.token);
                });
            }
            };
            $scope.cargar_cantidad_productos_sin_ubicacion = function () {
                if (localStorageService.get('localStorageUser').rol !== compareToRol ){
                var url = "/productos/productosasg/cantidad/" + name_bd;
                $http.get(url).success(function (respuesta) {
                    //cargar todas las promociones disponibles
                    $scope.numero_productos_sin_asg = respuesta[0].cantidad;
                    //console.log(JSON.stringify($scope.promocionesActivas));
                }).error(function (data, status, headers, config) {
                    $scope.token = 'error: ' + data + ' ' + status + ' ' + headers + ' ' + JSON.stringify(config);
                    console.log('-->' + $scope.token);
                });
            }
            };

            /*controla la cantidad de dias que faltan para que culmine la promocion*/
            $scope.dias_restantes = function (FechaFin) {
                var fecha1 = new Date();
                var fecha2 = new Date(FechaFin);
                var diasDif = fecha2.getTime() - fecha1.getTime();
                var dias = Math.round(diasDif / (1000 * 60 * 60 * 24));
                if (dias < 0)
                    return 0;
                return dias;
            };
        })
        /*controlador de la seccion de promociones*/
        .controller('PromocionesCtrl', function ($scope, $http, notify, localStorageService) {
            var name_bd = localStorageService.get('localStorageUser').bd_supmercado;

            $scope.cargar_promociones = function () {
                $scope.promociones = [];

                $http.get('/web/promocion/' + name_bd).success(function (result, status, headers, config) {
                    /*promociones  obtenidas*/
                    $scope.promociones = result;
                    promociones = result;
                    console.log("los datos son " , promociones);

                    if (result === "") {
                        $scope.promociones = [];
                    }
                }).error(function (data, status, headers, config) {
                    console.log("Ops: could not get any data" + JSON.stringify(data));
                });

            };//fin cargar promociones

            $scope.guardar_promociones = function (promo) {
                var nombre = document.getElementById('nombre_promocion').value;
                var descripcion = document.getElementById('nombre_descripcion').value;
                var descuento = document.getElementById('descuento').value;

                if (nombre) { //verificar si existe nombre promocion a guardar
                    if (descripcion) { // verificar si existe descripcion de la promocion a guardar
                        if (descuento) { // verificar si existe descuento
                        //var nueva_promocion = {"nombre": nombre, "descripcion": descripcion};
                        $scope.promo.descuento = descuento / 100;
                        console.log(">> " , $scope.promo.descuento);
                        var url = '/web/promocion';
                       
                        $scope.promo.name_bd = name_bd;
                        console.log("la promo a guardar es " , $scope.promo);
                        $http.post(url, $scope.promo)
                                .success(function (data, status, headers, config) {
                                    //cargar promocion guardada en la vista 
                                    $scope.promociones.splice(0, 0, 
                                        {"id_promocion" : data, 
                                         "nombre"       : nombre, 
                                         "descripcion"  : descripcion,
                                         "descuento"    : $scope.promo.descuento});
                                    alertaSuccess(notify, 'Guardado Satisfactoriamente');
                                    //borrar campos
                                    document.getElementById('nombre_promocion').value = "";
                                    document.getElementById('nombre_descripcion').value = "";
                                    document.getElementById('descuento').value = "";
                                    //SI guarda correcto ocultar div
                                    $scope.hideForm();

                                }).error(function (data, status, headers, config) {
                                    alertaError(notify , 'Error al guardar intente mas tarde');
                                    console.log("Error en : " + data);
                            });

                    } else {
                        console.log("requiere una descuento");
                        $scope.mensajeErrorDescuento = true;
                        
                    } 
                }else {
                        console.log("requiere una descripcion");
                        $scope.mensajeErrorPromocionDescripcion = true;
                } 
            }else {
                    console.log("requiere un nombre");
                    $scope.mensajeErrorPromocionNombre = true;
                };

            };//fin guardar promociones

            $scope.ocultaErrorNombre = function () {
                $scope.mensajeErrorPromocionNombre = false;
            };
            $scope.ocultaErrorDesc = function () {
                $scope.mensajeErrorPromocionDescripcion = false;
            };
            $scope.ocultaErrorDescuento = function () {
                $scope.mensajeErrorDescuento = false;
            };

            $scope.openModalConfirmacionEliminarPromocion = function () {
                $('#source-modal-delete-promocion').modal('show');
            };

            //verificar si existe una promocion para eliminar
            $scope.existePromocionAsigandaEliminar = function (promocion) {

                $scope.promocionEliminar = promocion;
                var url = "/promocion/asignado_promo/" + promocion.id_promocion + "/" + name_bd;

                $http.get(url).success(function (data, status, headers, config) {
                    /*promocion si se encuentra asignada*/
                    $scope.openModalConfirmacionEliminarPromocion();

                }).error(function (data, status, headers, config) {
                    console.log("Ops: could not get any data" + JSON.stringify(data));
                    /*si la promocion no se encuentra asiganda eliminar */
                    $scope.eliminar_promocion_id(promocion);
                });
            };

            $scope.aceptarEliminacion = function (promocion) {
                $scope.eliminar_promocion_id(promocion);
            };

            $scope.eliminar_promocion_id = function (promocion) {
                var id = promocion.id_promocion;

                var url = '/admin/productos/promocion/' + id +"/" + name_bd;
                
                $http.delete(url).success(function (data, status, headers, config) {
                    /*eliminado correcto*/
                    /*eliminar del arreglo de la vista*/
                    $scope.promociones.splice($scope.promociones.indexOf(promocion), 1);
                    /*muestra mensaje*/
                    alertaSuccess(notify , 'Eliminado Satisfactoriamente');

                }).error(function (data, status, headers, config) {
                    /*error al eliminar*/
                    alertaError(notify , 'Error al eliminar');
                    console.log("Ops: could not get any data");
                });
            };


            //mostrar-oculta formulario
            $scope.hideForm = function () {
                $scope.formVisibility = false;
                $scope.opcion1 = true;
            };
            $scope.opcion1 = true;

            $scope.hideFormOpcion1 = function () {
                //resetaer valores de campos
                $scope.opcion1 = true;
                document.getElementById('nombre_promocion').value = "";
                document.getElementById('nombre_descripcion').value = "";

            };
            $scope.ShowForm = function () {
                //$scope.formVisibility = true;
                $scope.opcion1 = false;
                $scope.formVisibility = $scope.formVisibility === true ? false : true;
            };
        })
        /*controlador de secciones*/
        .controller('SeccionesCtrl', function ($scope, $http, notify, localStorageService) {
            var name_bd = localStorageService.get('localStorageUser').bd_supmercado;

            $scope.cargar_secciones = function () {

                $scope.secciones = [];
                /*envia al servidor*/
                $http.get('/web/secciones/' + name_bd).success(function (data, status, headers, config) {
                    /*obtiene secciones*/
                    if (!data.mensaje) {
                        $scope.secciones = data;
                        secciones = data;
                    } else {
                        /*no contiene secciones*/

                        $scope.secciones = [];
                        alertaError(notify, 'No contiene secciones');

                    }
                }).error(function (data, status, headers, config) {
                    console.log("Ops: could not get any data");
                });

            };//fin cargar promociones

            $scope.guardar_secciones = function () {

                if ($scope.seccion.nombre) {
                    var url = '/web/secciones/' + name_bd;
                    $http.post(url, $scope.seccion)
                            .success(function (data, status, headers, config) {
                                $scope.secciones.splice(0, 0, {"id": data, "nombre": $scope.seccion.nombre});
                                alertaSuccess(notify , 'Guardado Satisfactoriamente')
                                $scope.seccion.nombre = "";
                                //SI guarda correcto ocultar div
                                $scope.hideForm();

                            }).error(function (data, status, headers, config) {
                                alertaError(notify , 'Error al guardar intente mas tarde');
                                console.log("Error en : " + data);
                    });
                }


            };//fin guardar promociones
            /*modal para confirmar al eliminar una seccion*/
            $scope.openModalConfirmacionEliminarSeccion = function () {
                $('#source-modal-delete-seccion').modal('show');
            };

            //verificar si existe una promocion para eliminar
            $scope.existeSeccionAsigandaEliminar = function (seccion) {
                $scope.seccionEliminar = seccion;
                var url = "/seccion/asignada_ubicacion/" + seccion.id + '/' + name_bd;
                $http.get(url).success(function (data, status, headers, config) {
                    $scope.openModalConfirmacionEliminarSeccion();
                }).error(function (data, status, headers, config) {
                   /*si no se encuentra elimina directo*/
                    $scope.eliminar_seccion_id(seccion);
                });
            };
            $scope.aceptarEliminacion = function (seccion) {
                $scope.eliminar_seccion_id(seccion);
            };
            $scope.eliminar_seccion_id = function (seccion) {
                var id = seccion.id;

                var url = '/web/secciones/' + id + '/' + name_bd;

                $http.delete(url).success(function (data, status, headers, config) {
                    alertaSuccess(notify , 'Eliminado Satisfactoriamente');
                    console.log("A sido eliminado " + data);
                    $scope.secciones.splice($scope.secciones.indexOf(seccion), 1);
                }).error(function (data, status, headers, config) {
                    alertaError(notify , 'Error al eliminar');
                    console.log("Ops: could not get any data");
                });
            };

            //mostrar-oculta formulario
            $scope.hideForm = function () {
                $scope.formVisibility = false;
                $scope.opcion1 = true;
            };
            $scope.opcion1 = true;

            $scope.hideFormOpcion1 = function () {
                //resetaer valores de campos
                $scope.opcion1 = true;
                document.getElementById('nombre_seccion').value = "";

            };
            $scope.ShowForm = function () {
                //$scope.formVisibility = true;
                $scope.opcion1 = false;
                $scope.formVisibility = $scope.formVisibility === true ? false : true;
            };
        })
        /*controlador promociones*/
        .controller('PromocionEdicionCtrl', function ($scope, $routeParams, $http, $location, 
                                                      notify, localStorageService) {

            var name_bd = localStorageService.get('localStorageUser').bd_supmercado;

            $scope.cargar_promocion_id = function () {
                console.log("cargar promocion en bd " , name_bd);

                $scope.productoSleccionado = [];
                var url = '/admin/productos/promocion/' + $routeParams.promocionId + "/" + name_bd;
                $http.get(url).success(function (data, status, headers, config) {
                    
                    if (data !== "") {
                        //$scope.productoSleccionado = data[0];
                        promociones = data;

                        $scope.productoSleccionado = {
                            nombre      : data[0].nombre,
                            descuento   : data[0].descuento * 100,
                            descripcion : data[0].nombre

                        };
                        
                    }else{
                        $scope.productoSleccionado = [];
                    }
                }).error(function (data, status, headers, config) {
                    console.log("Ops: could not get any data");
                });
            };
            //actulizar promocion seleccionada
            $scope.actualizar_promocion = function (promocion) {
                console.log("promocion actualizada " , promocion);

                promocion.descuento =  promocion.descuento / 100;

                var url = '/admin/productos/promocion/' + $routeParams.promocionId + "/" + name_bd;
                $http.put(url, promocion)
                        .success(function (data, status, headers, config) {
                            // redireccionar a la lista de promociones
                            $location.path('/ng-promocion');
                            alertaSuccess(notify , 'Actualizado Satisfactoriamente');

                            $scope.productoSleccionado = '';
                        }).error(function (data, status, headers, config) {
                            alertaError(notify , 'Error al Actualizar');
                            console.log("Ops: " + data);
                });
            };
        })

        //controlador seccion edicion
        .controller('SeccionEdicionCtrl', function ($scope, 
                                                    $routeParams,
                                                    $http, 
                                                    $location, 
                                                    notify, 
                                                    localStorageService) {

            var name_bd = localStorageService.get('localStorageUser').bd_supmercado;

            $scope.cargar_seccion_id = function () {
                $scope.seccionSeleccionada = [];
                var url = '/web/secciones/' + $routeParams.seccionId + '/' + name_bd;
                $http.get(url).success(function (data, status, headers, config) {
                    $scope.seccionSeleccionada = data[0];
                    secciones = data;
                    if (data === "") {
                        $scope.seccionSeleccionada = [];
                    }
                }).error(function (data, status, headers, config) {
                    console.log("Ops: could not get any data");
                });
            };

            //actulizar promocion seleccionada
            $scope.actualizar_seccion = function (seccion) {
                var url = '/web/secciones/' + $routeParams.seccionId + '/' + name_bd;
                $http.put(url, seccion)
                        .success(function (data, status, headers, config) {
                            //redireccionar a la lista de promociones
                            $location.path('/ng-secciones');
                            alertaSuccess(notify , 'Actualizado Satisfactoriamente');

                            $scope.seccionSeleccionada = '';
                        }).error(function (data, status, headers, config) {
                            alertaError(notify, 'Error al Actualizar');
                            console.log("Ops: " + data);
                });
            };
        })
        .controller('AsgPromocionesCtrl', function ($scope, $http, notify, $filter, localStorageService) {
            //presentar el boton por defecto
            $scope.mostartocultarboton = true;
            var name_bd = localStorageService.get('localStorageUser').bd_supmercado;

            $scope.mostrarOcultarForm = function () {

                $scope.mostartocultarform = $scope.mostartocultarform === true ? false : true;
                //ocultar el boton
                $scope.mostartocultarboton = $scope.mostartocultarboton === false ? true : false;
                //en caso de cancelar el guardado resetear valores de la lista
                $scope.productos_seleccionados = [];
            };


            $scope.cargar_promociones_asg = function () {
                $scope.promociones_asg = [];
                console.log("enviar datos al servidor");
                
                $http.get('/admin/productos/promocionasg/' + name_bd).success(function (data, status, headers, config) {
                    if (!data.mensaje) {
                        $scope.promociones_asg = data;
                        promociones = data;
                        console.log("los valores de la base de datos son " + data);
                    } else {
                        alertaError(notify, 'No existen promociones asiganadas');

                    }
                }).error(function (data, status, headers, config) {
                    console.log("Ops: could not get any data");
                });



            };//fin cargar promociones

            $scope.productos_seleccionados = [];
            $scope.agregar_productos = function (producto) {
                
                //enviar a preguntar si ese producto ya se encuentra en la bd 
                var url = "/productos/asignado_promo/" + producto.codigo + "/" + name_bd;

                $http.get(url).success(function (data, status, headers, config) {

                    /*cargar el producto a agregar*/
                    $scope.producto_asignar = producto;
                    //preguntar si desea agregarlos
                    //mostrar modal
                    $scope.openModalConfirmacionAgregar();

                }).error(function (data, status, headers, config) {
                    console.log("Ops: could not get any data" + JSON.stringify(data));
                    //productos no encontrados agregar para ser asignados
                    $scope.productos_seleccionados.push(producto);
                    $scope.productos_buscados.splice($scope.productos_buscados.indexOf(producto), 1);
                });
            };

            //para llamar al modal
            $scope.openModalConfirmacionAgregar = function () {
                $('#source-modal-add-producto').modal('show');
            };

            //continuar con el asignado en el modal
            $scope.continuarGuardado = function (producto) {
                $scope.productos_seleccionados.push(producto);
                $scope.productos_buscados.splice($scope.productos_buscados.indexOf(producto), 1);
                $scope.buttonAgregar = false;
                $scope.confirmacionVisible = false;
            };
            //cancelamos el guardado con el asignado en el modal
            $scope.cancelarGuardado = function () {
                $scope.buttonAgregar = false;
                $scope.confirmacionVisible = false;
            };

            //seccion para edicion en asignacion productos
            $scope.remover_posiciones = function (producto) {
                $scope.productos_asignados.push(producto);
                $scope.productos_buscados.splice($scope.productos_buscados.indexOf(producto), 1);
            };
            //fin seccion

            $scope.eliminar_productos = function (producto) {
                $scope.productos_seleccionados.splice($scope.productos_seleccionados.indexOf(producto), 1);
            };

            /*borrar productos de la seccion buscar en Asignar promociones Edicion*/
            $scope.borrarProductosBuscados = function () {
                $scope.productos_buscados = [];
                $scope.texto_buscar = "";
            };


            $scope.buscar_productos = function () {
                $scope.productos_buscados = [];
                var url = '/admin/productos/';
                var buscar = $scope.texto_buscar;
                url += buscar + "/" + name_bd;

                if (buscar) {
                    $http.get(url).success(function (data, status, headers, config) {
                        /*productos encontrados*/
                        $scope.productos_buscados = data;
                        if (data === "") {
                            $scope.productos_buscados = [];
                        }
                    }).error(function (data, status, headers, config) {
                        console.log("Ops: could not get any data");
                    });
                } else {
                    console.log("ingrese un nombre en el campo de texto")
                }
            };//fin cargar productos

            //resetear el mensaje de error de la promocion
            $scope.reset_msg_promo = function () {
                $scope.mensaje_promocion = "";
            };
            //resetear el mensaje de error de fecha inicio
            $scope.reset_msg_fechainicio = function () {
                $scope.mensaje_fecha = "";
            };
            //resetear el mensaje de error de fecha fin
            $scope.reset_msg_fechafin = function () {
                console.log("el mensaje de error a sido llamado")
                $scope.mensaje_fecha_fin = "";
            };

            $scope.resetvalores = function () {
                document.getElementById('fecha_inicio').value = "";
                document.getElementById('fecha_fin').value = "";
            };

            $scope.compararMayorMenorFecha = function (FechaInicio, FechaFin) {

                var fecha1 = new Date(FechaInicio);
                var fecha2 = new Date(FechaFin);
                if (fecha2.getTime() >= fecha1.getTime())
                    return true;
                return false;
            };



            $scope.mensajeError = "";
            $scope.guardar_promociones_asg = function (promo) {
                /*controlar el ingreso de todos los campos*/
                if (!promo) {
                    $scope.mensaje_promocion = "Debe ingresar una promoción..!!!";
                } else {
                    if (!document.getElementById('fecha_inicio').value) {
                        $scope.mensaje_fecha = "requiere fecha inicio !!!";
                    } else {
                        var fecha_inicio = document.getElementById('fecha_inicio').value;
                        if (!document.getElementById('fecha_fin').value) {
                            $scope.mensaje_fecha_fin = "requiere fecha de finalización..!!!";
                        } else {
                            var fecha_fin = document.getElementById('fecha_fin').value;
                            if (!$scope.productos_seleccionados[0]) { //verificar que hayan agregado productos
                                alertaError(notify , 'Debe ingresar productos');
                            } else {
                                //comparar la fecha
                                if ($scope.compararMayorMenorFecha(fecha_inicio, fecha_fin)) {
                                    //en caso de llenar todos los campos

                                    var productos_seleccionados = $scope.productos_seleccionados;

                                    var nueva_promocion_asg = {"id_promocion": promo.id_promocion, "fecha": fecha_inicio, "fechaFin": fecha_fin};
                                    var url = '/admin/productos/promocionasg/' + name_bd;
                                    /*enviar agregar datos al servidor*/
                                    $http.post(url, nueva_promocion_asg)
                                            .success(function (data, status, headers, config) {

                                                //actualizar la vista
                                                $scope.promociones_asg.splice(0, 0, {"id_asg_promocion": data, "fecha": fecha_inicio, "fechaFin": fecha_fin, "nombre": promo.nombre});

                                                //ocultar el formulario
                                                $scope.mostartocultarform = false;
                                                //mostrar el boton
                                                $scope.mostartocultarboton = true;

                                                var id_promocion_guardada = JSON.stringify(data);


                                                for (var i = 0, max = productos_seleccionados.length; i < max; i++) {
                                                    var id_productos_seleccionados = productos_seleccionados[i].codigo;

                                                    var nueva_asg = {"id_asignar_promocion": data, "id_producto": id_productos_seleccionados};

                                                    //enviar a guardar las asignaciones
                                                    var url = '/admin/productos/relacionespromocionasg/1/' + name_bd;
                                                    $http.post(url, nueva_asg)
                                                            .success(function (data, status, headers, config) {
                                                                console.log("se guardo correctamente" + JSON.stringify(data));

                                                            }).error(function (data, status, headers, config) {
                                                        console.log("Error en : " + data);
                                                    });
                                                }

                                                /*si se guarda correcto se oculta la vista*/
                                                $scope.formVisibility = false;
                                                alertaSuccess(notify, 'Guardado Satisfactoriamente');

                                                //borrar valores de la lista
                                                $scope.productos_seleccionados = [];

                                                //fin enviar a guardar los productos

                                            }).error(function (data, status, headers, config) {
                                                alertaError(notify, 'Error al guardar intente mas tarde')
                                        //ocultar el formulario
                                        $scope.mostartocultarform = false;
                                        //mostrar el boton
                                        $scope.mostartocultarboton = true;

                                        console.log("Error en : " + data);
                                    });
                                } else {
                                    alertaError(notify, ' Error !!! La fecha de inicio debe ser menor o igual que la fecha de fin');
                                }
                            }
                        }
                    }
                }
            };//fin guardar promociones

            /*confirmar para eliminar una promocion*/
            $scope.confirmarEliminacionPromociones = function (promocion) {
                //preguntar al servidor si tiene productos asignados esa promocion
                $scope.promocionEliminar = promocion;
            };
            //delete promocion database
            $scope.eliminar_promocion_asg_id = function (promocion) {
                var id = promocion.id_asg_promocion;
                var url = '/admin/productos/promocionasgid/' + id + '/' + name_bd;

                $http.delete(url).success(function (data, status, headers, config) {
                    console.log("A sido eliminado " + data);
                    alertaError(notify, 'Eliminado Satisfactoriamente')
                    $scope.promociones_asg.splice($scope.promociones_asg.indexOf(promocion), 1);
                }).error(function (data, status, headers, config) {
                    alertaError(notify, 'Error al Eliminar');
                    console.log("Ops: could not get any data");
                });
            };

            //para presentar los productos en la vista de asg_promociones
            $scope.cargar_productos_relacionados_id = function (id_promo_asg) {
                var url = '/admin/productos/relacionespromocionasg/' + id_promo_asg + '/' + name_bd;
                $http.get(url).success(function (data, status, headers, config) {
                    $scope.productos_asignados_id = data;
                    if (data === "") {
                        $scope.productos_asignados_id = [];
                    }
                }).error(function (data, status, headers, config) {
                    console.log("Ops: could not get any data");
                });

            };//fin cargar productos
        })
        /*controlador de promociones asignadas en la seccion de edicion*/
        .controller('AsgPromocionEdicionCtrl', function ($scope, $routeParams, dateFilter, $http, $location, notify, localStorageService) {
            var promocion = [];
            var name_bd = localStorageService.get('localStorageUser').bd_supmercado;
            //borrar los productos en el buscador
            $scope.borrarProductosBuscados = function () {
                console.log("llamar al metodo eliminar");
                $scope.productos_buscados = [];
                $scope.texto_buscar = "";
            };

            $scope.asg_promocion_selecccionada = function (promocionSeleccionada) {
                asg_promo_select = promocionSeleccionada;

            };
            $scope.cargar_asg_promo = function () {

                $scope.promocion_actual_select = productos_asignados[0].nombre;
                $scope.promo_select = asg_promo_select;

            };

            $scope.productos_asignados = [];
            $scope.cargar_productos_relacionados = function () {

                var url = '/admin/productos/relacionespromocionasg/' + $routeParams.promo_asg_id + "/" + name_bd;

                $http.get(url).success(function (data, status, headers, config) {
                    $scope.productos_asignados = data;

                    //cargar datos extras
                    document.getElementById('fecha_inicio').value = dateFilter(data[0].fecha, 'yyyy-MM-dd');
                    document.getElementById('fecha_fin').value = dateFilter(data[0].fechaFin, 'yyyy-MM-dd');
                    $scope.promocion_actual_select = data;

                    console.log("los productos asignados " + JSON.stringify(data));
                    if (data === "") {
                        $scope.productos_asignados = [];
                    }
                }).error(function (data, status, headers, config) {
                    console.log("Ops: could not get any data");
                });

            };//fin cargar productos


            $scope.eliminar_relacion_producto_id = function (producto_seleccionado) {
                var id_asgpromo_product = producto_seleccionado.id_asgpromo_product;
                var url = '/admin/productos/relacionespromocionasg/' + id_asgpromo_product + "/" + name_bd;
                if ($scope.productos_asignados.length !== 1) {
                    $http.delete(url).success(function (data, status, headers, config) {
                        console.log("A sido eliminado " + data);
                        alertaSuccess(notify, 'Eliminado Satisfactoriamente');

                        $scope.productos_asignados.splice($scope.productos_asignados.indexOf(producto_seleccionado), 1);
                    }).error(function (data, status, headers, config) {
                        alertaError(notify, 'Error al Eliminar');
                        console.log("Ops: could not get any data");
                    });

                } else {
                    /*presentar el mensaje*/
                    $scope.msg_aviso = true;
                }
            };

            $scope.ocultarMensajeBorrado = function () {
                console.log("el mensaje de borrado ");
                $scope.msg_aviso = false;
            };
            //para establecer el modelo de la promocion seleccionada
            $scope.data = {
                promocion_select: ''
            };
            //seleccionar una promocion
            $scope.stateSelected = function (state) {
                $scope.promocion_seleccionada = state;
            };

            $scope.compararMayorMenorFecha = function (FechaInicio, FechaFin) {

                var fecha1 = new Date(FechaInicio);
                var fecha2 = new Date(FechaFin);
                if (fecha2.getTime() >= fecha1.getTime())
                    return true;
                return false;
            };


            $scope.actualizar_promocion_asg = function () {

                var fecha = document.getElementById('fecha_inicio').value;
                var fechaFin = document.getElementById('fecha_fin').value;
                var id_promocion = 0;
                if ($scope.promocion_seleccionada) {
                    id_promocion = $scope.promocion_seleccionada.id_promocion;
                } else {
                    console.log("no existe");
                    id_promocion = $scope.promocion_actual_select[0].id_promocion;
                }
                if ($scope.compararMayorMenorFecha(fecha, fechaFin)) {

                    var url = '/admin/productos/relacionespromocionasg/' + $routeParams.promo_asg_id + "/" + name_bd;
                    var promocion_asg_update = {"fecha": fecha, "fechaFin": fechaFin, "id_promocion": id_promocion};

                    $http.put(url, promocion_asg_update)
                            .success(function (data, status, headers, config) {
                                console.log("se guardo correctamente");
                                $location.path('/ng-asg-promociones');
                                $scope.productoSleccionado = '';
                                alertaSuccess(notify , 'Actualizado Satisfactoriamente');

                            }).error(function (data, status, headers, config) {
                                alertaError(notify, 'Error al Actualizar');
                                console.log("Ops: " + data);
                    });

                } else {
                    alertaError(notify , 'Error !!! La fecha de inicio debe ser menor o igual que la fecha de fin');
                }
            };
            //agregar producto a una promocion
            $scope.insertar_producto_promocion_bd = function (producto) {
                /*se obtiene el id de la promocion y el id del producto a asignar*/
                var nueva_asg = {"id_asignar_promocion": $routeParams.promo_asg_id, "id_producto": producto.codigo};

                //enviar a guardar las asignaciones
                var url = '/admin/productos/relacionespromocionasg'+ '/1/' + name_bd;

                $http.post(url, nueva_asg)
                        .success(function (data, status, headers, config) {
                            /*guardado correcto*/

                        }).error(function (data, status, headers, config) {
                    console.log("Error en : " + data);
                });

            };


        })
        //controlador de ubicacioon

        .controller('UbicacionCtrl', function ($scope, 
                                               $http, 
                                               $routeParams, 
                                               notify, 
                                               localStorageService) {


            var name_bd = localStorageService.get('localStorageUser').bd_supmercado;

            $scope.cargar_ubicaciones = function () {
                $scope.ubicaciones = [];
                
                $http.get('/web/productos/ubicacion/' + name_bd).success(function (data, status, headers, config) {

                    if (data.mensaje) {
                        //en caso de no encontrar productos
                        console.log("no tiene ubicaciones");
                        $scope.ubicaciones = [];
                        alertaError(notify , 'No contiene ubicaciones');

                    } else {
                        $scope.ubicaciones = data;
                    }
                    //promociones = data;
                    console.log("los valores de la base de datos son " + JSON.stringify(data));

                }).error(function (data, status, headers, config) {
                    console.log("Ops: could not get any data");
                });
            };//fin cargar promociones


            $scope.guardar_ubicacion_prueba = function (ubicacion) {
                console.log("los valores son >>!!  " + JSON.stringify(ubicacion));
                console.log(ubicacion.corredor);
            }

            /*para guardar una ubicacion se obtiene la misma por parametro*/
            $scope.guardar_ubicacion = function (ubicacion, seccion) {

                if (!seccion) {
                    $scope.mensaje_seccion = "Debe ingresar una seccion..!!!";
                } else {
                    ubicacion.fk_seccion = seccion.id;
                    var url = '/web/productos/ubicacion/' + name_bd;
                    $http.post(url, ubicacion)
                            .success(function (data, status, headers, config) {
                                console.log("se guardo correctamente" + JSON.stringify(data));
                                $scope.ubicaciones.splice(0, 0, {"cod_ubicacion": data, 
                                                                 "corredor": ubicacion.corredor, 
                                                                 "numeroEstante": ubicacion.numeroEstante, 
                                                                 "pasillo": ubicacion.pasillo,
                                                                 "nombre": seccion.nombre,
                                                                 "numeroPiso": ubicacion.numeroPiso,});

                                //habilitar boton nueva y ocultar formulario

                                $scope.bottonVisibility = true;
                                $scope.formVisibility = false;

                                //limpiar campos
                                alertaSuccess(notify , 'Guardado Satisfactoriamente');

                                document.getElementById("corredor_n").value = "";
                                document.getElementById("numero_Estante").value = "";
                                document.getElementById("pasillo_u").value = "";
                                document.getElementById("numero_Piso").value = "";

                            }).error(function (data, status, headers, config) {
                                alertaError(notify, 'Error al guardar intente mas tarde');
                                console.log("Error en : " + data);
                        });

                    //limpiar campos
                }

            };//fin guardar promociones


            $scope.delete_ubicacion_bd = function (ubicacion) {
                var id = ubicacion.cod_ubicacion;
                var url = '/web/productos/ubicacion/' + id + '/' + name_bd;

                $http.delete(url).success(function (data, status, headers, config) {
                    console.log("A sido eliminado " + data);
                    alertaSuccess(notify , 'Eliminado Satisfactoriamente')
                    $scope.ubicaciones.splice($scope.ubicaciones.indexOf(ubicacion), 1);
                }).error(function (data, status, headers, config) {
                    
                    alertaError(notify , 'Error al eliminar');
                    console.log("Ops: could not get any data");
                });
            }
            $scope.openModalConfirmacionEliminarPromocion = function () {
                $('#source-modal-delete-ubicacion').modal('show');
            };

            $scope.elimarUbicacionModal = function (ubicacion) {
                //eliminar de la bd la ubicacion
                $scope.delete_ubicacion_bd(ubicacion);
            }

            $scope.eliminar_ubicacion_id = function (ubicacion) {
                var url = "/ubicacion/tiene_productos/" + ubicacion.cod_ubicacion + "/" + name_bd;
                $http.get(url).success(function (data, status, headers, config) {
                    $scope.ubicacionSelectEliminar = ubicacion;
                    console.log("si existe ubicacion llmar al modal");
                    $scope.openModalConfirmacionEliminarPromocion();
                }).error(function (data, status, headers, config) {
                    console.log("no existe ubicacion eliminar directo");
                    $scope.delete_ubicacion_bd(ubicacion);
                    console.log("Ops: could not get any data");
                });
            };
            //actulizar ubicacion seleccionada
            $scope.actualizar_promocion = function (ubicacion) {
                var url = '/web/productos/ubicacion/' + $routeParams.ubicacionId + "/" + name_bd;
                $http.put(url, ubicacion)
                        .success(function (data, status, headers, config) {
                            //guardo correcto redireccionar a la lista de promociones
                            $location.path('/ng-ubicacion');
                            $scope.productoSleccionado = '';
                        }).error(function (data, status, headers, config) {
                    console.log("Ops: " + data);
                });
            };

            //mostrar-oculta formulario ubicacion
            $scope.bottonVisibilityubicacion = true;
            $scope.ocultarForm = function () {
                $scope.bottonVisibilityubicacion = true;
                $scope.formVisibility = false;
            };

            $scope.mostrarForm = function () {
                //$scope.formVisibility = true;
                $scope.formVisibility = $scope.formVisibility === true ? false : true;
                $scope.bottonVisibilityubicacion = $scope.bottonVisibilityubicacion === true ? false : true;
            };
        })

        .controller('UbicacionEdicionCtrl', function ($scope, 
                                                      $routeParams, 
                                                      $http, 
                                                      $location,
                                                      localStorageService) {
            $scope.visible_seccion_defecto = true;
            var name_bd = localStorageService.get('localStorageUser').bd_supmercado;

            $scope.ocultaropcioninicial = function () {
                console.log("ocultar");
                $scope.visible_seccion_defecto = false;
            }

            $scope.cargar_ubicacion_id = function () {
                $scope.ubicacionSleccionada = [];
                
                var url = '/web/productos/ubicacion/' + $routeParams.ubicacionId + '/' + name_bd;
                $http.get(url).success(function (data, status, headers, config) {
                    $scope.ubicacionSleccionada = data;
                    //promociones = data;
                    console.log("los valores de la base de datos son " + data);
                    if (data === "") {
                        $scope.ubicacionSleccionada = [];
                    }
                }).error(function (data, status, headers, config) {
                    console.log("Ops: could not get any data");
                });
            };
            //actulizar promocion seleccionada
            $scope.actualizar_ubicacion = function (ubicacion, seccion_seleccionada) {
                if (seccion_seleccionada)
                    ubicacion.fk_seccion = seccion_seleccionada.id;
                console.log("enviar por parametros" + JSON.stringify(ubicacion));
                var url = '/web/productos/ubicacion/' + $routeParams.ubicacionId + '/' + name_bd;
                $http.put(url, ubicacion)
                        .success(function (data, status, headers, config) {
                            console.log("se guardo correctamente");
                            //si se guardo correcto redireccionar a la lista de promociones
                            $location.path('/ng-ubicacion');

                            $scope.ubicacionSleccionada = '';
                        }).error(function (data, status, headers, config) {
                            console.log("Ops: " + data);
                        });
            };

        })

        .controller('AsgUbicacionCtrl', function ($scope, $http, $routeParams, notify, localStorageService) {
            
            var name_bd = localStorageService.get('localStorageUser').bd_supmercado;

            $scope.cargar_ubicaciones_asg = function () {
                $scope.ubicaciones_asg = [];
                var url = '/web/productos/ubicacionasg/' + $routeParams.cod_ubicacion + '/' + name_bd;
                $http.get(url).success(function (data, status, headers, config) {
                    $scope.ubicaciones_asg = data;

                }).error(function (data, status, headers, config) {
                    alertaError(notify, 'No contiene productos');
                    console.log("Ops: could not get any data");
                });
            };//fin cargar promociones
            $scope.productos_seleccionados = [];
            $scope.agregar_productos = function (producto) {
                $scope.productos_seleccionados.push(producto);
                $scope.productos_buscados.splice($scope.productos_buscados.indexOf(producto), 1);
            };
            //seccion para edicion en asignacion productos
            $scope.remover_posiciones = function (producto) {
                $scope.productos_asignados.push(producto);
                $scope.productos_buscados.splice($scope.productos_buscados.indexOf(producto), 1);
            };
            //fin seccion

            $scope.eliminar_productos = function (producto) {
                $scope.productos_seleccionados.splice($scope.productos_seleccionados.indexOf(producto), 1);
            };

            $scope.buscar_productos = function () {
                $scope.productos_buscados = [];
                var url = '/web/productos/productosasg/';
                var buscar = $scope.texto_buscar;
                url += buscar + "/" + name_bd;

                if (buscar) {
                    $http.get(url).success(function (data, status, headers, config) {
                        /*productos encontrados*/
                        $scope.productos_buscados = data;

                    }).error(function (data, status, headers, config) {
                        console.log("Ops: could not get any data");
                    });
                } else {
                    console.log("ingrese un nombre en el campo de texto");
                }
            };//fin cargar productos
            var ubicacion_seleccionada;
            $scope.ubicacionSelect = function (ubicacion) {

                ubicacion_seleccionada = ubicacion.cod_ubicacion;
                /*resetear la lista de productos buscados*/
                $scope.productos_buscados = [];
                /*resetear el nombre de los  productos buscados*/
                $scope.texto_buscar = "";
            };

            $scope.guardar_ubicaciones_asg = function (producto, ubicacion) {
                var productos_seleccionados = $scope.productos_seleccionados;
                /*json de producto a asigar ubicación*/
                var nueva_ubicacion_asg = {"codigo": producto.codigo, "ubicacion_id": ubicacion_seleccionada};

                var url = '/web/productos/ubicacionasg/' + name_bd;
                $http.put(url, nueva_ubicacion_asg)
                        .success(function (data, status, headers, config) {
                            console.log(JSON.stringify("datos guardads " + data));
                            //actualizar la vista
                            $scope.productos_buscados.splice($scope.productos_buscados.indexOf(producto), 1);
                            alertaSuccess(notify, 'Guardado Satisfactoriamente')

                        }).error(function (data, status, headers, config) {
                            alertaError(notify, 'Error al guardar intente mas tarde')
                            console.log("Error en : " + data);
                });
            };//fin guardar promociones*/

            //delete promocion database
            $scope.eliminar_ubicacion_asg_id = function (ubicacion) {
                var id = ubicacion.codigo;


                if ($scope.ubicaciones_asg.length != 1) {

                    var url = '/web/productos/ubicacionasg/' + id + '/' + name_bd;
                    $http.put(url, ubicacion)
                            .success(function (data, status, headers, config) {
                                console.log("A sido quitar la ubicacion " + data);
                                //actualizar la lista
                                alertaError(notify, 'Eliminado Satisfactoriamente ..!!!')
                                $scope.ubicaciones_asg.splice($scope.ubicaciones_asg.indexOf(ubicacion), 1);

                            }).error(function (data, status, headers, config) {
                        console.log("Ops: " + data);
                    });
                } else {
                    console.log("solo hay un producto");
                    $scope.msg_aviso = true;
                }
                ;
            };

            $scope.actualizar_ubicacion_asg = function (ubicacion, id) {
                ubicacion.fk_ubicacion = id.cod_ubicacion;
                var url = '/web/productos/ubicacionasg/' + ubicacion.id_productos_ubicacion + '/' + name_bd;
                //var promocion_asg_update = { "fk_ubicacion": ubicacion.fk_ubicacion};
                //console.log("los valores a enviar a la bd es  " + JSON.stringify(promocion_asg_update));
                $http.put(url, ubicacion)
                        .success(function (data, status, headers, config) {
                            console.log("se guardo correctamente");
                            //actualizar la lista
                            alertaSuccess(notify, 'Actualizado Satisfactoriamente');
                            $scope.ubicaciones_asg.splice($scope.ubicaciones_asg.indexOf(ubicacion), 1);

                        }).error(function (data, status, headers, config) {
                            alertaError(notify, 'Error al Actualizar !!!')
                            console.log("Ops: " + data);
                });
            };


            //mostrar-oculta formulario
            $scope.hideSelect = function () {
                $scope.selectVisibility = false;
            };
            $scope.ShowSelect = function (uniqueAttribute) {
                // $scope.selectVisibility = true;
                $scope[uniqueAttribute] = $scope[uniqueAttribute] === true ? false : true;

            };

        });//fin del controlador general

 function alertaSuccess (notify, mensaje){
        notify({
            message: mensaje,
            classes: 'alert-success', // alert-danger or alert-success
            templateUrl: '',
            position: 'center',
            duration: 2000
        });
    };

function alertaError (notify, mensaje){
        notify({
            message: mensaje,
            classes: 'alert-danger', // alert-danger or alert-success
            templateUrl: '',
            position: 'center',
            duration: 2000
        });
    };

var metodos = {
    //obtiene una promocion de la vista por un id seleccionado
    getPromocionId: function (promocionId) {
        for (var i = 0; i < promociones.length; i++) {
            if (promociones[i].id_promocion === parseInt(promocionId)) {
                return promociones[i];
            }
        }
        return null;
    }
};
