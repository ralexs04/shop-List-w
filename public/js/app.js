angular.module('shop.services', []);
angular.module('shop.controllers', []);

angular.module('App', ['starter.controllers', 'shop.services' , 'shop.controllers', 'darthwade.loading'])
        .config(function ($routeProvider , $locationProvider) {


    $locationProvider.hashPrefix('!');

            $routeProvider
                    .when('/ng-promocion', {
                        controller: 'PromocionesCtrl',
                        templateUrl: './templates/promocion.html'
                    })
                    .when('/ng-productos', {
                        controller: 'ProductosCtrl',
                        templateUrl: './com/admin/views/productos.html'
                    })
                    .when('/ng-secciones', {
                        controller: 'SeccionesCtrl',
                        templateUrl: './templates/secciones.html'
                    })
                    .when('/ng-seccion-edi/:seccionId', {
                        controller: 'SeccionEdicionCtrl',
                        templateUrl: './templates/seccion_edicion.html'
                    })
                    .when('/ng-configuracion', {
                        controller: 'ConfiguracionCtrl',
                        templateUrl: './templates/config.html'
                    })
                    .when('/ng-bienvenido', {
                        controller : 'BienvenidoCtrl',
                        templateUrl: './templates/bienvenido.html',
                        //authenticate: true
                    })
                    .when('/ng-asg-promociones', {
                        controller: 'AsgPromocionesCtrl',
                        templateUrl: './templates/asg_promociones.html'
                    })
                    .when('/ng-promo-edi/:promocionId', {
                        controller: 'PromocionEdicionCtrl',
                        templateUrl: './templates/promo_edicion.html'
                    })
                    //asignar promocion Edicion
                    .when('/ng-promo-asg-edi/:promo_asg_id', {
                        controller: 'AsgPromocionEdicionCtrl',
                        templateUrl: './templates/asg_promo_edit.html'
                    })
                    //analitica
                    .when('/ng-analitica', {
                        controller: 'AnaliticaCtrl',
                        templateUrl: './com/admin/views/analitica.html'
                    })
                    //ubicacion
                    .when('/ng-ubicacion', {
                        controller: 'UbicacionCtrl',
                        templateUrl: './templates/ubicacion.html'
                    })
                    //ver productos corres pondientes a una ubicacion
                    .when('/ng-productos-ubicacion/:cod_ubicacion', {
                        controller: 'AsgUbicacionCtrl',
                        templateUrl: './templates/prod_ubic_select.html'
                    })
                    //ubicacion edicion
                    .when('/ng-ubicacion-edi/:ubicacionId', {
                        controller: 'UbicacionEdicionCtrl',
                        templateUrl: './templates/ubicacion_edicion.html'
                    })
                    //asigar una ubicacion a muchos productos
                    .when('/ng-promocion-activa', {
                        controller: 'PromocionActivaCtrl',
                        templateUrl: './templates/promocion_activa.html'
                    })

                    //productos que no tienen ubicacion
                    .when('/ng-prd-sin-ubicacion', {
                        controller: 'ProductosPorAsgCtrl',
                        templateUrl: './templates/prduc_sin_ubicacion.html'
                    })
                    //perfil
                    .when('/ng-perfil', {
                        controller: 'PerfilCtrl',
                        templateUrl: './templates/perfil.html'
                    })
                    .when('/ng-modulos', {
                        controller: 'ServicioCtrl',
                        templateUrl: './com/admin/views/modulos.html'
                    })
                    //route for superusuario
                    .when('/ng-usuarios', {
                        controller: 'UsuariosCtrl',
                        templateUrl: './com/superuser/views/usuarios.html'
                    })
                    //enlista los supermercados
                    .when('/ng-supermercados', {
                        controller: 'SupermercadosCtrl',
                        templateUrl: './com/superuser/views/supermercados.html'
                    })
                    .when('/ng-registrasuper', {
                        controller : 'InicioCtrl',
                        templateUrl: './com/superuser/views/registrar.html'
                    })
                    .when('/ng-servicios', {
                        controller : 'ServicioCtrl',
                        templateUrl: './com/superuser/views/servicios.html'
                    })
                    //listas pedidas por el cliente
                    .when('/ng-listas', {
                        controller: 'ListasCtrl',
                        templateUrl: './com/admin/views/listas.html'
                    })
                    .when('/ng-lproceso', {
                        controller: 'ListasCtrl',
                        templateUrl: './com/admin/views/listas-proceso.html'
                    })
                    .when('/ng-ldespachadas', {
                        controller: 'ListasCtrl',
                        templateUrl: './com/admin/views/listas-despachada.html'
                    })

                    //por defecto
                    .otherwise({
                            //redirectTo: '/ng-bienvenido'
                    });
        })

        .run(function ($rootScope, $location ,  localStorageService) {
            if (localStorageService.get('localStorageUser')) {
                if (localStorageService.get('localStorageUser').rol === 'superusuario') {
                    $location.path('/ng-usuarios');
                } 
                else {
                    if ($location.path() === '/plantilla.html'){
                        $location.path('/ng-bienvenido');
                    } else {
                        $location.path($location.path());
                    }
                }
            }

    $rootScope.$on("$routeChangeStart", function (event, next, current) {
        if (localStorageService.get('localStorageDemo')) {
            console.log("redirecccionar al principal");

        } else {
            /*si el usuario no esta logeado*/
            //window.location="http://shoplist-shoplistrk.rhcloud.com/";
            window.location = host;
        }
    });
});
