// app/routes.js

module.exports = function(app, passport) {

	// =====================================
	// HOME PAGE (with login links) ========
	// =====================================
	app.get('/', function(req, res) {
		res.render('index'); // load the index.ejs file
	});

	// =====================================
	// LOGIN ===============================
	// =====================================
	// show the login form
	app.get('/login', function(req, res) {
		// render the page and pass in any flash data if it exists
		res.render('index.html', { message: req.flash('loginMessage') });
		//res.send({ message: req.flash('loginMessage') });
	});

		app.get('/errorlogin', function(req, res) {
		// render the page and pass in any flash data if it exists
		//res.render('index.html', { message: req.flash('loginMessage') });
		//res.send({ message: req.flash('loginMessage') });
		return res.send({ message: req.flash('loginMessage') });
	});

	// process the login form
	app.post('/login', passport.authenticate('local-login', {
            successRedirect : '/profile', // redirect to the secure profile section
            failureRedirect : '/errorlogin', // redirect back to the signup page if there is an error
            failureFlash : true // allow flash messages
		}),
        function(req, res) {
            console.log("hello");

            if (req.body.remember) {
              req.session.cookie.maxAge = 1000 * 60 * 3;
            } else {
              req.session.cookie.expires = false;
            }
        res.redirect('/');
    });

//verificar password antiguo

app.post('/contrasenia', passport.authenticate('local-password-verificar', {
            successRedirect : '/contraseniasuccess', // redirect to the secure profile section
            failureRedirect : '/contraseniafailure', // redirect back to the signup page if there is an error
            failureFlash : true // allow flash messages
		}),
        function(req, res) {
            console.log("hello");

    });


	// =====================================
	// SIGNUP ==============================
	// =====================================
	// show the signup form
	app.get('/signup', function(req, res) {
		// render the page and pass in any flash data if it exists
		res.render('signup.ejs', { message: req.flash('signupMessage') });
	});

	// process the signup form
	app.post('/signup', passport.authenticate('local-signup', {
		successRedirect : '/profile', // redirect to the secure profile section
		failureRedirect : '/signup', // redirect back to the signup page if there is an error
		failureFlash : true // allow flash messages
	}));

	// =====================================
	// UPDATE PASSWORD =========================
	// =====================================

	app.post('/contraseniaupdate', passport.authenticate('local-update-password', {
		successRedirect : '/contraseniaupdatesucess', // redirect to the secure profile section
		failureRedirect : '/contraseniaupdatefailure', // redirect back to the signup page if there is an error
		failureFlash : true // allow flash messages
	}));

	// =====================================
	// PROFILE SECTION =========================
	// =====================================
	// we will want this protected so you have to be logged in to visit
	// we will use route middleware to verify this (the isLoggedIn function)
	app.get('/profile', isLoggedIn, function(req, res) {
		//res.render('plantilla.html', {
			//res.send({user : req.user});
			/*res.render('plantilla.html', {
			user : req.user // get the user out of session and pass to template
		});*/
	   //res.render('ralexs',{title:"hola mundo"});
		//enviar el usuario logeado a la pagina 
         res.send({user : req.user});
	});

	//verificar contrasenia en caso de ser correcta
	app.get('/contraseniasuccess',  function(req, res) {
         res.send({user : req.user});
	});
		//verificar contrasenia en caso de ser erronea
	app.get('/contraseniafailure', function(req, res) {
         res.send(500);
	});



	// =====================================
	// UPDATE PASSWORD =========================
	// =====================================
	app.get('/contraseniaupdatesucess', function(req, res) {
         res.send(200);
	});
		app.get('/contraseniaupdatefailure', function(req, res) {
         res.send(500);
	});

	// =====================================
	// LOGOUT ==============================
	// =====================================
	app.get('/logout', function(req, res) {
		req.logout();
		//req.session.destroy();
		//req.session = null;
		//res.header('Cache-Control', 'no-cache');
		//res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
		
		console.log("cerrar session");
		res.redirect('/');
	});

};

// route middleware to make sure
function isLoggedIn(req, res, next) {

	// if user is authenticated in the session, carry on
	if (req.isAuthenticated())
		return next();
	// if they aren't redirect them to the home page
	res.redirect('/');
}
