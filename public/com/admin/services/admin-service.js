    angular.module('shop.services')
        .service('adminService', function($q, $http){
        var results;
        var productos;

        return {
            doProductos         : doProductos,
            getproductos        : getproductos,
            doCargarProductosBd : doCargarProductosBd
        };

        function doProductos(texto , name_bd) {
            var deferred = $q.defer();
            var url = '/admin/productos/'+ texto + '/' + name_bd;
            $http.get(url)
              .then(function(res) {
                productos = res.data;
                deferred.resolve(productos);
                }, function(err){
                    deferred.resolve(err);
                });

            return deferred.promise;
        };

        function doCargarProductosBd(name_bd) {
            var deferred = $q.defer();
            var url = '/cargar_productos/' + name_bd;
            $http.get(url)
              .then(function(res) {
                deferred.resolve(res);
                }, function(err){
                    deferred.resolve(err);
                });

            return deferred.promise;
        };

        function getproductos(){
            return productos;
        }        

});
