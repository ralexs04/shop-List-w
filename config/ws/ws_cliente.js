
var mysql = require('mysql');
var bcrypt = require('bcrypt-nodejs'); // para encriptar la contraseni a actualizar

var dbconfig = require('../database');
var connection = mysql.createConnection(dbconfig.connection);

//uri's
var cliente = '/cliente/:id_usuario?'; //guarda todos los clientes registrados
var login_cliente = '/loginCliente'; //login del client
var servicios = '/servicios'; //login del client

connection.query('USE ' + dbconfig.database);

module.exports = function(app) {

  app.post(cliente , function(req,res,done){
  var passw = bcrypt.hashSync(req.body.password, null, null);
    //get data
      var data = {
          email   :req.body.email,
          password  :passw,
          nombres :req.body.nombres,
          apellidos :req.body.apellidos,
          ubicacion : req.body.ubicacion
       };
      
      connection.query("INSERT INTO user_cliente SET ? ",data, function(err, rows){
        if(err){
          console.log(err);
          return done("Mysql error, check your query");
        }
        //devuelve ok
        res.send(200);
      });
    })

  app.put(cliente , function(req,res,done){
    //get data
    var id_user = req.params.id_usuario;

    if (req.body.password){
       var passw = bcrypt.hashSync(req.body.password, null, null);
       req.body.password = passw;
    }
    
      connection.query("UPDATE user_cliente SET ?  WHERE id = ? ",[req.body, id_user], function(err, rows){
        if(err){
          console.log(err);
          return done("Mysql error, check your query");
        }
        //devuelve ok
        res.send(200);
      });
    })

  app.get(cliente , function(req,res,done){
    connection.query('USE ' + dbconfig.database);
    var id_user = req.params.id_usuario;
    connection.query("SELECT nombres, apellidos, email, ubicacion FROM user_cliente WHERE id  = ? ", id_user , function(err, rows){
        if (err)
          return res.json({msg: error})
        if (!rows.length)
          return res.json({msg:"Mysql error, check your query"});
        if(rows.length < 1)
          return res.json({ msg: 'Usuario no encontrado'});

        return res.json({msg: "Ok", data : rows})

      });
    })

  app.post(login_cliente, function(req, res, done) {
		var email = req.body.email;
		var password = req.body.password;

    connection.query("SELECT * FROM user_cliente WHERE email = ?",[email], function(err, rows){
        if (err)
          return res.json({msg: error})
        if (!rows.length) {
            //return done(null, false, req.flash('loginMessage', 'Usuario no encontrado.')); // req.flash is the way to set flashdata using connect-flash
            return res.json({msg:"Usuario no encontrado"});
        }
        // if the user is found but the password is wrong
        if (!bcrypt.compareSync(password, rows[0].password))
            //return done(null, false, req.flash('loginMessage', 'Contraseña erronea. !')); // create the loginMessage and save it to session as flashdata
            return res.json({msg:"Contraseña errornea"})

        // all is well, return successful user
        return res.json({msg: "Ok", data : rows})
        //return done(null, rows[0]);
    });
  })

    //guardar los servicios con los que desea el cliente

  app.post(servicios , function(req,res,done){
      //get data

        var data = {
            promocion : req.body.promocion,
            ubicacion_producto : req.body.ubicacion_producto,
            listas : req.body.listas,
            analitica : req.body.analitica
         };
        connection.query('USE ' + req.body.bd);
        connection.query("INSERT INTO servicios set ? ",data, function(err, rows){
          if(err){
            console.log(err);
            return done("Mysql error, check your query");
          }
          //devuelve ok
          connection.query('USE ' + dbconfig.database);
          res.send(200);
        });
      })

}