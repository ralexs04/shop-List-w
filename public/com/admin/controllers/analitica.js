angular.module('shop.controllers')
    .controller('AnaliticaCtrl', AnaliticaCtrl)

function AnaliticaCtrl($scope,
                       localStorageService,
                       $http,
                       notify,
                       $loading) {

    var name_bd = localStorageService.get('localStorageUser').bd_supmercado;

    //$scope.cargar_highchart = cargar_highchart;
    $scope.productos_mas_vendidos = productos_mas_vendidos;
    $scope.productos_stock = productos_stock;
    $scope.filtroactivo = true;
    $scope.filtroinactivo = false;
    $scope.stockactive = true;
    $scope.stockinactive = false;

    function productos_mas_vendidos() {
        if ($scope.cantidad_vendidos) {
            $loading.start('cargando');
            var url = "/productos/vendidos/"+ name_bd +"/" + $scope.cantidad_vendidos;
            $http.get(url).success(function (_data_, status, headers, config) {
                    if (_data_.data) {
                        $scope.filtroactivo = true;
                        $scope.filtroinactivo = false;
                        data = formatdatahighchart(_data_.data);
                        var container  = '#containervendidos';
                        cargar_highchart(data, container);
                    }
                    $loading.finish('cargando');

                      //cargar_user(_data_.data , lista);
                }).error(function (data, status, headers, config) {
                    console.log("Error:" , data);
                    $scope.filtroinactivo = true;
                    $scope.filtroactivo = false;
                    $loading.finish('cargando');
            });
        } else {
            alertaSuccess(notify , "Ingrese un numero para filtrar .!!!")
        }
    }

    function productos_stock() {

        if ($scope.cantidad_stock) {
            $loading.start('cargando');
            var url = "/productos/stock/"+ name_bd +"/" + $scope.cantidad_stock;
            $http.get(url).success(function (_data_, status, headers, config) {
                if (_data_.data) {
                    $scope.stockactive = true;
                    $scope.stockinactive = false;
                    data = formatdatahighchart(_data_.data);
                    
                    var resutl = data;
                    if(data.length > 20){
                        result = data.slice(0, 20)
                    }
                    var nombres = getNombresArray(result);
                    cargarhighchartstock(result, nombres);
                }

                $loading.finish('cargando');

                      //cargar_user(_data_.data , lista);
                }).error(function (data, status, headers, config) {
                    $loading.finish('cargando');
                    console.log("Error:" , data);
                    $scope.stockactive = true;
                    $scope.stockinactive = false;
            });

        } else {
            alertaSuccess(notify , "Ingrese un numero para filtrar .!!!");   
        }

    }

    function getNombresArray(array){
        var names = [];
        for( var i=0;i<array.length;i++ ){
            names.push(array[i].name);
        }
        return names;
    }

    function formatdatahighchart(data) {
        var str = JSON.stringify(data);
        str = str.replace(/nom_prod/g, 'name');
        str = str.replace(/cant_vendido/g, 'y');
        str = str.replace(/cant_prod/g, 'y');
        return JSON.parse(str);
    }

    function cargar_highchart(data, container) {

        $(container)
        .highcharts({
            chart: {
              plotBackgroundColor: null,
              plotBorderWidth: null,
              plotShadow: false,
              type: 'pie'
            },
            title: {
              text: 'Productos mas vendidos hasta la fecha actual'
            },
            tooltip: {
              pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
              pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                  enabled: true,
                  format: '<b>{point.y}</b>',
                  style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
                    fontSize: '10px'
                  }
                }
              }
            },
            series: [{
              name: 'Vendido',
              colorByPoint: true,
              data: data
            }]
        });
    }

    function cargarhighchartstock(data, nombres) {
        $('#containerstock').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Stock'
            },
            subtitle: {
                text: 'Estilo: Barras'
            },
            xAxis: {
                categories: nombres,
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Stock'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y} unidad(es) </b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
              name: 'Cantidad de Stock',
              colorByPoint: true,
              data: data
            }]
        });

    }

    function noResultado() {
       $scope.filtroactivo = $scope.filtroactivo === true ? false : true;
       $scope.filtroinactivo = $scope.filtroinactivo === true ? false : true;
    }
}

function alertaSuccess (notify, mensaje){
    notify({
        message: mensaje,
        classes: 'alert-success', // alert-danger or alert-success
        templateUrl: '',
        position: 'center',
        duration: 2000
    });
};
