var nodemailer = require("nodemailer");
var fs = require('fs');
var path = require('path');
var mysql = require('mysql');
var bcrypt = require('bcrypt-nodejs'); // para encriptar la contraseni a actualizar

var dbconfig = require('../database');
var connection = mysql.createConnection(dbconfig.connection);

//uri's
var listas          = '/listas/:name_bd/:estado?'; //obtiene las listas
var listasall          = '/listasall/:name_bd'; //obtiene las listas
var listas_products = '/listas/productos/:name_bd/:id_lista'; //obtiene las listas con sus productos
var lista_estados_cliente = '/lista/estados/:name_bd/:id_lista'; //obtiene una lista (movil)
var servicios       = '/serviciosdisponibles/:name_bd'; //obtiene los servicios disponibles
var servicios_supermercado = '/servicios_supermercado/:name_bd'; //guarda si el admin instala un  nuevo modulo
var cargar_productos = '/cargar_productos/:name_bd'; //guarda si el admin instala un  nuevo modulo
var recuperar_clave_cliente = '/recuperar/clave/cliente'; //proceso de recuperacion de clave
var actualizar_clave_cliente = '/actualizar/clave/cliente/:id_cliente'; //proceso de recuperacion de clave
var productos_vendidos = '/productos/vendidos/:name_bd/:cantidad'; //permite obtener los productos mas vendidos
var productos_stock = '/productos/stock/:name_bd/:cantidad_stock'; //permite determinar los productos del stock
connection.query('USE ' + dbconfig.database);

module.exports = function(app) {

  app.post(recuperar_clave_cliente , function(req,res,done){

    var email = req.body.email;
    connection.query("SELECT * FROM user_cliente WHERE email = ?",[ email ], function(err, rows) {
      if(err){
        console.log(err);
        return done("Mysql error, check your query");
      }

      if (rows.length < 1){
        return res.json({status: 400, data : rows})
      } else {
        cargarCodigo(email, rows[0].nombres);
      }

      return res.json({status: 200, data : rows})
      
    });
  })

  function cargarCodigo (email, username){
    connection.query("SELECT * FROM codigos",[], function(err, rows) {
      if(err){
        console.log(err);
      }
      //TODO cojer un codigo aleatorio para enviar al correo
      var codigo = rows[Math.floor(Math.random()*rows.length)];
      enviarCorreoClienteClave(email, codigo.codigo, username)
      
    });
  }

  app.put(recuperar_clave_cliente , function(req,res,done){
    var codigo = req.body.codigo;
    connection.query("SELECT * FROM codigos WHERE codigo = ?",[ codigo ], function(err, rows) {
      if(err){
        console.log(err);
        return done("Mysql error, check your query");
      }
      if (rows.length < 1){
        return res.json({status: 400, data : rows})
      }
      return res.json({status: 200, data : rows})     
    });
  })

  app.post(actualizar_clave_cliente , function(req,res,done){

    var passw = bcrypt.hashSync(req.body.clave, null, null);
    var id = req.params.id_cliente;

    connection.query("UPDATE user_cliente SET password = ? WHERE id = ? ",[passw , id], function(err, rows){
        if(err){
          console.log(err);
          return done("Mysql error, check your query");
        }
        //devuelve ok
        res.send(200);
      });
   
  })

  app.get(lista_estados_cliente , function(req,res,done){

    var name_bd = req.params.name_bd;
    var id_lista = req.params.id_lista;
    connection.query('USE ' + name_bd);

    connection.query("SELECT * FROM listas WHERE id  = ? ", [id_lista] ,function(err,rows){
      if (err)
        return done(err);
      if (!rows.length)
         return done(null, false, req.flash('msg', 'Mysql error, check your query"'));
      if(rows.length < 1)
        return done(nll, false, req.flash('msg', 'Lista no encontrada"'));
          // all is well, return successful user
      return res.json({msg: "Ok", data : rows})
      connection.query('USE ' + dbconfig.database);
    });
  }) 

  app.get(productos_vendidos , function(req,res,done){

    var name_bd = req.params.name_bd;
    var cantidad = req.params.cantidad;
    connection.query('USE ' + name_bd);

    connection.query("SELECT nom_prod, cant_vendido FROM producto WHERE cant_vendido  >= ? ", [cantidad] ,function(err,rows){
      if (err)
        return done(err);
      if (!rows.length)
         return done(null, false, req.flash('msg', 'Mysql error, check your query"'));
      if(rows.length < 1)
        return res.json({msg: "No contiene productos vendidos superiores a esa cantidad"})
          // all is well, return successful user
      return res.json({msg: "Ok", data : rows})
      connection.query('USE ' + dbconfig.database);
    });
  })

  app.get(productos_stock , function(req,res,done){

    var name_bd = req.params.name_bd;
    var cantidad = req.params.cantidad_stock;
    connection.query('USE ' + name_bd);

    connection.query("SELECT nom_prod, cant_prod FROM producto WHERE cant_prod  <= ? ", [cantidad] ,function(err,rows){
      if (err)
        return done(err);
      if (!rows.length)
         return done(null, false, req.flash('msg', 'Mysql error, check your query"'));
      if(rows.length < 1)
        return res.json({msg: "No contiene productos con stock"})
          // all is well, return successful user
      return res.json({msg: "Ok", data : rows})
      connection.query('USE ' + dbconfig.database);
    });
  }) 


  app.get(listas , function(req,res,done){

    var name_bd = req.params.name_bd;
    var estado = req.params.estado;
    connection.query('USE ' + name_bd);

    connection.query("SELECT * FROM listas WHERE " + estado + " = ? " , [1] ,function(err,rows){
      if (err)
        return done(err);
      if (!rows.length)
         return done(null, false, req.flash('msg', 'Mysql error, check your query"'));
      if(rows.length < 1)
        return done(nll, false, req.flash('msg', 'Lista no encontrada"'));
          // all is well, return successful user
      return res.json({msg: "Ok", data : rows})
      connection.query('USE ' + dbconfig.database);
    });
  })

  app.get(listasall , function(req,res,done) {

    var name_bd = req.params.name_bd;
    connection.query('USE ' + name_bd);

    connection.query("SELECT * FROM listas" , [] ,function(err,rows){
      if (err)
        return done(err);
      if (!rows.length)
         return done(null, false, req.flash('msg', 'Mysql error, check your query"'));
      if(rows.length < 1)
        return done(nll, false, req.flash('msg', 'Lista no encontrada"'));
          // all is well, return successful user
      return res.json({msg: "Ok", data : rows})
      connection.query('USE ' + dbconfig.database);
    });
  })	

  app.put(listas , function(req,res,done){

    var name_bd = req.params.name_bd;
    connection.query('USE ' + name_bd);
    var id_lista = req.body.id;

    if (req.body.estado_despachada === 1){
      console.log("cambiar fecha");
      req.body.date_time_despacho = new Date();
    }
    
    connection.query("UPDATE listas set ? WHERE id = ?",[ req.body , id_lista], function(err, rows) {
      if(err){
        console.log(err);
        return done("Mysql error, check your query");
      }

      res.send(200);
      connection.query('USE ' + dbconfig.database);
    });
  })

  app.get(servicios , function(req,res,done){

    var name_bd = req.params.name_bd;
    
    connection.query('USE ' + name_bd);
    connection.query("SELECT * FROM servicios ", [] ,function(err,rows){
      if (err)
        return done(err);
      if (!rows.length)
         return done(null, false, req.flash('msg', 'Mysql error, check your query"'));
      if(rows.length < 1)
        return res.json({msg: "no contiene servicios"})
          // all is well, return successful user
      return res.json({msg: "Ok", data : rows})
   
      connection.query('USE ' + dbconfig.database);
    });
  })  

  app.post(listas , function(req,res,done){

    var name_bd = req.params.name_bd;
    connection.query('USE ' + name_bd);
    var fecha_pedido = new Date();
    var data = {
      id_user_cliente   : req.body.id_user,
      date_time_pedido  : fecha_pedido,
      estado_inicial    : 1,
      estado_proceso    : 0,
      estado_despachada : 0,
      entrega           : req.body.entrega
    }
    var productos = req.body.productos;

    connection.query("INSERT INTO listas set ? ",data, function(err, rows){
      if(err){
        console.log(err);
        return done("Mysql error, check your query");
      }
      var id_listaIsertada = rows.insertId;
      var cont = 0;
      for (var i = 0 ; i < productos.length ; i++) {
        var datapro = {
          fk_lista    : rows.insertId,
          fk_producto : productos[i].codigo,
          cantidad    : productos[i].cantidad
        }

        actualizarStockProducto(productos[i]);

        connection.query("INSERT INTO lista_producto set ? ",datapro, function(err, rows){
          if(err){
            console.log(err);
            return done("Mysql error, check your query");
          }
          cont ++;
          if (cont === (productos.length-1)){
            //res.send(200);
            return res.json({msg: "Ok", data : id_listaIsertada})
          }
        })

      }
    });
  })

function actualizarStockProducto(producto){
    var codigo = producto.codigo;
    console.log("codigo ", codigo);
    connection.query("SELECT cant_prod, cant_vendido FROM producto WHERE codigo = ? ",[codigo], function(err, rows){
    //connection.query("INSERT INTO producto set ? ",req.body, function(err, rows){
      if(err){// error en consulta
        console.log(err);
      }

      var cantidad = rows[0].cant_prod - producto.cantidad;
      //actualizar la cantidad del producto vendido
      var cantidad_vendido = rows[0].cant_vendido + producto.cantidad; 

      if (cantidad < 0 ){
        cantidad = 0;
      }

      connection.query("UPDATE producto SET cant_prod = ?, cant_vendido = ? WHERE codigo = ? ",[cantidad, cantidad_vendido, codigo], function(err, rows){
      //connection.query("INSERT INTO producto set ? ",req.body, function(err, rows){
        if(err){// error en consulta
          console.log(err);
        }
        
       });

     });
}

  app.get(listas_products , function(req,res,done){

    var name_bd = req.params.name_bd;
    var id_lista = req.params.id_lista;
    connection.query('USE ' + name_bd);
    connection.query("SELECT p.cod_barras_prod, p.nom_prod , p.pvp_prod , lp.cantidad FROM lista_producto lp , producto p WHERE lp.fk_lista = ? and lp.fk_producto = p.codigo" , [id_lista] ,function(err,rows){
      if (err)
        return done(err);
      if (!rows.length)
         return done(null, false, req.flash('msg', 'Mysql error, check your query"'));
      if(rows.length < 1)
        return done(nll, false, req.flash('msg', 'Lista no encontrada"'));
          // all is well, return successful user
      return res.json({msg: "Ok", data : rows})
      connection.query('USE ' + dbconfig.database);
    });
      
  })

    app.put(servicios_supermercado , function(req,res,done){
      
    var name_bd = req.params.name_bd;
    var id_servicio = req.body.id;

    connection.query('USE ' + name_bd); 
    connection.query("UPDATE servicios set ? WHERE id = ?",[ req.body , id_servicio], function(err, rows) {
      if(err){
        console.log(err);
        return done("Mysql error, check your query");
      }

      res.send(200);
      connection.query('USE ' + dbconfig.database);
    });

  })

//cargar productos extras en la bd 
  app.get(cargar_productos , function(req,res,done){
    var name_bd = req.params.name_bd;
    connection.query('USE ' + name_bd);
    var sql = fs.readFileSync(cargarUrl()).toString();
    connection.query(sql, function(err, result){
       if(err){
            return done(err);
        }
        return res.json({msg: "Ok", data : result});
        connection.query('USE ' + dbconfig.database);
    })

  })

  function cargarUrl(){
    var url = path.join(__dirname, '../../scripts/productos.sql');
    return url;
  }  
}

function enviarCorreoClienteClave(email, codigo, username){
  //conexion al gmail 
  var smtpTransport = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
      user: "rk.cis.movil@gmail.com",
    pass: "administrador1991"
    }
  });

var mensaje = "Su codigo de activacion es : " + codigo;

var mailOptions = {
  from: "Shop-List le informa ..!!!! ", // sender address
  to: email, // list of receivers
  subject: "Hola " +username, // Subject line
  html: mensaje // html body
  }
            
  smtpTransport.sendMail(mailOptions, function(error, response){
    if(error){
    console.log("error " , error);
  }else{
    console.log("correo para recuperacion de clave correcto");
  }

  }); 
}