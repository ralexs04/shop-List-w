angular.module('shop.controllers')
    .controller('ListasCtrl', ListasCtrl)
    .filter('filterColums', function(){
    
    return function(productos, cantidad) {
     if (!cantidad) {
       return productos.slice(0, 1);
     } else {
       return productos.slice(0, cantidad);
     }
   };
});

function ListasCtrl($scope,
                    localStorageService,
                    $http,
                    notify,
                    $loading,
                    $timeout){
	var name_bd = localStorageService.get('localStorageUser').bd_supmercado;
    var url_listas = "/listas/" + name_bd; 
	    
    $scope.cargar_listas = cargar_listas;
    $scope.data = [];
    $scope.cantlist = {};
    $scope.cargar_mas_productos = cargar_mas_productos;
    $scope.cambiarEstado = cambiarEstado;
    $scope.cargar_nombre = cargar_nombre;
    $scope.cargar_nombre_default = cargar_nombre_default;
    $scope.activar_exportar = activar_exportar;
    $scope.activarexportar = false;
    var index = 1;
    $scope.ver = {};


    /*paso  1 cargar las listas disponibles con estado 0 === no despacado*/
    /*paso 2 enviar a cargar cada lista con sus respectivos productos*/

    function cargar_listas(estado){
        $scope.estadoLista = estado;
        url_listas +="/"+estado;
    	$http.get(url_listas).success(function (data, status, headers, config) {
            var listas = data.data;
            for (var i = 0 ; i < listas.length; i++) {
				cargar_productos_lista(listas[i]);
            }
                  
            }).error(function (data, status, headers, config) {
                alertaSuccess(notify , 'No contiene listas');
                console.log("Error:" , data);
        });
    }

    function cargar_productos_lista(lista){
    	var url = "/listas/productos/"+ name_bd +"/" +lista.id;
    	$http.get(url).success(function (_data_, status, headers, config) {
    		//$scope.data.push([{results : _data_.data} , {lista : lista}]);
            
            cargar_usuario_lista(_data_.data , lista);
                  //cargar_user(_data_.data , lista);
            }).error(function (data, status, headers, config) {
                console.log("Error:" , data);
        });
    }

    function cargar_usuario_lista(result, lista_){
        var url = "/cliente/" +lista_.id_user_cliente;
        $http.get(url).success(function (_data_, status, headers, config) {
            $scope.data.push([{results : result} , {lista : lista_} , { user :  _data_.data }, index]);
            cargar_mas_productos(result, index);
            index++;      
            }).error(function (data, status, headers, config) {
                console.log("Error:" , data);
        });
    }

    function cargar_mas_productos(lista, name){
        $scope.cantlist[name] = $scope.cantlist[name] === 1 ? lista.length : 1;
    }

    function cargar_nombre_default(index){
        $scope.ver[index] = "ver mas";
    }

    function cargar_nombre(index) {
        
        if ($scope.ver[index] === "ver mas") {
            $scope.ver[index] = "ver menos";
        } else {
            $scope.ver[index] = "ver mas";
        }
    }

    function cambiarEstado(estado, lista, arreglo){
        var data , estado;
        var _id = lista.id
        if (estado === 'estado_inicial'){
            data = {estado_inicial : 1 , estado_proceso : 0 , estado_despachada : 0 , id : _id }
            estado = "Estado Inicial"
        }
        if (estado === 'estado_proceso'){
            data = {estado_inicial : 0 , estado_proceso : 1 , estado_despachada : 0 , id : _id }
            estado = "Estado en Proceso"
        }
        if (estado === 'estado_despachada'){
            estado = "Estado Despachada"
            data = {estado_inicial : 0 , estado_proceso : 0 , estado_despachada : 1 , id : _id , date_time_despacho : new Date()}
        }

        llamarAlerta(estado, lista, arreglo, data, estado, $scope);
        /**/
    }


    function llamarAlerta (estado , lista, arreglo, data, estado, $scope) {
        swal({  title: "¿Esta seguro? ",
                text: "El estado de la lista se cambiará a : " + "\"" + estado + "\"" +"!",
                type: "warning",   showCancelButton: true, 
                confirmButtonColor: "#DD6B55", 
                confirmButtonText: "Si",
                cancelButtonText : "No",
                closeOnConfirm: false }, 
            function(isConfirm) {
                console.log("is confirm" , isConfirm );
                if (isConfirm) {
                    $scope.cambiar(estado, lista, arreglo, data); 
                   }
                
        });
    }

    $scope.cambiar = function(estado, lista, arreglo, data) {
        console.log("es hora de cambiar de estado");
        
        $timeout(function () {
            console.log("callback para actualizar lista");
        }, 1000);
           
        $http.put(url_listas , data).success(function (data, status, headers, config) {
            $scope.data.splice($scope.data.indexOf(arreglo), 1);
            swal({title: "Cambio satisfactorio", 
                      text : "Su estado a sido cambiada.",
                      type : "success"});
            
            }).error(function (data, status, headers, config) {
                console.log("Error:" , data);
        });
    }
    
    function activar_exportar(index) {
        $scope.activarexportar = $scope.activarexportar === true ? false : true;
        $scope.cargar_id_ex = index;
    }
}

//alerta
    function alertaError (notify, mensaje){
        notify({
            message: mensaje,
            classes: 'alert-danger', // alert-danger or alert-success
            templateUrl: '',
            position: 'center',
            duration: 2000
        });
    };

     function alertaSuccess (notify, mensaje){
        notify({
            message: mensaje,
            classes: 'alert-success', // alert-danger or alert-success
            templateUrl: '',
            position: 'center',
            duration: 2000
        });
    }
