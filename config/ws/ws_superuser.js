
var mysql = require('mysql');
var bcrypt = require('bcrypt-nodejs'); // para encriptar la contraseni a actualizar

var dbconfig = require('../database');
var connection = mysql.createConnection(dbconfig.connection);

//uri's
var usuarios_admin = '/usuarios/admin'; //obtiene todos los usuarios administradores
var usuario_admin = '/usuario/admin'; //guarda un nuevo usuario
var edit_usr_admin = '/usuario/admin/:id'; //editar un nuevo usuario
var supermercados = '/supermercados'; //obtiene todos los supermercados
var get_user = '/get_user/:username'; //obtener true o false (si existe usuario)
var servicios_super = '/servicios_super'; //obtiene todos los servicios



connection.query('USE ' + dbconfig.database);

module.exports = function(app) {
//retornar si existe o no un usuario para el resgistro

app.get( servicios_super , function(req,res,done){
    var username = req.params.username;
    connection.query("SELECT * FROM servicios", [] ,function(err,rows){
             if (err)
                    return done(err);
                if (!rows.length)
                    return done(null, false, req.flash('loginMessage', 'Mysql error, check your query"'));
                 if(rows.length < 1)
                    return res.send(JSON.stringify({"mensaje " : "No existen servicios"}));
                res.send(JSON.stringify(rows));
        });
  });

    //editar datos de un usuario administrador
  app.put(servicios_super , function(req,res,done){
      var id = req.body.id;
      var data = {
        descripcion: req.body.descripcion,
        titulo: req.body.titulo,
        namespace: req.body.namespace
      }
      connection.query("UPDATE servicios SET ? WHERE id = ? ",[data, id], function(err, rows){
           if(err){
                console.log(err);
                return done("Mysql error, check your query");
           }
           res.send(200);
        });
    });

  app.post(servicios_super , function(req,res,done){

    var data = {
        descripcion: req.body.descripcion,
        titulo: req.body.titulo,
        namespace: req.body.namespace
      }

      connection.query("INSERT INTO servicios SET ?",[data], function(err, rows){
           if(err){
                console.log(err);
                return done("Mysql error, check your query");
           }
           res.send(200);
        });
    });

  app.get( get_user , function(req,res,done){
    var username = req.params.username;
    connection.query("SELECT username FROM users WHERE username = ?", [username] ,function(err,rows){
             if (err)
                    return done(err);
                if (!rows.length)
                    return done(null, false, req.flash('loginMessage', 'Mysql error, check your query"'));
                 if(rows.length < 1)
                    res.send(false);
                res.send(true);
        });
  });

    //obtiene todos los usuarios administradores
    app.get( usuarios_admin , function(req,res,done){
        connection.query("SELECT * FROM users",
                            [] ,function(err,rows){

          if (err)
            return done(err);
          if (!rows.length)
            return done(null, false, req.flash('errormensaje', 'Mysql error, check your query"'));
          if(rows.length < 1)
            return res.send(JSON.stringify({"mensaje " : "No existen usuarios"}));

          res.send(JSON.stringify(rows));
        });
    });

    //guardar un nuevo usuario administrador en la base de datos

    app.put(usuario_admin , function(req,res,done){

      var password = req.body.admin_contrasenia;
      var fecha_actual = new Date();

      var contrasenia = bcrypt.hashSync(password, null, null)//encriptar la contrasenia
          var data = {
          username : req.body.admin_usuario,
          password : contrasenia,
          email    : req.body.admin_correo,
          nombres  :  req.body.admin_nombres,
          apellidos  :  req.body.admin_apellidos,
          rol  :  req.body.admin_rol,
          fecha_creacion : fecha_actual,
          bd_supmercado : req.body.bd_supmercado
       };
      //insertar en mysql
      connection.query("INSERT INTO users SET ?",[ data ], function(err, rows) {
        if(err){
          console.log(err);
          return done("Mysql error, check your query");
        }
        res.send(rows);
      });
    });

    //editar datos de un usuario administrador
    app.put(edit_usr_admin , function(req,res,done){
      var id_admin     = req.params.id;
      var fecha_actual = new Date();
      //verificar el cambio de contrasenia para la  encriptacion
      var cambiopw  = req.body.admin_camibiopassw; 
      var passw  = req.body.admin_contrasenia; 

      if (cambiopw === true){
        passw = bcrypt.hashSync(req.body.admin_contrasenia, null, null);
      }
      // obtener datos
      var data = {
        username : req.body.admin_usuario,
        password : passw,
        email    : req.body.admin_correo,
        nombres  :  req.body.admin_nombres,
        apellidos  :  req.body.admin_apellidos,
        rol  :  req.body.admin_rol,
        fecha_modificacion:fecha_actual
      };

      connection.query("UPDATE users SET ? WHERE id = ? ",[data , id_admin], function(err, rows){
           if(err){
                console.log(err);
                return done("Mysql error, check your query");
           }
           res.send(200);
        });
    });

    //obtiene todos los supermercados 
       
    app.get( supermercados , function(req,res,done){

        connection.query("SELECT bd_supmercado FROM users",
                            [] ,function(err,rows){
          var lista_supermercados = []                   

          if (err)
            return done(err);
          if (!rows.length)
            return done(null, false, req.flash('msg', 'Mysql error, check your query"'));
          if(rows.length < 1)
            return res.send(JSON.stringify({"msg " : "No existen supermercados"}));
              var supermercadoslen =  rows.length;
              var cont = 0 , contadorfinal =0;
              for(var i = 0; i < supermercadoslen; i++){
                var obj = rows[i];
                for(var key in obj){
                    var name_bd = obj[key];
                    if (name_bd){
                      contadorfinal++;
                      //llamar a la bd
                      connection.query('USE ' + name_bd);
                      connection.query("SELECT * FROM supermercado, servicios",
                            [] ,function(err,rows){
                        lista_supermercados.push(rows[0]);
                        
                          cont ++;
                          if ( cont === contadorfinal ){
                          return res.json({data : lista_supermercados});
                        }

                      });
                   }
                }
            }
          connection.query('USE ' + dbconfig.database);
        });
    });
    //guardar supermercado

    app.post(supermercados , function(req,res,done){
    //get data
      var bd = req.body.bd; //obtener el nombre de la bd para

      /*var client = mysql.createConnection({
        host: "localhost",
        database: bd,
        user: "root",
        password: ""
      });*/

      connection.query('USE ' + bd);

      var data = {
          nombre:req.body.nombre,
          nombre_bd:req.body.bd,
          direccion:req.body.direccion,
          id_admin :req.body.id_admin,
          id_paypal:req.body.id_paypal,
          id_analitica_paypal : req.body.id_analitica_paypal
       };
      
      connection.query("INSERT INTO supermercado set ? ",data, function(err, rows){
        if(err){
          console.log(err);
          return done("Mysql error, check your query");
        }
        //devuelve ok
        connection.query('USE ' + dbconfig.database);
        res.send(200);
      });
      //connect.end();
    })

    app.put(supermercados , function(req,res,done){
    //get data
      var id_supermercado = req.body.id;
      var data = {
          nombre:req.body.nombre,
          direccion:req.body.direccion,
          id_admin:req.body.id_admin
       };
       
      
      connection.query("UPDATE supermercado set ? WHERE id = ? ",[data , id_supermercado],function(err, rows){
        if(err){
          console.log(err);
          return done("Mysql error, check your query");
        }
        //devuelve ok
        res.send(200);
      });
    });

}
