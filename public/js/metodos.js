//metodos globaels

 function alertaSuccess (notify, mensaje){
        notify({
            message: mensaje,
            classes: 'alert-success', // alert-danger or alert-success
            templateUrl: '',
            position: 'center',
            duration: 2000
        });
    };

function alertaError (notify, mensaje){
        notify({
            message: mensaje,
            classes: 'alert-danger', // alert-danger or alert-success
            templateUrl: '',
            position: 'center',
            duration: 2000
        });
    };