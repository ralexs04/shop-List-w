    angular.module('shop.services')
        .service('superService', function($q, $http){
        var results;
        var administradores;
        var servicios;

        return {
            doAdminitradores    : doAdminitradores,
            doSupermercados     : doSupermercados,
            doServicios         : doServicios,
            getAdministradores  : getAdministradores,
            getServicios        : getServicios
        };

        function doAdminitradores() {
            var deferred = $q.defer();

            $http.get("/usuarios/admin")
              .then(function(res) {
                administradores = res.data;
                deferred.resolve(administradores);
                }, function(err){
                    deferred.resolve(err);
                });

            return deferred.promise;
        }

        function doSupermercados(){
            var deferred = $q.defer();
            $http.get("/supermercados")
              .then(function(res) {
                results = res.data;
                deferred.resolve(results.data);
                }, function(err){
                    deferred.resolve(err);
                });

            return deferred.promise;
        }
        function doServicios(){
            var deferred = $q.defer();
            $http.get("/servicios_super")
              .then(function(res) {
                servicios = res.data;
                deferred.resolve(servicios);
                }, function(err){
                    deferred.resolve(err);
                });

            return deferred.promise;
        }


        function getAdministradores(){
            return administradores;
        }
        function getServicios(){
            return servicios;
        }        

});
