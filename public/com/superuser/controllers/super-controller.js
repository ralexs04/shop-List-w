
angular.module('shop.controllers', ['cgNotify'])
    .controller('UsuariosCtrl', ['$scope', 
                                 '$http', 
                                 'notify', 
                                 'superService', 
                                 function($scope, $http, notify, superService){

        $scope.usuarios_admin        = usuarios_admin;
        $scope.checkPassword         = checkPassword;
        $scope.guardar_administrador = guardar_administrador;
        $scope.limpiar_modal_admin   = limpiar_modal_admin;
        $scope.editar_admin          = editar_admin;
        $scope.guardar_edit_admin    = guardar_edit_admin;


        //variables para el guardado y editado de usuario
        $scope.isguardado = true;
        $scope.iseditado  = false;

        function usuarios_admin () {
        
            superService.doAdminitradores().then(function(_administradores_) {
                $scope.usuariosAdministradores = _administradores_;
            });
        };

        //TODO : rg verificar que las contraseñas considan

        function checkPassword (modeloUserAdminForm , modeloUser ) {
            modeloUserAdminForm.admin_contraseniarp.$error.dontMatch = 
            modeloUser.admin_contrasenia !== modeloUser.admin_contraseniarp;
        };

        function guardar_administrador(modeloUser){
            var url = "/usuario/admin";
            modeloUser.bd_supmercado = 'unknown'
            $http.put(url, modeloUser)
                        .success(function (data, status, headers, config) {
                            // redireccionar a la lista de promociones
                            $scope.usuariosAdministradores.splice(1, 0 , {
                                   nombres   : modeloUser.admin_nombres, 
                                   apellidos : modeloUser.admin_apellidos, 
                                   email     : modeloUser.admin_correo, 
                                   rol       : modeloUser.admin_rol
                            });
                            //$scope.usuariosAdministradores.splice($scope.usuariosAdministradores.indexOf(modeloUser), 1);
                            console.log("guardado correctamente ");
                                
                        }).error(function (data, status, headers, config) {
                        console.log("error al guardar");
                            metodos.guardado_incorrecto(notify);
                            console.log("Ops: " + data);
                });
        }

        function editar_admin(user){
            $scope.isguardado = false;
            $scope.iseditado  = true;
            //cargar datos en el modelo
            $scope.constrasenia_original = user.password;

            $scope.modeloUser = { 
                admin_nombres   : user.nombres,
                admin_apellidos : user.apellidos,
                admin_correo    : user.email,
                admin_usuario   : user.username,
                admin_rol       : user.rol,
                admin_contrasenia:user.password,
                admin_id        : user.id,
                admin_camibiopassw : false
            };
        }

        function guardar_edit_admin(user){
            console.log("el usuario a guardar editado es ", user);
                var url = '/usuario/admin/' + user.admin_id;

                if ($scope.constrasenia_original !== user.admin_contrasenia){
                    console.log("las contrasenas han sido cambiadas");
                    user.admin_camibiopassw = true;
                }
                $http.put(url, user)
                        .success(function (data, status, headers, config) {
                            console.log("se guardo correctamente" + JSON.stringify(data));
                            //actulizar la vista
                            usuarios_admin();
                            //presentar mesaje de guardado satisfactoriamente
                            metodos.guardado_correcto(notify , 'Guardado Satisfactoriamente ...!!!');

                        }).error(function (data, status, headers, config) {
                            metodos.guardado_incorrecto(notify);
                            console.log("Ops: " + data);
                });
        }

        function limpiar_modal_admin (){
            $scope.isguardado = true;
            $scope.iseditado  = false;
            console.log("hora de limpiar");
            document.getElementById('nombres').value = "";           
            document.getElementById('apellidos').value = "";           
            document.getElementById('usuario').value = "";           
            document.getElementById('contrasenia').value = "";
            document.getElementById('correo').value = "";
        }
}])

.controller('SupermercadosCtrl', ['$scope', 
                                  '$http', 
                                  'notify',
                                  'superService', 
                                  function($scope, $http, notify, superService){


    $scope.getSupermercados     = getSupermercados;
    $scope.getAdministrador     = getAdministrador;
    $scope.getAdministradores   = getAdministradores;
    $scope.saveSupermercado     = saveSupermercado;
    $scope.editar_supermercado  = editar_supermercado;
    $scope.guardar_edit_supermercado  = guardar_edit_supermercado;

    //variables para el guardado y editado de usuario
    $scope.isguardado = true;
    $scope.iseditado  = false;

    function getSupermercados () {
        console.log("cargar los supermercados")
        superService.doSupermercados().then(function(_supermercados_) {
                console.log("supermercados " , _supermercados_);
                $scope.listasupermercados = _supermercados_;
            });
        };

    function getAdministrador (id_admin){
        var results = superService.getAdministradores();
        var administrador = metodos.getAdminId(results, id_admin);
        return administrador;    
    }    

    function getAdministradores (){
        $scope.getAllAdministradores = superService.getAdministradores();
    }

    function saveSupermercado(modeloUser, administrador){
        
         modeloUser.id_admin = administrador.id;
        
         $http.post("/supermercados", modeloUser)
            .success(function (data, status, headers, config) {
                console.log("superm " , modeloUser);

            $scope.listasupermercados.splice(0, 0 , modeloUser);

            metodos.guardado_correcto(notify , 'Guardado Satisfactoriamente ...!!!');
            console.log("guardado correctamente ");
                                
            }).error(function (data, status, headers, config) {
            console.log("error al guardar");
                metodos.guardado_incorrecto(notify);
                console.log("Ops: " + data);
        });
    }

    function editar_supermercado(supermercado){
        
        $scope.isguardado = false;
        $scope.iseditado  = true;
        //cargar datos en el modelo

        $scope.modeloUser = supermercado;
        var administrador = getAdministrador(supermercado.id_admin);
        $scope.adminseleccionado = administrador.nombres;
        //console.log("los datos cargados son " , $scope.modeloUser);

    }

    function guardar_edit_supermercado(supermercado, admin){

        if (admin)
            supermercado.id_admin = admin.id;
        
        $http.put("/supermercados", supermercado)
            .success(function (data, status, headers, config) {

                getSupermercados();
                //actulizar la vista
                //usuarios_admin();
                //presentar mesaje de guardado satisfactoriamente
                metodos.guardado_correcto(notify , 'Guardado Satisfactoriamente ...!!!');

            }).error(function (data, status, headers, config) {
                metodos.guardado_incorrecto(notify);
                console.log("Ops: " + data);
        });
    }
}])

.controller('ServicioCtrl', ['$scope', 
                                  '$http', 
                                  'notify',
                                  'superService',
                                  'localStorageService',
                                  '$loading',
                                  '$timeout',
                                  function($scope, $http, notify, superService ,localStorageService, $loading, $timeout){


    $scope.getServicios         = getServicios;
    $scope.saveService          = saveService;
    $scope.guardar_edit_servicio= guardar_edit_servicio;
    $scope.editar_service       = editar_service;
    $scope.getCancelar          = getCancelar;
    $scope.limpiar_modal        = limpiar_modal;
    $scope.habilitarmodulos     = habilitarmodulos;
    $scope.instalarmodulo       = instalarmodulo;
    $scope.desinstalarmodulo    = desinstalarmodulo;

    //variables para el guardado y editado de usuario
    $scope.isguardado = true;
    $scope.iseditado  = false;

    var name_bd = localStorageService.get('localStorageUser').bd_supmercado;

    function getServicios () {
        console.log("cargar los servicios")
        superService.doServicios().then(function(_servicios_) {
                console.log("servicios >" , _servicios_);
                if (_servicios_.length){
                    $scope.listaservicios = _servicios_;
                }else{
                    metodos.guardado_correcto(notify , 'No contiene servicios guardados ...!!!');
                }
            });
    };

    function getCancelar () {
        //restablecers
        superService.doServicios().then(function(_servicios_) {
                $scope.listaservicios = _servicios_;
    });
    };

    function getServicio (id_servicio){
        var results = superService.getServicios();
        var servicio = metodos.getAdminId(results, id_servicio);
        return servicio;    
    }   

    function saveService(modeloServicio){
        modeloServicio.id = '';
        console.log(">>> " , modeloServicio);
              
         $http.post("/servicios_super", modeloServicio)
            .success(function (data, status, headers, config) {
                console.log("superm " , modeloServicio);

            $scope.listaservicios.splice(0, 0 , modeloServicio);

            metodos.guardado_correcto(notify , 'Guardado Satisfactoriamente ...!!!');
            console.log("guardado correctamente ");
                                
            }).error(function (data, status, headers, config) {
            console.log("error al guardar");
                metodos.guardado_incorrecto(notify);
                console.log("Ops: " + data);
        });
    }

    function guardar_edit_servicio(servicio){
        console.log("el servicio editado a guardar es ", servicio);
        $http.put("/servicios_super", servicio)
            .success(function (data, status, headers, config) {
                getServicios();
                //actulizar la vista
                //usuarios_admin();
                //presentar mesaje de guardado satisfactoriamente
                metodos.guardado_correcto(notify , 'Guardado Satisfactoriamente ...!!!');

            }).error(function (data, status, headers, config) {
                metodos.guardado_incorrecto(notify);
                console.log("Ops: " + data);
        });
    }

    function editar_service(service){
        
        $scope.isguardado = false;
        $scope.iseditado  = true;
        //cargar datos en el modelo
        console.log("el servicio seleccionado es " , service);
        $scope.modeloServicio = service;
        //var administrador = getServicio(service.id);
        //$scope.servicioSeleccionado = administrador;
        //console.log("los datos cargados son " , $scope.modeloUser);
    }

    function limpiar_modal (){
        $scope.isguardado = true;
        $scope.iseditado  = false;
        document.getElementById('titulo').value = "";           
        document.getElementById('namespace').value = "";           
        document.getElementById('descripcion').value = "";           
    }

    //seccion de instalacion de un nuevo modulo por parte del administrador
    //habilitar modulo para install 
    function habilitarmodulos(modulo){
        var promocion = localStorageService.get('localStorageViews').promocion;
        var ubicacion_producto = localStorageService.get('localStorageViews').ubicacion_producto;
        var listas = localStorageService.get('localStorageViews').listas;
        var analitica = localStorageService.get('localStorageViews').analitica;
        if (promocion === 1 && modulo === 'promocion' ){
            return true;
        }else if (ubicacion_producto === 1 && modulo === 'ubicacion_producto'){
            return true;
        }else if (listas === 1 && modulo === 'listas'){
                return true;
        }else if (analitica === 1 && modulo === 'analitica'){
                return true;
        }else{
            return false;
        }
    }

    function instalarmodulo(modulo){
        var promocion = localStorageService.get('localStorageViews').promocion;
        var ubicacion_producto = localStorageService.get('localStorageViews').ubicacion_producto;
        var listas = localStorageService.get('localStorageViews').listas;
        var id = localStorageService.get('localStorageViews').id;
        var analitica = localStorageService.get('localStorageViews').analitica;
        var data = { id: id, promocion : promocion, 
                     ubicacion_producto: ubicacion_producto, 
                     listas: listas,
                     analitica: analitica};
        if (modulo === 'promocion' ) {
            data.promocion = 1;
            actualizarservicio(data);
        } else if (modulo === 'ubicacion_producto') {
            data.ubicacion_producto = 1;
            actualizarservicio(data);
        } else if (modulo === 'listas') {
            data.listas = 1;
            //verificar si tiene ID Paypal
            get_datos_supermercado(data);
        } else if (modulo === 'analitica') {
            data.analitica = 1;
            //verificar si tiene ID Paypal
            get_datos_supermercadoAnalitica(data);
        }

        console.log("enviar " , data);
        
    } 

    function desinstalarmodulo(modulo){
        var promocion = localStorageService.get('localStorageViews').promocion;
        var ubicacion_producto = localStorageService.get('localStorageViews').ubicacion_producto;
        var listas = localStorageService.get('localStorageViews').listas;
        var analitica = localStorageService.get('localStorageViews').analitica;
        var id = localStorageService.get('localStorageViews').id;
        var data = { id: id, 
                     promocion : promocion,
                     ubicacion_producto: ubicacion_producto, 
                     listas: listas,
                     analitica: analitica};

        if (modulo === 'promocion' ) {
            data.promocion = 0;
            cargar_promociones(data); //verificar si contiene datos el modulo
        } else if (modulo === 'ubicacion_producto') {
            data.ubicacion_producto = 0;
            cargar_secciones(data); //verificar si contiene datos el modulo
        } else if (modulo === 'listas') {
            data.listas = 0;
            cargar_listas(data); //verificar si contiene datos el modulo
        } else if (modulo === 'analitica') {
            data.analitica = 0;
            actualizarservicio(data);
        }

        console.log("enviar " , data);
        //actualizarservicio(data);
        //cargar_promociones();
        //cargar_secciones();
    }

    function cargar_promociones (_data) {
        var promociones = [];

        $http.get('/web/promocion/' + name_bd).success(function (result, status, headers, config) {           
            promociones = result;
            swal("Advertencia!", "No puede desinstalar el modulo cuando contiene datos en producción.");

        }).error(function (data, status, headers, config) {
            console.log("Ops: could not get any data" + JSON.stringify(data));
            actualizarservicio(_data);
        });

    };//fin cargar promociones

    function cargar_listas(_data){
        var url_listas = "/listasall/" + name_bd; 
        $http.get(url_listas).success(function (data, status, headers, config) {
            var listas = data.data;
            console.log(listas);
            swal("Advertencia!", "No puede desinstalar el modulo cuando contiene datos en producción.");
                  
            }).error(function (data, status, headers, config) {
                console.log("Error:" , data);
                actualizarservicio(_data);
        });
    }

    function cargar_secciones (_data) {
        var secciones = [];
        /*envia al servidor*/
        $http.get('/web/secciones/' + name_bd).success(function (data, status, headers, config) {
            /*obtiene secciones*/
            if (!data.mensaje) {
                secciones = data;
                console.log("no se puede desintalar contiene datos almacenados en el modulo");
                swal("Advertencia!", "No puede desinstalar el modulo cuando contiene datos en producción.");
            } else {
                /*no contiene secciones*/
                secciones = [];
                actualizarservicio(_data);

            }
        }).error(function (data, status, headers, config) {
            console.log("Ops: could not get any data");
        });

    };//fin cargar promociones

    function actualizarservicio (servicio){
        $loading.start('loadmodulos');
        var url = "/servicios_supermercado/" + name_bd;
           $http.put(url, servicio)
            .success(function (data, status, headers, config) {
                localStorageService.set('localStorageViews', servicio);
                
                $timeout(function () {
                    location.reload();
                }, 2000);
                 $timeout(function () {
                    $loading.finish('loadmodulos');
                }, 4000);
               
                console.log("la data actualizada es " , localStorageService.get('localStorageViews')); 

            }).error(function (data, status, headers, config) {
                metodos.guardado_incorrecto(notify);
                console.log("Ops: " + data);
        });
    }

      //obtener los datos del supermercado
        function get_datos_supermercado (servicio) {
            console.log("cargar los datos del supermercado localStorageUser" , localStorageService.get('localStorageUser').bd_supmercado )
            var bd_supmercado = localStorageService.get('localStorageUser').bd_supmercado
            var user = localStorageService.get('localStorageDemo');
            var url = '/web/supermercado/' + user + "/" + bd_supmercado;
            //var url = '/web/supermercado/' + {params:{"user": user, "namebd": bd_supmercado}};

            $http.get(url).success(function (data, status, headers, config) {
                /*almacena los datos del supermercado*/
                $scope.supermercado = data[0];

                console.log("los datos del supermercado son >> " , $scope.supermercado);
                console.log("los datos  >> " , $scope.supermercado.id_paypal);
                if ($scope.supermercado.id_paypal){
                    console.log("si existe id paypal instalar");
                    actualizarservicio(servicio);
                } else {
                    swal("Paypal!", "No existe el Id Paypal configurado, porfavor ingrese a configuracion y agrege su id de paypal conrrespondiente.");
                    console.log("no existe el id paypal activar el  paypal en la vista");

                }

            }).error(function (data, status, headers, config) {
                console.log("Ops: could not get any data");
            });
        };

        function get_datos_supermercadoAnalitica (servicio) {
            console.log("cargar los datos del supermercado localStorageUser" , localStorageService.get('localStorageUser').bd_supmercado )
            var bd_supmercado = localStorageService.get('localStorageUser').bd_supmercado
            var user = localStorageService.get('localStorageDemo');
            var url = '/web/supermercado/' + user + "/" + bd_supmercado;
            //var url = '/web/supermercado/' + {params:{"user": user, "namebd": bd_supmercado}};

            $http.get(url).success(function (data, status, headers, config) {
                /*almacena los datos del supermercado*/
                $scope.supermercado = data[0];

                console.log("los datos del supermercado son >> " , $scope.supermercado);
                console.log("los datos  >> " , $scope.supermercado.id_analitica_paypal);
                if ($scope.supermercado.id_analitica_paypal){
                    console.log("si existe id paypal instalar");
                    actualizarservicio(servicio);
                } else {
                    swal("Paypal!", "No existe el Id Paypal configurado, porfavor ingrese a configuracion y agrege su id de paypal conrrespondiente.");
                    console.log("no existe el id paypal activar el  paypal en la vista");

                }

            }).error(function (data, status, headers, config) {
                console.log("Ops: could not get any data");
            });
        };

}]);

//metodos generales 
var metodos = {
    guardado_correcto : function(notify , mensaje){
        notify({
            message: mensaje,
            classes: 'alert-success', // alert-danger or alert-success
            templateUrl: '',
            position: 'center',
            duration: 2000
        });
    }, 

    guardado_incorrecto : function(notify){
        notify({
            message: 'Error al guardar Intente mas tarde :( ',
            classes: 'alert-danger', // alert-danger or alert-success
            templateUrl: '',
            position: 'center',
            duration: 2000
        });
    },

    getAdminId: function (listadmin , administradorId) {
        for (var i = 0; i < listadmin.length; i++) {
            if (listadmin[i].id === parseInt(administradorId)) {
                return listadmin[i];
            }
        }
        return null;
    }
}
