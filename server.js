// server.js

// set up ======================================================================
// get all the tools we need
var express  = require('express');
var app      = express();
//var port     = process.env.PORT || 8000;
var ipaddress = process.env.OPENSHIFT_NODEJS_IP || "127.0.0.1";
var port = process.env.OPENSHIFT_NODEJS_PORT || 8080;

var passport = require('passport');
var flash    = require('connect-flash');

var path     = require('path');
var bodyParser = require('body-parser');

// configuration ===============================================================


require('./config/passport')(passport); // configuracion de la contraseña
app.use(express.static(path.join(__dirname, 'public'))); //ruta estatica
app.use(bodyParser.json({limit: '50mb'}));
//para que al momento de retroceder en el navegador no acceda a  la session
app.use(function(req, res, next) {
    if (!req.user)
        res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
    next();
});



app.use(bodyParser.urlencoded({ extended: true })); //support x-www-form-urlencoded
app.use(bodyParser.json());

app.engine('.html', require('ejs').__express);
app.set('view engine', 'html');//reconosca archivos con extencion .html
app.set('views', './public'); //fiajamos la carpeta public

//configuracion
app.configure(function() {

	// set up our express application
	app.use(express.logger('dev')); // log every request to the console
	app.use(express.cookieParser()); // read cookies (needed for auth)
	app.use(express.bodyParser()); // get information from html forms
	//app.set('view engine', 'ejs'); // set up ejs for templating

	// required for passport
	app.use(express.session({ secret: 'vidyapathaisalwaysrunning' } )); // session secret
	app.use(passport.initialize());
	app.use(passport.session()); // persistent login sessions
	app.use(flash()); // use connect-flash for flash messages stored in session
	app.use(app.router);
});
//cargar archivos


//determina cada unas de las actividades que pueden realizar en el web service

app.all("/*", function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Content-Type");
    res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");
    return next();

});

// routes ======================================================================
require('./app/routes.js')(app, passport); // load our routes and pass in our app and fully configured passport

//query===============================================
require('./config/query.js')(app); // load cada uno de las consultas realizadas por url
require('./config/ws/ws_superuser.js')(app); // load cada uno de las consultas realizadas por url
require('./config/ws/ws_cliente.js')(app); // ws del cliente 
require('./config/ws/ws_client_admin.js')(app); // ws del cliente 
require('./config/esquema/create_squema.js')(app); // ws del cliente 

//send email
require('./app/sendemail.js')(app); // load our routes and pass in our app and fully configured passport

// launch ======================================================================
//development
/*app.listen(port);
console.log('The magic happens on port ' + port);*/

//produccion
app.listen(port, ipaddress, function() {
    console.log('The magic happens on port ' + port);
});
