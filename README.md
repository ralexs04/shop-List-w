#Sistema de administracion web shoplist
Pasos para inicar:

     1) clonar el respositorio
     2) tener la base de datos configurada y up
     3) ejecutar npm start para que arranque la aplicacion


Descripcion:

	Aplicacion web para gestionar la ubicacion de productos,
        promociones, aplicada como caso de estudio en el micromercado ciudad victoria.

	Requerimientos Principales:

		- gestion login
        - crear promociones
		- crear ubicaciones
		- muestra promociones disponibles
		- gestion de productos
		- gestion de usuarios(supermercados)
		- gestionar clientes  

	Disponible:
		- chrome
		- firefox

Generada por:

	-Raul Alexander Gomez Armijos correo:(ralexs04@gmail.com)
	-Yessenia Karina Barrionuevo Sarango

Licencia:

	MIT License.

    Produccion

# Complete Guide to Node Authentication with MySQL

Code for the entire scotch.io tutorial series: Complete Guide to Node Authentication with MongoDB

Current version database is ported to MySQL

We will be using Passport to authenticate users locally,

## Instructions

If you would like to download the code and try it for yourself:

1. Clone the repo: `git clone git@gitlab.com:ralexs04/shop-List-w.git`
1. Install packages: `npm install`
1. Edit the database configuration: `config/database.js`
1. Create the database schema: `node scripts/create_database.js`
1. Launch: `node server.js`
1. Visit in your browser at: `http://localhost:8080`
