angular.module('shop.controllers')
    .controller('ProductosCtrl', ['adminService'] , ProductosCtrl, adminService);

function ProductosCtrl($scope,
                       localStorageService, 
                       $http,
                       adminService,
                       notify,
                       $loading){
	var name_bd = localStorageService.get('localStorageUser').bd_supmercado;
    var urlproducto = '/admin/productos/'+ name_bd;

//	$scope.visibleFormProducto = true;
    $scope.buscar_productos     = buscar_productos;
    $scope.guardar_producto     = guardar_producto;
    $scope.activarFormProducto  = activarFormProducto;
    $scope.editar_producto      = editar_producto;
    $scope.actualizar_producto  = actualizar_producto;
    $scope.detalles_producto    = detalles_producto;
    $scope.activarGuardado      = activarGuardado;
    $scope.cargaImagen          = cargaImagen;
    $scope.cargarProductosBd    = cargarProductosBd;
    $scope.cargarFiltroSecciones= cargarFiltroSecciones;
    $scope.filtrarProductos     = filtrarProductos;

    function buscar_productos(texto){
        $scope.productos_buscados = [];
        
        if ($scope.texto_buscar) {
            console.log("enviar al servicio");
            $loading.start('cargando');

            adminService.doProductos($scope.texto_buscar , name_bd).then(function(_productos_) {
                $scope.productos_buscados = _productos_;
                if (_productos_ === "Productos Not found") {
                    console.log("no contiene productos");
                    alertaSuccess(notify, "Producto no encontrado");
                    $scope.productos_buscados = [];
                }
                $loading.finish('cargando');
            });

        } else {
            console.log("ingrese un nombre en el campo de texto")
        }
    };//fin cargar productos

    function guardar_producto(producto){
        
        producto.imagen = $scope.imagen;
        $http.post(urlproducto, producto).success(function (data, status, headers, config) {
                alertaSuccess(notify, "Producto guardado exitosamente");
                activarFormProducto();
                
            }).error(function (data, status, headers, config) {
                console.log("Ops: could not save any data");
                alertaError(notify , "Error al guardar porfavor intente mas tarde !!");
                activarFormProducto();
            });
    }

    function actualizar_producto(producto){
        //producto.imagen = $scope.imagen;
        producto.imagen = $scope.imagen;
        $http.put(urlproducto, producto).success(function (data, status, headers, config) {
            producto.imagen = data.imagen;
            alertaSuccess(notify, "Producto actualizado exitosamente");
            activarFormProducto();
                
            }).error(function (data, status, headers, config) {
                console.log("Ops: could not save any data");
                alertaError(notify , "Error al actualizar porfavor intente mas tarde !!");
                activarFormProducto();
            });
        }


    function activarFormProducto(){
        $scope.visibleFormProducto = $scope.visibleFormProducto === true ? false : true;
    }

    function activarGuardado(){
        $scope.isGuardado = true;
        $scope.isEditado = false;
        //borrar datos guardados anterior por una edicion
        $scope.producto = [];

    }

    function limpiardatosform(){
        /*nom_prod
        cod_prod
        cod_barras_prod
        cant_prod
        costo_prod
        pvp_prod
        iva_prod
        cod_marca*/
    }


    function editar_producto(producto){
        console.log(">>> ", producto);
        console.log("cargar");
        $scope.producto = producto;
        $scope.isEditado = true;
        $scope.isGuardado = false;
        activarFormProducto();

    }
    function detalles_producto (producto){
        console.log("el producto a cagar es " , producto);
        $scope.producto = producto;
    }
    
    //seccion de la imagen

    function cargaImagen(){
        try {
            if (window.FileReader) {
                var oFReader = new FileReader();
                oFReader.readAsDataURL(document.getElementById("imgInput").files[0]);
                oFReader.onload = function (oFREvent) {
                    $scope.$apply(function(){
                        $scope.imagen = oFREvent.target.result;
                        document.getElementById("imgPreview").src= $scope.imagen;
                    });
                };
            } else {
                alert("Informacion",'Su navegador no soporta importación de archivos, por favor actualicelo');
            }
        } catch (e) {
            console.log(e);
        }
    }

    function cargarProductosBd(){

        adminService.doCargarProductosBd(name_bd).then(function(response) {
            console.log("los datos de respuesa es "  , response);
        });
    }

    function cargarFiltroSecciones() {
        $scope.ubicaciones = [{nombre : "Todos"}];
        $http.get('/web/productos/ubicacion/' + name_bd).success(function (data, status, headers, config) {
            if (data.mensaje) {
                //en caso de no encontrar productos
                console.log("no tiene ubicaciones");
                //$scope.ubicaciones = [];
            } else {
                data.push({nombre : "Todos"})
                $scope.ubicaciones= data;

            }
        }).error(function (data, status, headers, config) {
            console.log("Ops: could not get any data");
        });
    }

    function filtrarProductos() {
         if ($scope.secciones_select && $scope.secciones_select.nombre !== "Todos") {
                $loading.start('cargando');
                $scope.productos_buscados = [];
                var url = '/web/productos/ubicacionasg/' + $scope.secciones_select.cod_ubicacion + '/' + name_bd;
                cargarProductos(url);
        } else if ($scope.secciones_select && $scope.secciones_select.nombre === "Todos"){
            $loading.start('cargando');
            var url = '/productos/totales/'+ name_bd;
            cargarProductos(url);
        }
    }

    function cargarProductos(url) {
        $http.get(url).success(function (data, status, headers, config) {
            $scope.productos_buscados = data;
            $loading.finish('cargando');
        }).error(function (data, status, headers, config) {
            alertaError(notify, 'No contiene productos');
            $loading.finish('cargando');
            console.log("Ops: could not get any data");
        });
    }

    
}

